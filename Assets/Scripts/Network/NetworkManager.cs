﻿﻿using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;
using Network.Cache;
using Network.Events;
using UI;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Network {
	public class NetworkManager : MonoBehaviour {
		public const string URI = "";

		public delegate void RequestCallback(WebResponse response);

		public static string AuthToken = "";


		#region Singleton

		private static NetworkManager Instance {
			get {
				if (!_instance) {
					_instance = new GameObject("Network Manager").AddComponent<NetworkManager>();
					DontDestroyOnLoad(_instance.gameObject);
				}
				return _instance;
			}
		}
		private static NetworkManager _instance;

		#endregion


		/// <summary>
		/// Отправка POST запроса на сервер
		/// </summary>
		/// <param name="packet">Тип пакета</param>
		/// <param name="successCallback">Метод в который возвращается ответ</param>
		/// <param name="errorCallback">Метод в который возвращается ответ</param>
		/// <param name="parameters">Массив параметров</param>
		/// <returns></returns>
		public static void SendPacket(Packets.Packet packet,        RequestCallback successCallback,
									RequestCallback  errorCallback, params string[] parameters) {
			Instance.StartCoroutine(_instance.Post(packet, successCallback, errorCallback, parameters));
		}


		private static WWWForm CreateForm(params string[] parameters) {
			WWWForm form = new WWWForm(); // Создаем форму

			form.headers.Add("User-agent", "Game");
			form.AddField("token", AuthToken); // Добавляем поле с токеном

			// Добавляем кастомные параметры
			for (int i = 0; i < parameters.Length; i++) {
				var pName = "p" + (i + 1);
				var p     = parameters[i];
				form.AddField(pName, p);
			}

			return form;
		}


		private static bool CheckResponseErrors(UnityWebRequest response) {
			// Вывод ошибок
			if (!response.isNetworkError && !response.isHttpError) return true;
			Debug.LogError( $"Error: {response.error}");
			return false;
		}


		private static void CheckConnectionLost(WebResponse response) {
			if (response.Code == 503) {
				var text = response.Text;
				if (text.Length > 32) {
					text = text.Substring(0, 29) + "...";
				}
				Notification.Create($"Соединение с сервером разорвано: {text}");
				CloseConnection();
			}
		}


		private IEnumerator Post(Packets.Packet packet, RequestCallback successCallback, RequestCallback errorCallback,
								params string[] parameters) {
			WWWForm form = CreateForm(parameters);

			var uri = Packets.GetPacketURI(packet);
			if (packet != Packets.Packet.CheckConnection && packet != Packets.Packet.UpdateTimer) {
				print($"Sending packet URI: {uri}({string.Join(",", parameters)})");
			}

			// Отправка запроса и ожидание ответа
			var response = UnityWebRequest.Post(uri, form);
			yield return response.SendWebRequest();

			WebResponse r = new WebResponse(response.downloadHandler.text, response.downloadHandler.data,
											response.error, response.responseCode);

			r.Text = Regex.Unescape(r.Text);

			if (CheckResponseErrors(response)) {
				successCallback?.Invoke(r);
			}
			else {
				Debug.LogError($"Error on '{uri}': {r.Text}");
				errorCallback?.Invoke(r);
				CheckConnectionLost(r);
			}
		}


		public static void CloseConnection() {
			CacheManager.Reset();
			Session.StopSession();
			EventsController.OnConnectionLost();
			EventsController.ResetAllEvents();
			SceneManager.LoadScene("Menu");
		}
	}
}