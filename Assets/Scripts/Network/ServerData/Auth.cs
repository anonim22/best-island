using Newtonsoft.Json;

namespace Network.ServerData {
	public class Auth {
		[JsonProperty("token")]      public string Token;
		[JsonProperty("id")]         public int    UserID;
		[JsonProperty("updated_at")] public string Updated;
		[JsonProperty("created_at")] public string Created;
	}
}