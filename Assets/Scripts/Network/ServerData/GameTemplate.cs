using Newtonsoft.Json;

namespace Network.ServerData {
	public class GameTemplate {
		[JsonProperty("time_prepare")]         	public float PrepareTime;
		[JsonProperty("money")]                	public float StartMoney;
		[JsonProperty("general_manager_time")] 	public int GeneralManagerTime;
		[JsonProperty("financial_manager_time")]public int FinancialManagerTime;
		[JsonProperty("hr_time")]            	public int HrTime;
		[JsonProperty("builder_time")]       	public int BuilderTime;
		[JsonProperty("start_round_time")]   	public float StartRoundTime;
		[JsonProperty("game_round_time")]    	public int GameRoundTime;
	}
}