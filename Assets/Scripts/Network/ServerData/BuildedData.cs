using Newtonsoft.Json;

namespace Network.ServerData {
	public class BuildedData {
		[JsonProperty("id")]             public int Id;
		[JsonProperty("area_id")]        public int AreaId;
		[JsonProperty("round_no")]       public int Round;
		[JsonProperty("building_id")]    public int BuildingId;
		[JsonProperty("stage")]          public int Stage;
		[JsonProperty("builded_blocks")] public int BuildedBlocks;
	}
}