using Newtonsoft.Json;

namespace Network.ServerData {
	public class MoneyChanged {
		[JsonProperty("money")]   public int Balance;
		[JsonProperty("changed")] public int Changed;
	}
}