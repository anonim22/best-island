using Newtonsoft.Json;

namespace Network.ServerData {
	public class CharteringData {
		[JsonProperty("id")]        public int    Id;
		[JsonProperty("sender")]    public int    SenderId;
		[JsonProperty("recipient")] public int    Recipient;
		[JsonProperty("ship_id")]   public int    ShipId;
		[JsonProperty("status")]    public string Status;
	}
}