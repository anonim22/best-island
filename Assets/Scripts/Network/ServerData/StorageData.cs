using Newtonsoft.Json;

namespace Network.ServerData {
	public class StorageData {
		[JsonProperty("ids")]     public string Materials;
		[JsonProperty("amounts")] public string Quantity;
	}
}