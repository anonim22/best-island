using Newtonsoft.Json;

namespace Network.ServerData {
	public class LoanData {
		[JsonProperty("id")]          public int    Id;
		[JsonProperty("loan")]        public int    Loan;
		[JsonProperty("balance")]     public int    Balance;
		[JsonProperty("round")]       public int    RequestRound;
		[JsonProperty("percent")]     public int    Percent;
		[JsonProperty("need_to_pay")] public int    ToPay;
		[JsonProperty("status")]      public string Status;
	}
}