using Newtonsoft.Json;

namespace Network.ServerData {
	public class MenuGamesData {
		[JsonProperty("id")]              public int    ID;
		[JsonProperty("name")]            public string Name;
		[JsonProperty("game_is_started")] public string GameIsStarted;
		[JsonProperty("teams_count")]     public int    TeamsCount;
		[JsonProperty("players_count")]   public int    PlayersCount;
	}
}