using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Network.ServerData {
	public class Templates {
		[JsonProperty("game_template")] public GameTemplate GameTemplate;
	}
}