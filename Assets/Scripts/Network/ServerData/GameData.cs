using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Network.ServerData {
	public class GameData {
		[JsonProperty("id")]               public int    ID;
		[JsonProperty("name")]             public string Name;
		[JsonProperty("teams_count")]      public int    TeamsCount;
		[JsonProperty("game_template_id")] public int    GameTemplateId;
		[JsonProperty("round_no")]         public int    Round;

		[JsonProperty("game_is_started")]   private int _gameIsStarted;
		[JsonProperty("game_is_ended")]     private int _gameIsEnded;
		[JsonProperty("game_is_paused")]    private int _gameIsPaused;
		[JsonProperty("paused_by_admin")]   private int _adminPause;
		[JsonProperty("round_is_started")]  private int _roundIsStarted;
		[JsonProperty("paused_by_auction")] private int _pausedByAuction;

		[JsonProperty("buildings_template_id")] public int BuildingsTemplateId;

		public bool GameIsStarted {
			get => Convert.ToBoolean(Convert.ToInt16(_gameIsStarted));
			set => _gameIsStarted = Convert.ToInt16(value);
		}

		public bool GameIsEnded {
			get => Convert.ToBoolean(Convert.ToInt16(_gameIsEnded));
			set => _gameIsEnded = Convert.ToInt16(value);
		}
		public bool RoundIsStarted {
			get => Convert.ToBoolean(Convert.ToInt16(_roundIsStarted));
			set => _roundIsStarted = Convert.ToInt16(value);
		}
		public bool GameIsPaused    => Convert.ToBoolean(Convert.ToInt16(_gameIsPaused));
		public bool AdminPause      => Convert.ToBoolean(Convert.ToInt16(_adminPause));
		public bool PausedByAuction => Convert.ToBoolean(Convert.ToInt16(_pausedByAuction));
	}
}