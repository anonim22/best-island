using System.Collections.Generic;
using System.Linq;
using Network.Cache.Data;
using Newtonsoft.Json;
using UnityEngine;

namespace Network.ServerData {
	public class RoundResultData {
		public int            Round;
		public List<TeamData> Teams;
		public TeamData CurrentTeam {
			get {
				var teamId = TeamDataCache.ActiveTeamId;
				return Teams.FirstOrDefault(t => t.TeamId == teamId);
			}
		}

		public class TeamData {
			[JsonProperty("round_no")]   public int Round;

			[JsonProperty("team_id")]   public int    TeamId;
			[JsonProperty("team_name")] public string TeamName;

			[JsonProperty("building_id")] public int BuildingId;
			/// <summary>
			/// Построено вовремя
			/// </summary>
			[JsonProperty("built_in_time")] public int BuiltOnTime;

			/// <summary>
			/// Доход со здания в текущем раунде
			/// </summary>
			[JsonProperty("revenue_build")] public float RevenueBuild;
			/// <summary>
			/// Доход в текущем раунде
			/// </summary>
			[JsonProperty("revenue")] public float Revenue;

			/// <summary>
			/// Денег потрачено в текущем раунде
			/// </summary>
			[JsonProperty("money_spent")] public float MoneySpent;
			/// <summary>
			/// Денег привлечено в текущем раунде
			/// </summary>
			[JsonProperty("money_attracted")] public float MoneyAttracted;

			// ***Эффективности***
			[JsonProperty("act_round")]        public float RoundEfficiency;
			[JsonProperty("act_personal")]     public float HREfficiency;
			[JsonProperty("act_executor")]     public float BuilderEfficiency;
			[JsonProperty("act_final_dir")]    public float FMEfficiency;
			[JsonProperty("act_gen_director")] public float DirectorEfficiency;
			[JsonProperty("act_area")]         public int AreaEfficiency;

			/// <summary>
			/// Выплачены проценты по кредиту
			/// </summary>
			[JsonProperty("annual_payment")] public int LoanPercentPayed;

			/// <summary>
			/// Стихийные бедствия
			/// </summary>
			[JsonProperty("disasters")] public string Disasters;
			/// <summary>
			/// События
			/// </summary>
			[JsonProperty("events")] public string Events;
			/// <summary>
			/// Страховка
			/// </summary>
			[JsonProperty("insurance")] public string Insurance;

			/// <summary>
			/// Результат по деньгам за раунд
			/// </summary>
			[JsonProperty("fin_result_round")] public float RoundResult;
			/// <summary>
			/// Результат по деньгам за всю игру
			/// </summary>
			[JsonProperty("fin_result_game")] public float GameResult;
		}


		public static RoundResultData Parse(WebResponse response) {
			var result = new RoundResultData();
			result.Teams = JsonConvert.DeserializeObject<List<TeamData>>(response.Text);
			return result;
		}
	}
}