using System;
using Network.Cache.Data;
using Newtonsoft.Json;

namespace Network.ServerData {
	public class TeamData {
		[JsonProperty("id")]      public int    ID;
		[JsonProperty("game_id")] public int    GameID;
		[JsonProperty("name")]    public string TeamName;
		[JsonProperty("money")]   public int    Money;

		[JsonProperty("general_manager_time")] 	 public int GeneralManagerTime;
		[JsonProperty("financial_manager_time")] public int FinancialManagerTime;
		[JsonProperty("hr_time")]      			 public int HrTime;
		[JsonProperty("builder_time")] 			 public int BuilderTime;

		[JsonProperty("player1_name")] public string Player1_Name;
		[JsonProperty("player2_name")] public string Player2_Name;
		[JsonProperty("player3_name")] public string Player3_Name;
		[JsonProperty("player4_name")] public string Player4_Name;

		[JsonProperty("round_end")]    public int RoundEnd;
		[JsonProperty("area")]         public int  AreaId;

		public int[] Roles = new int[2];
	}
}