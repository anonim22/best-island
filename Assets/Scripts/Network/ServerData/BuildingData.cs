using Newtonsoft.Json;

namespace Network.ServerData {
	public class BuildingData {
		[JsonProperty("bid")]          public int    Id;
		[JsonProperty("name")]         public string Name;
		[JsonProperty("description")]  public string Description;
		[JsonProperty("income")]       public float  Income;
		[JsonProperty("income_bonus")] public float  IncomeBonus;
	}
}