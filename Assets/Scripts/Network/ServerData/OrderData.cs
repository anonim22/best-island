using System.Collections;
using System.Linq;
using Game;
using Game.Data;
using Network.Cache;
using Network.Cache.Data;
using Newtonsoft.Json;

namespace Network.ServerData {
	public class OrderData {
		[JsonProperty("id")]        public int    Id;
		[JsonProperty("order_no")]  public int    OrderId;
		[JsonProperty("team_id")]   public int    TeamId;
		[JsonProperty("materials")] public string MaterialsId;
		[JsonProperty("amounts")]   public string Quantity;
		[JsonProperty("status")]    public string Status;

		// For event
		[JsonProperty("order_id")]
		public int _Id {
			set => Id = value;
		}
		[JsonProperty("materials_id")]
		public string _MaterialsId {
			set => MaterialsId = value;
		}
		[JsonProperty("amount")]
		public string _Quantity {
			set => Quantity = value;
		}


		public static Order Parse(OrderData orderData) {
			var order = new Order {
				Id     = orderData.Id,
				OrderId =  orderData.OrderId,
				TeamId = (orderData.TeamId == 0) ? TeamDataCache.ActiveTeam.ID : orderData.TeamId
			};

			var materials         = orderData.MaterialsId.Split(',');
			var materialsQuantity = orderData.Quantity.Split(',');

			for (int i = 0; i < materials.Length; i++) {
				var materialId = int.Parse(materials[i]);
				var quantity   = int.Parse(materialsQuantity[i]);

				var material      = MaterialsData.Data.Materials.Single(m => m.Id == materialId);
				var orderMaterial = new OrderMaterial(material) {Quantity = quantity};
				order.Materials.Add(orderMaterial);
			}

			if (!string.IsNullOrEmpty(orderData.Status)) {
				order.Payed = orderData.Status.Equals("payed");
			}

			return order;
		}
	}
}