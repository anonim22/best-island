using System.Collections.Generic;
using Newtonsoft.Json;

namespace Network.ServerData {
	public class GameResultData {
		[JsonProperty("teams")] public List<TeamData> Teams;
		[JsonProperty("most_act_dir")] public string TopDirector;

		public class TeamData {
			[JsonProperty("team_id")]           public int    TeamId;
			[JsonProperty("team_name")]         public string TeamName;

			[JsonProperty("buildings")]         public int[] Buildings;
			[JsonProperty("total_rounds")]      public int Rounds;

			/// <summary>
			/// Денег потрачено за сыгранные раунды
			/// </summary>
			[JsonProperty("money_spent")]       public float MoneySpent;
			/// <summary>
			/// Денег привлечено за сыгранные раунды
			/// </summary>
			[JsonProperty("money_attracted")]   public float MoneyAttracted;
			[JsonProperty("money_received")]    public float MoneyReceived;
			/// <summary>
			/// Денег передано другим командам
			/// </summary>
			[JsonProperty("money_sent")]        public float MoneySent;
			/// <summary>
			/// Денег потрачено на лоты аукциона
			/// </summary>
			[JsonProperty("auction_costs")]     public float AuctionCosts;


			/// <summary>
			/// Доход за сыгранные раунды
			/// </summary>
			[JsonProperty("revenue")]           public float Revenue;
			/// <summary>
			/// Доход за 10 лет
			/// </summary>
			[JsonProperty("revenue_10")]        public float Revenue10;
			/// <summary>
			/// Доход от аукциона
			/// </summary>
			[JsonProperty("revenue_auction")]   public float AuctionRevenue;
			/// <summary>
			/// Доход от аукциона за 10 лет
			/// </summary>
			[JsonProperty("revenue_auction_10")]public float AuctionRevenue10;
			/// <summary>
			/// Доход от возможностей
			/// </summary>
			[JsonProperty("revenue_events")]    public float EventsRevenue;
			/// <summary>
			/// Доход от возможностей за 10 лет
			/// </summary>
			[JsonProperty("revenue_events_10")] public float EventsRevenue10;
			/// <summary>
			/// Затраты от событий
			/// </summary>
			[JsonProperty("expenses_events")]   public float EventsCosts;
			/// <summary>
			/// Затраты от событий за 10 лет
			/// </summary>
			[JsonProperty("expenses_events_10")]public float EventsCosts10;
			/// <summary>
			/// Затраты от событий
			/// </summary>
			[JsonProperty("annual_payment")]   public float AnnualPayment;
			/// <summary>
			/// Затраты от событий за 10 лет
			/// </summary>
			[JsonProperty("annual_payment_10")]public float AnnualPayment10;



			/// <summary>
			/// Баланс к концу игры
			/// </summary>
			[JsonProperty("fin_result_game")] public float MoneyResult;
			[JsonProperty("fin_result_game_10")] public float MoneyResult10;

			// ***Эффективности***
			[JsonProperty("act_personal")]     public float HREfficiency;
			[JsonProperty("act_executor")]     public float BuilderEfficiency;
			[JsonProperty("act_final_dir")]    public float FMEfficiency;
			[JsonProperty("act_gen_director")] public float DirectorEfficiency;
			[JsonProperty("game_act")]         public float GameEfficiency;
			[JsonProperty("act_area")]         public float AreaEfficiency;
			[JsonProperty("most_act_round")]   public int MostEfficiencyRound;

			[JsonProperty("events")]    public List<string> Events;
			[JsonProperty("disasters")] public List<string> Disasters;
			// [JsonProperty("insurance")] public List<string> Insurance;
		}
	}
}