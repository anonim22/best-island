using Newtonsoft.Json;

namespace Network.ServerData {
	public class SetBuildingData {
		[JsonProperty("building_id")] public int BuildingId;
		[JsonProperty("area_no")]     public int AreaId;
	}
}