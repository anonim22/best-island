using Newtonsoft.Json;

namespace Network.ServerData {
	public class TransferData {
		[JsonProperty("id")]          public int Id;
		[JsonProperty("team_id")]     public int Sender;
		[JsonProperty("target_team")] public int ReceiverTeam;
		[JsonProperty("sum")]         public int Sum;
	}
}