using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Network.ServerData.Event {
	public class Event {
		[JsonProperty("players_connected")]    public  List<string>                   PlayersConnected;
		[JsonProperty("players_disconnected")] public  List<string>                   PlayersDisconnected;
		[JsonProperty("game_paused")]          public  List<string>                   GamePaused;
		[JsonProperty("game_resumed")]         public  List<string>                   GameResumed;
		[JsonProperty("round_time_left")]      public  int                            UpdateRoundTimer = int.MaxValue;
		[JsonProperty("disaster")]             public  List<string>                   Disasters;
		[JsonProperty("money_changed")]        public  List<MoneyChanged>             MoneyChanged;
		[JsonProperty("order_created")]        public  List<OrderData>                OrdersCreated;
		[JsonProperty("order_removed")]        public  List<int>                      OrderRemoved;
		[JsonProperty("order_payed")]          public  List<int>                      OrderPayed;
		[JsonProperty("on_chartering_update")] public  List<CharteringData>           CharteringData;
		[JsonProperty("loan_update")]          public  List<LoanData>                 LoanData;
		[JsonProperty("update_materials")]     public  List<StorageData>              StorageData;
		[JsonProperty("UpdateMoneySend")]      public  List<TransferData>             TransferData;
		[JsonProperty("UpdateBuildingState")]  public  BuildedData                    BuildedData;
		[JsonProperty("set_building")]         private List<SetBuildingData>          _setBuildingData;
		[JsonProperty("disasters")]            public  Disaster                       Disaster;
		[JsonProperty("auction_next")]         public  string                         Auction;
		[JsonProperty("auction_fail")]         public  int                            AuctionReject;
		[JsonProperty("auction_success")]      public  List<int>                      AuctionConfirm;
		[JsonProperty("event")]                public  List<int>                      GameEvents;
		[JsonProperty("result_confirmed")]     public  List<RoundResultData.TeamData> RoundResultConfirmed;
		[JsonProperty("other")]                public  List<string>                   Other;
		[JsonProperty("game_result_confirmed")]
		public GameResultData GameResult;

		public SetBuildingData SetBuildingData => _setBuildingData?.FirstOrDefault();
	}
}