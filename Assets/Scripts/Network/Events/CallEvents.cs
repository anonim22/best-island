using System.Collections.Generic;
using Network.ServerData;
using UnityEngine;

namespace Network.Events {
	public static partial class EventsController {
		public static void OnConnectionLost() {
			OnConnectionLostEvent?.Invoke();
		}


		private static void OnOrdersCreated(List<OrderData> data) {
			OnOrdersCreateEvent?.Invoke(data);
		}


		private static void OnOrdersRemoved(List<int> orders) {
			OnOrdersRemovedEvent?.Invoke(orders);
		}


		private static void OnOrdersPayed(List<int> orders) {
			OnOrdersPayedEvent?.Invoke(orders);
		}


		private static void OnMoneyChanged(List<ServerData.MoneyChanged> moneyChanged) {
			OnMoneyChangedEvent?.Invoke(moneyChanged);
		}


		private static void OnLoansUpdate(List<LoanData> loans) {
			OnLoansUpdateEvent?.Invoke(loans);
		}


		private static void OnCharteringUpdate(List<CharteringData> data) {
			OnCharteringUpdateEvent?.Invoke(data);
		}


		private static void OnStorageUpdate(List<StorageData> data) {
			OnStorageUpdateEvent.Invoke(data);
		}


		private static void OnTransferUpdate(List<TransferData> data) {
			OnTransferUpdateEvent.Invoke(data);
		}


		private static void OnBuildingStageUpdate(BuildedData data) {
			OnBuildingStageUpdateEvent.Invoke(data);
		}


		private static void OnSetBuilding(SetBuildingData building) {
			OnSetBuildingEvent.Invoke(building);
		}


		private static void OnUpdateRoundTimer(int time) {
			OnUpdateRoundTimerEvent?.Invoke(time);
		}


		private static void OnGameResumed(List<string> reason) {
			OnGameResumedEvent?.Invoke(reason);
		}


		private static void OnGamePaused(List<string> reason) {
			OnGamePausedEvent?.Invoke(reason);
		}


		private static void OnDisaster(int disasterId) {
			OnDisasterEvent?.Invoke(disasterId);
		}


		private static void OnAuction(string lot) {
			OnAuctionEvent?.Invoke(lot);
		}


		private static void OnAuctionConfirm() {
			OnAuctionConfirmEvent?.Invoke();
		}


		private static void OnAuctionReject() {
			OnAuctionRejectEvent?.Invoke();
		}


		private static void OnGameEvent(int id) {
			OnGameEvent_event?.Invoke(id);
		}


		private static void OnRoundResultConfirmed(RoundResultData data) {
			OnRoundResultConfirmedEvent?.Invoke(data);
		}


		private static void OnGameResultConfirmed(GameResultData data) {
			OnGameResultConfirmedEvent?.Invoke(data);
		}
	}
}