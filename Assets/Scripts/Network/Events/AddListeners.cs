using MainMenu;

namespace Network.Events {
	public static partial class EventsController {
		public static void AddListener(DefaultEvents eventType, DefaultEvent callback) {
			switch (eventType) {
				case DefaultEvents.ConnectionLost: {
					OnConnectionLostEvent += callback;
					break;
				}
			}
		}


		public static void AddOrdersCreatedListener(OrdersData callback) {
			OnOrdersCreateEvent += callback;
		}


		public static void AddOrdersRemovedListener(OrdersId callback) {
			OnOrdersRemovedEvent += callback;
		}


		public static void AddOrdersPayedListener(OrdersId callback) {
			OnOrdersPayedEvent += callback;
		}


		public static void AddMoneyChangedListener(MoneyChanged callback) {
			OnMoneyChangedEvent += callback;
		}


		public static void AddLoansUpdateListener(UpdateLoans callback) {
			OnLoansUpdateEvent += callback;
		}


		public static void AddCharteringUpdateListener(UpdateChartering callback) {
			OnCharteringUpdateEvent += callback;
		}


		public static void AddStorageUpdateListener(UpdateStorage callback) {
			OnStorageUpdateEvent += callback;
		}


		public static void AddTransferUpdateListener(UpdateTransfer callback) {
			OnTransferUpdateEvent += callback;
		}


		public static void AddBuildedDataUpdateListener(UpdateBuildingStage callback) {
			OnBuildingStageUpdateEvent += callback;
		}


		public static void AddSetBuildingListener(SetBuilding callback) {
			OnSetBuildingEvent += callback;
		}


		public static void AddUpdateRoundTimerListener(UpdateRoundTimer callback) {
			OnUpdateRoundTimerEvent += callback;
		}


		public static void AddGamePausedListener(ChangeGameState callback) {
			OnGamePausedEvent += callback;
		}


		public static void AddGameResumedListener(ChangeGameState callback) {
			OnGameResumedEvent += callback;
		}


		public static void AddDisasterListener(Disaster callback) {
			OnDisasterEvent += callback;
		}


		public static void AddAuctionListener(Auction callback) {
			OnAuctionEvent += callback;
		}


		public static void AddAuctionConfirmListener(AuctionResponse callback) {
			OnAuctionConfirmEvent += callback;
		}


		public static void AddAuctionRejectListener(AuctionResponse callback) {
			OnAuctionRejectEvent += callback;
		}


		public static void AddGameEventListener(GameEvent callback) {
			OnGameEvent_event += callback;
		}


		public static void AddRoundResultConfirmedListener(RoundResultConfirmed callback) {
			OnRoundResultConfirmedEvent += callback;
		}


		public static void AddGameResultConfirmedListener(GameResultConfirmed callback) {
			OnGameResultConfirmedEvent += callback;
		}
	}
}