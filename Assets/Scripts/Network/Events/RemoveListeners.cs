using MainMenu;

namespace Network.Events {
	public static partial class EventsController {
		public static void RemoveRoundResultConfirmedListener(RoundResultConfirmed callback) {
			OnRoundResultConfirmedEvent -= callback;
		}
		public static void RemoveGameResultConfirmedListener(GameResultConfirmed callback) {
			OnGameResultConfirmedEvent -= callback;
		}
	}
}