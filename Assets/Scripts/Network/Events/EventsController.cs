using System.Collections.Generic;
using System.Linq;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;

namespace Network.Events {
	public static partial class EventsController {
		public delegate void DefaultEvent();
		public delegate void OrdersData(List<OrderData>                 data);
		public delegate void OrdersId(List<int>                         orders);
		public delegate void MoneyChanged(List<ServerData.MoneyChanged> moneyChanged);
		public delegate void UpdateLoans(List<LoanData>                 loans);
		public delegate void UpdateChartering(List<CharteringData>      chartering);
		public delegate void UpdateStorage(List<StorageData>            storage);
		public delegate void UpdateTransfer(List<TransferData>          transfer);
		public delegate void UpdateBuildingStage(BuildedData            buildedData);
		public delegate void SetBuilding(SetBuildingData                building);
		public delegate void UpdateRoundTimer(int                       timer);
		public delegate void ChangeGameState(List<string>               reason);
		public delegate void Disaster(int                               id);
		public delegate void GameEvent(int                              id);
		public delegate void Auction(string                             lot);
		public delegate void RoundResultConfirmed(RoundResultData       data);
		public delegate void GameResultConfirmed(GameResultData         data);
		public delegate void AuctionResponse();

		private static event DefaultEvent         OnConnectionLostEvent;
		private static event OrdersData           OnOrdersCreateEvent;
		private static event OrdersId             OnOrdersRemovedEvent, OnOrdersPayedEvent;
		private static event MoneyChanged         OnMoneyChangedEvent;
		private static event UpdateLoans          OnLoansUpdateEvent;
		private static event UpdateChartering     OnCharteringUpdateEvent;
		private static event UpdateStorage        OnStorageUpdateEvent;
		private static event UpdateTransfer       OnTransferUpdateEvent;
		private static event UpdateBuildingStage  OnBuildingStageUpdateEvent;
		private static event SetBuilding          OnSetBuildingEvent;
		private static event UpdateRoundTimer     OnUpdateRoundTimerEvent;
		private static event ChangeGameState      OnGamePausedEvent, OnGameResumedEvent;
		private static event Disaster             OnDisasterEvent;
		private static event GameEvent            OnGameEvent_event;
		private static event Auction              OnAuctionEvent;
		private static event AuctionResponse      OnAuctionConfirmEvent, OnAuctionRejectEvent;
		private static event RoundResultConfirmed OnRoundResultConfirmedEvent;
		private static event GameResultConfirmed  OnGameResultConfirmedEvent;


		#region Processing Server Events

		private static ServerData.Event.Event ParseResponse(string response) {
			return JsonConvert.DeserializeObject<ServerData.Event.Event>(response);
		}


		private static bool CheckEmptyResponse(string response) {
			return response == "connected" || response == "[]";
		}


		/// <summary>
		/// Обработка полученных событий
		/// </summary>
		public static void ProcessEvents(string response) {
			if (CheckEmptyResponse(response)) return;


			var debugEvents = JsonConvert.DeserializeObject<Dictionary<string, object>>(response);
			if (debugEvents.Count >= 2 || !debugEvents.ContainsKey("round_time_left")) {
				Debug.Log($"Event: {response}");
			}

			var events = ParseResponse(response);

			if (events.OrdersCreated != null) OnOrdersCreated(events.OrdersCreated);
			if (events.OrderRemoved  != null) OnOrdersRemoved(events.OrderRemoved);
			if (events.OrderPayed    != null) OnOrdersPayed(events.OrderPayed);

			if (events.GamePaused       != null) OnGamePaused(events.GamePaused);
			if (events.GameResumed      != null) OnGameResumed(events.GameResumed);
			if (events.UpdateRoundTimer != int.MaxValue) OnUpdateRoundTimer(events.UpdateRoundTimer);

			if (events.MoneyChanged         != null) OnMoneyChanged(events.MoneyChanged);
			if (events.CharteringData       != null) OnCharteringUpdate(events.CharteringData);
			if (events.LoanData             != null) OnLoansUpdate(events.LoanData);
			if (events.StorageData          != null) OnStorageUpdate(events.StorageData);
			if (events.TransferData         != null) OnTransferUpdate(events.TransferData);
			if (events.BuildedData          != null) OnBuildingStageUpdate(events.BuildedData);
			if (events.SetBuildingData      != null) OnSetBuilding(events.SetBuildingData);
			if (events.Disaster             != null) OnDisaster(events.Disaster.DisasterId);
			if (events.GameEvents           != null) OnGameEvent(events.GameEvents.Last());
			if (events.RoundResultConfirmed != null) OnRoundResultConfirmed(new RoundResultData {Teams = events.RoundResultConfirmed});
			if (events.GameResult           != null) OnGameResultConfirmed(events.GameResult);

			if (!string.IsNullOrEmpty(events.Auction)) OnAuction(events.Auction);
			if (events.AuctionConfirm != null) OnAuctionConfirm();
			if (events.AuctionReject  != 0) OnAuctionReject();

			if (events.Other != null && events.Other.Count > 0) ProcessOtherEvents(events.Other);
		}


		private static void ProcessOtherEvents(List<string> events) {
			foreach (var e in events) {
				/*switch (e) {
					case "all_watched_intro":
						OnAllWatchedIntro();
						break;
					case "all_picked_team":
						OnAllTeamPicked();
						break;
				}*/
			}
		}

		#endregion


		public static void ResetEvent(DefaultEvents eventType) {
			switch (eventType) {
				case DefaultEvents.ConnectionLost: {
					OnConnectionLostEvent = null;
					break;
				}
			}
		}


		public static void ResetAllEvents() {
			OnConnectionLostEvent = null;

			OnOrdersCreateEvent  = null;
			OnOrdersRemovedEvent = null;
			OnOrdersPayedEvent   = null;

			OnMoneyChangedEvent         = null;
			OnLoansUpdateEvent          = null;
			OnCharteringUpdateEvent     = null;
			OnStorageUpdateEvent        = null;
			OnTransferUpdateEvent       = null;
			OnBuildingStageUpdateEvent  = null;
			OnSetBuildingEvent          = null;
			OnUpdateRoundTimerEvent     = null;
			OnDisasterEvent             = null;
			OnRoundResultConfirmedEvent = null;
			OnGameResultConfirmedEvent  = null;
			OnGameEvent_event           = null;

			OnAuctionEvent        = null;
			OnAuctionConfirmEvent = null;
			OnAuctionRejectEvent  = null;

			OnGamePausedEvent  = null;
			OnGameResumedEvent = null;
		}


		public enum DefaultEvents {
			ConnectionLost,
		}
	}
}