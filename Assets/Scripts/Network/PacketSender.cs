using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Network {
	public static class PacketSender {
		/// <summary>
		/// Получение токена от сервера
		/// </summary>
		public static void Auth(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.Auth, onSuccess, onError);
		}


		/// <summary>
		/// Проверка соединения
		/// </summary>
		public static void CheckConnection(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.CheckConnection, onSuccess, onError, Random.value.ToString());
		}


		/// <summary>
		/// Получение списка игр
		/// </summary>
		public static void GetGames(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GamesList, onSuccess, onError);
		}


		/// <summary>
		/// Получает информацию об игре
		/// </summary>
		public static void GetGame(int gameId, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GetGame, onSuccess, onError, gameId.ToString());
		}


		/// <summary>
		/// Подключение к игре
		/// </summary>
		public static void SetGame(int gameId, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.SetGame, onSuccess, onError, gameId.ToString());
		}


		/// <summary>
		/// Подключение к игре
		/// </summary>
		public static void LoadCache(string table, string[] fields, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			var fieldsRow = String.Join(",", fields);
			NetworkManager.SendPacket(Packets.Packet.LoadCache, onSuccess, onError, table, fieldsRow);
		}

		public static void LoadCache(string table, string[] fields, string sqlCondition = "", NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			var fieldsRow = String.Join(",", fields);
			NetworkManager.SendPacket(Packets.Packet.LoadCache, onSuccess, onError, table, fieldsRow, sqlCondition);
		}


		/// <summary>
		/// Подключение к игре
		/// </summary>
		public static void UpdateCache(string table, int rowId, string field, string value, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.UpdateCache, onSuccess, onError, table, rowId.ToString(), field, value);
		}


		/// <summary>
		/// Подключение к игре
		/// </summary>
		public static void CreateRow(string table, string[] fields, string[] values, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			var fieldsRow = String.Join(",", fields);
			var valuesRow = String.Join(",", values);
			NetworkManager.SendPacket(Packets.Packet.CreateRow, onSuccess, onError, table, fieldsRow, valuesRow);
		}


		/// <summary>
		/// Получение свободных команд
		/// </summary>
		public static void GetAvailableTeams(int gameId, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GetAvailableTeams, onSuccess, onError, gameId.ToString());
		}


		/// <summary>
		/// Привязка устройства к команде
		/// </summary>
		public static void SetTeam(int teamId, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.SetTeam, onSuccess, onError, teamId.ToString());
		}


		/// <summary>
		/// Получение построенных зданий
		/// </summary>
		public static void GetBuildings(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GetBuildings, onSuccess, onError);
		}


		/// <summary>
		/// Установка здания в зоне
		/// </summary>
		public static void SetBuilding(int buildingId, int areaId, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.SetBuilding, onSuccess, onError, buildingId.ToString(), areaId.ToString());
		}


		/// <summary>
		/// Обновление этапа стройки
		/// </summary>
		public static void ChangeBuildingStage(int stage, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.ChangeBuildingStage, onSuccess, onError, stage.ToString());
		}


		/// <summary>
		/// Обновление этапа стройки
		/// </summary>
		public static void SetBlocks(int blocks, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.SetBlocks, onSuccess, onError, blocks.ToString());
		}


		/// <summary>
		/// Выбор ролей на следующий раунд
		/// </summary>
		public static void SetRoles(IEnumerable<string> players, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			var pString = string.Join(",", players);
			NetworkManager.SendPacket(Packets.Packet.SetRoles, onSuccess, onError, pString);
		}


		/// <summary>
		/// Получение отправленных ролей
		/// </summary>
		public static void GetRoles(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GetRoles, onSuccess, onError);
		}


		/// <summary>
		/// Получение имен всех директоров выбранных ранее
		/// </summary>
		public static void GetDirectors(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GetDirectors, onSuccess, onError);
		}


		/// <summary>
		/// Создание заказа
		/// </summary>
		public static void CreateOrder(int[] materialsId, int[] quantity, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			var materialsRow = String.Join(",", materialsId);
			var quantityRow = String.Join(",", quantity);
			NetworkManager.SendPacket(Packets.Packet.CreateOrder, onSuccess, onError, materialsRow, quantityRow);
		}


		/// <summary>
		/// Удаление заказа
		/// </summary>
		public static void RemoveOrder(int orderId, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.RemoveOrder, onSuccess, onError, orderId.ToString());
		}


		/// <summary>
		/// Оплата заказа
		/// </summary>
		public static void PayOrder(int orderId, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.PayOrder, onSuccess, onError, orderId.ToString());
		}


		/// <summary>
		/// Загрузка заказов
		/// </summary>
		public static void LoadOrders(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.LoadOrders, onSuccess, onError);
		}


		/// <summary>
		/// Загрузка склада
		/// </summary>
		public static void LoadStorage(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.LoadStorage, onSuccess, onError);
		}


		/// <summary>
		/// Изменение кол-ва материала
		/// </summary>
		public static void UseMaterial(int materialId, int amount, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.UseMaterial, onSuccess, onError, materialId.ToString(), amount.ToString());
		}


		/// <summary>
		/// Загрузка записей о приглашениях в доставку
		/// </summary>
		public static void CharteringLoad(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.LoadChartering, onSuccess, onError);
		}


		/// <summary>
		/// Приглашение на совместную доставку
		/// </summary>
		public static void CharteringInvite(int recipientTeam, int shipId, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.CharteringInvite, onSuccess, onError, recipientTeam.ToString(), shipId.ToString());
		}


		/// <summary>
		/// Принятие приглашения на совместную доставку
		/// </summary>
		public static void CharteringConfirm(int confirmedTeam, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.CharteringConfirm, onSuccess, onError, confirmedTeam.ToString());
		}


		/// <summary>
		/// Отклонение приглашения на совместную доставку
		/// </summary>
		public static void CharteringReject(int rejectedTeam, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.CharteringReject, onSuccess, onError, rejectedTeam.ToString());
		}


		/// <summary>
		/// Отмена приглашения на совместную доставку
		/// </summary>
		public static void CharteringCancel(int recipientTeam, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.CharteringCancel, onSuccess, onError, recipientTeam.ToString());
		}


		/// <summary>
		/// Отправка заказов
		/// </summary>
		public static void SendShip(int shipId, IEnumerable<string> orders, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			var ordersString = string.Join(",", orders);
			NetworkManager.SendPacket(Packets.Packet.SendShip, onSuccess, onError, shipId.ToString(), ordersString);
		}


		/// <summary>
		/// Создание заявки на кредит
		/// </summary>
		public static void RequestLoan(int sum, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.RequestLoan, onSuccess, onError, sum.ToString());
		}


		/// <summary>
		/// Выплата суммы по кредиту
		/// </summary>
		public static void PayLoan(int id, int sum, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.PayLoan, onSuccess, onError, id.ToString(), sum.ToString());
		}


		/// <summary>
		/// Загрузка кредитов
		/// </summary>
		public static void LoadLoans(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.LoadLoans, onSuccess, onError);
		}


		/// <summary>
		/// Загрузка информации о перечислениях денег между командами
		/// </summary>
		public static void LoadTransfers(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.LoadTransfers, onSuccess, onError);
		}


		/// <summary>
		/// Перечисление денег другой команде
		/// </summary>
		public static void SendMoney(int teamId, int sum, int oneTimePay, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.SendMoney, onSuccess, onError, teamId.ToString(), sum.ToString(), oneTimePay.ToString());
		}


		/// <summary>
		/// Отмена ежегодного перечисления денег другой команде
		/// </summary>
		public static void CancelTransfer(int id, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.CancelTransfer, onSuccess, onError, id.ToString());
		}


		/// <summary>
		/// Обновление таймеров команды
		/// </summary>
		public static void UpdateTimer(Roles.PlayerRoles role, int timer, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.UpdateTimer, onSuccess, onError, ((int)role).ToString(), timer.ToString());
		}


		/// <summary>
		/// Готовность к переходу в следующий раунд
		/// </summary>
		public static void RoundReady(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.RoundReady, onSuccess, onError);
		}


		/// <summary>
		/// Сообщает серверу о том, что на этом устройстве игра загрузилась
		/// </summary>
		public static void GameLoaded(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GameLoaded, onSuccess, onError);
		}


		/// <summary>
		/// Завершение раунда
		/// </summary>
		public static void EndRound(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.EndRound, onSuccess, onError);
		}


		/// <summary>
		/// Ставка на аукционе
		/// </summary>
		public static void AuctionBet(int bet, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.AuctionBet, onSuccess, onError, bet.ToString());
		}


		/// <summary>
		/// Получение текущего лота
		/// </summary>
		public static void GetAuctionLot(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GetAuctionLot, onSuccess, onError);
		}


		/// <summary>
		/// Покупка страховки
		/// </summary>
		public static void BuyInsurance(int id, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.BuyInsurance, onSuccess, onError, id.ToString());
		}


		/// <summary>
		/// Получение купленной страховки в текущем раунде
		/// </summary>
		public static void GetInsurance(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GetInsurance, onSuccess, onError);
		}


		/// <summary>
		/// Получение результатов раунда
		/// </summary>
		public static void GetRoundResult(int round = 0, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			if (round == 0) {
				NetworkManager.SendPacket(Packets.Packet.GetRoundResult, onSuccess, onError);
			}
			else {
				NetworkManager.SendPacket(Packets.Packet.GetRoundResult, onSuccess, onError, round.ToString());
			}
		}


		/// <summary>
		/// Отправка очков за подбор персонала
		/// </summary>
		public static void SetHRScore(int score, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.SetHRScore, onSuccess, onError, score.ToString());
		}


		/// <summary>
		/// Отправлялись ли в текущем раунде очки за выбор компетенций
		/// </summary>
		public static void CheckHRScore(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.CheckHRScore, onSuccess, onError);
		}


		/// <summary>
		/// Получение установленных водных горок
		/// </summary>
		public static void GetSlides(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GetSlides, onSuccess, onError);
		}


		/// <summary>
		/// Установка водных горок
		/// </summary>
		public static void SetSlides(int slides, NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.SetSlides, onSuccess, onError, slides.ToString());
		}


		/// <summary>
		/// Получение результатов игры
		/// </summary>
		public static void GetResults(NetworkManager.RequestCallback onSuccess = null, NetworkManager.RequestCallback onError = null) {
			NetworkManager.SendPacket(Packets.Packet.GetResults, onSuccess, onError);
		}
	}
}