using System.Collections;
using Network.Events;
using UnityEngine;

namespace Network {
	public class Session : MonoBehaviour {
		public static bool Active;

		private const  float     CheckConnectionTime = 1f;
		private static Session   _instance;
		private bool _checking;


		private void Update() {
			if (Input.GetKeyDown(KeyCode.Escape)) {
				NetworkManager.CloseConnection();
			}
		}


		public static void StartSession(string token) {
			NetworkManager.AuthToken = token;
			CreateSession();
		}


		public static void StopSession() {
			NetworkManager.AuthToken = string.Empty;
			DestroySession();
		}


		private static void CreateSession() {
			Active = true;

			if (_instance) Destroy(_instance.gameObject);
			_instance = new GameObject("Session").AddComponent<Session>();
			_instance.StartCoroutine(_instance.CheckConnection());
			DontDestroyOnLoad(_instance.gameObject);
		}


		private static void DestroySession() {
			if (_instance) Destroy(_instance.gameObject);
			Active = false;
		}


		private IEnumerator CheckConnection() {
			while (!string.IsNullOrEmpty(NetworkManager.AuthToken)) {
				if (!_checking) {
					_checking = true;
					PacketSender.CheckConnection(OnCheckSuccess, OnCheckFailed);
				}
				yield return new WaitForSeconds(CheckConnectionTime);
			}
		}


		private void OnCheckSuccess(WebResponse response) {
			_checking = false;
			EventsController.ProcessEvents(response.Text);
		}


		private void OnCheckFailed(WebResponse response) {
			_checking = false;
		}
	}
}