namespace Network {
	public struct WebResponse {
		public string Text;
		public byte[] Data;
		public string Error;
		public long   Code;


		public WebResponse(string text, byte[] data, string error, long code) {
			Error = error;
			Text  = text;
			Data  = data;
			Code = code;
		}


	}
}