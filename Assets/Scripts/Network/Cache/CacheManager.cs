using Network.Cache.Data;

namespace Network.Cache {
	public class CacheManager {
		public static GameDataCache GameDataCache = new GameDataCache ();
		public static TeamDataCache TeamDataCache = new TeamDataCache ();

		public static bool CacheLoaded => GameDataCache.IsLoaded && TeamDataCache.IsLoaded;

		public delegate void OnCacheLoaded();

		private static event OnCacheLoaded CacheLoadEvent;


		public static void LoadCache() {
			GameDataCache.Load();
			TeamDataCache.Load();
		}


		public static void Reset() {
			GameDataCache = new GameDataCache();
			TeamDataCache = new TeamDataCache();
			CacheLoadEvent = null;
		}


		public static void AddCacheLoadedListener(OnCacheLoaded onLoaded) {
			CacheLoadEvent += onLoaded;
		}


		public static void OnCacheLoadedEvent() {
			if(!CacheLoaded) return;
			CacheLoadEvent?.Invoke();
		}
	}
}