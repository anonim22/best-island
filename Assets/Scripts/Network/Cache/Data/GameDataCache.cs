using System;
using System.Collections.Generic;
using System.Linq;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;

namespace Network.Cache.Data {
	public class GameDataCache : CacheData {
		public int      GameId;
		public GameData Data;
		public int      ID => Data.ID;
		public bool GameIsStarted {
			get => Data.GameIsStarted;
			set => Data.GameIsStarted = value;
		}
		public bool GameIsEnded {
			get => Data.GameIsEnded;
			set => Data.GameIsEnded = value;
		}
		public bool RoundIsStarted {
			get => Data.RoundIsStarted;
			set => Data.RoundIsStarted = value;
		}
		public bool GameIsPaused => Data.GameIsPaused;
		public bool AdminPause   => Data.AdminPause;

		public int TeamsCount     => Data.TeamsCount;
		public int GameTemplateId => Data.GameTemplateId;
		public int Round          => Data.Round;

		public Templates Templates { get; private set; }


		public void Load() {
			base.Load("game_data", new string[] { }, $"id={GameId}");
		}


		protected override void OnLoadCache(WebResponse response) {
			Debug.Log(response.Text);

			var list = JsonConvert.DeserializeObject<Dictionary<string, List<GameData>>>(response.Text);
			Data = list.Single().Value.Single();
			base.OnLoadCache(response);
		}


		public void LoadTemplates(WebResponse response) {
			Templates = JsonConvert.DeserializeObject<Templates>(response.Text.Replace("null", "0"));
		}
	}
}