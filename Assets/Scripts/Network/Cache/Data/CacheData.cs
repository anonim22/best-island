using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Network.Cache.Data {
	public class CacheData {
		public    bool   IsLoaded { get ; private set; }
		protected string Table;

		public delegate void OnUpdated();

		public event OnUpdated OnUpdatedEvent;


		protected void Load(string tableName, string[] fields, string where = "") {
			if (string.IsNullOrEmpty(tableName)) throw  new Exception("Table name is empty.");
			//if (fields == null || fields.Length == 0) throw new Exception("Fields not defined.");

			Table = tableName;

			if (string.IsNullOrEmpty(where))
				PacketSender.LoadCache(tableName, fields, OnLoadCache, OnLoadCacheFail);
			else
				PacketSender.LoadCache(tableName, fields, where, OnLoadCache, OnLoadCacheFail);
		}


		/*protected void Set(string field, string value, int rowId) {
			Rows[field] = value;
			UpdateCache(field, value, rowId);
		}


		protected string Get(int rowId, string field) {
			return Rows[rowId].Fields[field];
		}*/


		protected void UpdateCache(string field, string value, int rowId) {
			PacketSender.UpdateCache(Table, rowId, field, value);
		}


		protected virtual void OnLoadCache(WebResponse response) {
			IsLoaded = true;

			if (CacheManager.CacheLoaded) CacheManager.OnCacheLoadedEvent();
		}


		private void OnLoadCacheFail(WebResponse response) {
			Debug.LogError($"Error loading cache from table {Table}.");
			Debug.LogError($"{response.Text}:{response.Error}:{response.Code}");
			if(response.Code != 503) NetworkManager.CloseConnection();
		}
	}
}