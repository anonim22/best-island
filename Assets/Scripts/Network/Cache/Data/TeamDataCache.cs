using System.Collections.Generic;
using System.Linq;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;

namespace Network.Cache.Data {
	public class TeamDataCache : CacheData {
		public static TeamData ActiveTeam => CacheManager.TeamDataCache._activeTeam;
		public static int ActiveTeamId => CacheManager.TeamDataCache._activeTeam.ID;
		private TeamData _activeTeam;
		public List<TeamData> Teams { get; private set; }
		public int[] Roles = new int[2];


		public void Load() {
			base.Load("team_data", new string[] { }, $"game_id={CacheManager.GameDataCache.GameId}");
		}


		protected override void OnLoadCache(WebResponse response) {
            Debug.Log(response.Text);
			var list = JsonConvert.DeserializeObject<Dictionary<string, List<TeamData>>>(response.Text);
			Teams = list.Single().Value;

			base.OnLoadCache(response);
		}


		public void SetTeam(int teamId)
		{
		    for (var i = 0; i < Teams.Count; i++)
		    {
		        var team = Teams[i];
		        if (team.ID != teamId) continue;

		        _activeTeam = Teams[i];
		        return;
			}

            Debug.LogError($"Team {teamId} not found in cache");
		}
	}
}