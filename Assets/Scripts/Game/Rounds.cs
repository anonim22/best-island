using System;
using System.Collections.Generic;
using Network;
using Network.Cache;
using Network.Cache.Data;
using Network.Events;
using Network.ServerData;
using Newtonsoft.Json;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game {
	public static class Rounds {
		public static List<RoundResultData> RoundResults = new List<RoundResultData>();

		public delegate void                   RoundChangeState(int round);
		private static  event RoundChangeState OnRoundEndEvent;
		private static  event RoundChangeState OnRoundStartEvent;

		public static int Round = 1;


		public static void Init() {
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, Reset);

			Round = CacheManager.GameDataCache.Round;
			if (!CacheManager.GameDataCache.RoundIsStarted) Round = Round - 1;
			Debug.Log($"Round: {Round}");
			SceneManager.sceneLoaded += OnSceneLoaded;
		}


		// Загрузка предыдущих раундов
		public static void Load(Action onLoadCallback) {
			if (Round > 1) {
				LoadPreviousRound(1, onLoadCallback);
			}
			else {
				onLoadCallback?.Invoke();
			}
		}


		// Загрузка переданного раунда и последующих, если доступны, кроме текущего.
		// Вызывает onLoadCallback после загрузки последнего раунда
		private static void LoadPreviousRound(int loadRound, Action onLoadCallback) {
			LoadRoundResult(loadRound, delegate (RoundResultData data) {
				RoundResults.Add(data);
				if (loadRound >= Round - 1) onLoadCallback.Invoke();
				else LoadPreviousRound(loadRound + 1, onLoadCallback);
			});
		}


		private static void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
			SceneManager.sceneLoaded -= OnSceneLoaded;

			GameManager.OnRoundEnd   += OnRoundEnd;
			GameManager.OnRoundStart += OnRoundStart;

			if (GameManager.RoundEnd || !CacheManager.GameDataCache.GameIsStarted) {
				if (Round >= 1) {
					LoadRoundResult(Round, delegate (RoundResultData data) {
						RoundResults.Add(data);
						UIManager.GameRoundResults.ShowResult(data);
						if(GameManager.DeviceNum == 1) UIManager.RolePick.CheckRoles();
					});
				}
				else if(GameManager.DeviceNum == 1) {
					UIManager.RolePick.CheckRoles();
				}
			}
		}


		public static void AddRoundStartListener(RoundChangeState callback) {
			OnRoundStartEvent += callback;
		}


		public static void AddRoundEndListener(RoundChangeState callback) {
			OnRoundEndEvent += callback;
		}


		private static void OnRoundStart() {
//			Debug.Log($"OnRoundStart. RoundEnd: {GameManager.RoundEnd}");

			// Если до события раунд не был завершен, то выход (защита от дубля события)
			if (!GameManager.RoundEnd) return;

			Round += 1;
			CacheManager.GameDataCache.Data.RoundIsStarted = true;

			if (!CacheManager.GameDataCache.GameIsStarted) {
				CacheManager.GameDataCache.GameIsStarted = true;
			}

			Debug.Log($"Round {Round} started");
			OnRoundStartEvent?.Invoke(Round);
		}


		private static void OnRoundEnd() {
			// Если до события раунд был завершен, то выход (защита от дубля события)
			if (GameManager.RoundEnd) return;

			Debug.Log($"Round {Round} ended");
			CacheManager.GameDataCache.Data.RoundIsStarted = false;
			OnRoundEndEvent?.Invoke(Round);

			LoadRoundResult(Round, delegate (RoundResultData data) {
				RoundResults.Add(data);
				UIManager.GameRoundResults.ShowResult(data);
				if(GameManager.DeviceNum == 1 && Round < 4) UIManager.RolePick.ShowPanel();
			});
		}


		private static void LoadRoundResult(int round, Action<RoundResultData> callback) {
			PacketSender.GetRoundResult(round, delegate (WebResponse r) {
				Debug.Log(r.Text);
				if (r.Text == "waiting for admin" || r.Text == "waiting for round") {
					if (round < Round) {
						Debug.LogError($"В раунде {round} не была выставлена оценка директору");
						var d = new RoundResultData {Round = round };
						callback?.Invoke(d);
						return;
					}

					GameManager.WaitingRoundResult = true;
					EventsController.AddRoundResultConfirmedListener(delegate (RoundResultData result) {
						result.Round = round;
						callback?.Invoke(result);
						GameManager.WaitingRoundResult = false;
					});
					return;
				}

				var data = RoundResultData.Parse(r);
				data.Round = round;
				callback?.Invoke(data);
			});
		}


		private static void Reset() {
			Round = 0;
			RoundResults.Clear();

			OnRoundStartEvent = null;
			OnRoundEndEvent   = null;
		}
	}
}