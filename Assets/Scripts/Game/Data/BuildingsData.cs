using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Data {
	[CreateAssetMenu(fileName = "Buildings", menuName = "Buildings Data")]
	public class BuildingsData : ScriptableObject {
		public List<Building> Buildings = new List<Building>();
		public GameObject[]   Stages    = new GameObject[4];

		public static BuildingsData Data {
			get {
				if (_data != null) return _data;
				_data = Resources.Load<BuildingsData>("Buildings");
				return _data;
			}
		}
		private static BuildingsData _data;


		public Building GetBuilding(int buildingId) {
			var building = Buildings.FirstOrDefault(b => b.Id == buildingId);
			if(building == null) Debug.LogError($"Building with id {buildingId} not found");
			return building;
		}
	}

	[Serializable]
	public class Building {
		public int          Id;
		public string       Name;
		public GameObject   Model;
		public GameObject[] Stages = new GameObject [4];
	}
}