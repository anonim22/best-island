using System;
using System.Collections.Generic;
using System.Linq;
using Network;
using Network.Events;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;

namespace Game.Data {
	public static class Transfers {
		public static List<TransferData> TransferData = new List<TransferData>();


		public static void Init() {
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, ResetData);
			EventsController.AddTransferUpdateListener(Update);
		}


		public static void Load(Action callback = null) {
			PacketSender.LoadTransfers(delegate (WebResponse response) {
				Debug.Log(response.Text);
				TransferData = JsonConvert.DeserializeObject<List<TransferData>>(response.Text);
				callback?.Invoke();
			});
		}


		private static void Update(List<TransferData> data) {
			TransferData = data;
		}


		private static void ResetData() {
			TransferData.Clear();
		}
	}
}