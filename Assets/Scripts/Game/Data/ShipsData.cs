using System;
using System.Collections.Generic;
using System.Linq;
using Network;
using Newtonsoft.Json;
using UnityEngine;

namespace Game.Data {
	[CreateAssetMenu(fileName = "Ships", menuName = "Ships Data")]
	public class ShipsData : ScriptableObject {
		private static ShipsData Data
		{
			get
			{
				if (_data != null) return _data;
				_data = Resources.Load<ShipsData>("Ships");
				return _data;
			}
		}
		private static ShipsData _data;
		public List<Ship> Ships = new List<Ship>();


		public static Ship GetShip(int shipId) {
			return Data.Ships.Single(s => s.Id == shipId);
		}
	}


	[Serializable]
	public class Ship {
		public int Id;
		public Sprite Icon;
		public string Name;
		public int Capacity;
		public float RentCost;
		public int RentedTeam;
	}
}