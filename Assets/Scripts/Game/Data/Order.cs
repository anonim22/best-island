﻿using System;
using System.Collections.Generic;
using System.Linq;
using Network;
using Network.Events;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;
using EventsController = Network.Events.EventsController;

namespace Game.Data {
	public class Order {
		public delegate void UpdateOrder();

		public static event UpdateOrder OnUpdateOrderEvent;

		public static List<Order> Orders = new List<Order>();

		public int                 Id;
		public int                 OrderId;
		public int                 TeamId;
		public bool                Payed;
		public List<OrderMaterial> Materials = new List<OrderMaterial>();


		public static void Init() {
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, ResetOrders);
			EventsController.AddOrdersCreatedListener(OnOrdersCreated);
			EventsController.AddOrdersRemovedListener(OnOrdersRemoved);
			EventsController.AddOrdersPayedListener(OnOrdersPayed);
		}


		public float GetSum() {
			return Materials.Sum(m => m.PriceForPiece * m.Quantity);
		}


		/// <summary>
		/// Вес в тоннах
		/// </summary>
		public float GetWeight() {
			return Materials.Sum(m => m.Weight * m.Quantity) / 1000;
		}


		public static void AddOrder(Order order) {
			if (Orders.Any(o => o.Id == order.Id)) return;

			Orders.Add(order);
			UpdateOrderEvent();
		}


		public static void RemoveOrder(Order order) {
			Debug.Log("Order removed");
			Orders.Remove(order);
			UpdateOrderEvent();
		}


		public static void RemoveOrder(int orderId) {
			var order = Orders.FirstOrDefault(o => o.Id == orderId);
			if (order == null) return;

			Orders.Remove(order);
			UpdateOrderEvent();
		}


		public static void SetPayedOrder(int orderId) {
			var order = Orders.FirstOrDefault(o => o.Id == orderId);
			if (order == null || order.Payed) return;

			order.Payed = true;
			UpdateOrderEvent();
		}


		public static void LoadOrders(Action callback = null) {
			PacketSender.LoadOrders(delegate (WebResponse response) {
				Debug.Log(response.Text);
				Orders.Clear();
				var data = JsonConvert.DeserializeObject<List<OrderData>>(response.Text);
				data.ForEach(order => AddOrder(OrderData.Parse(order)));
				callback?.Invoke();
			});
		}


		public static void ResetOrders() {
			OnUpdateOrderEvent = null;
			Orders.Clear();
		}


		private static void OnOrdersCreated(List<OrderData> data) {
			data?.ForEach(o => AddOrder(OrderData.Parse(o)));
		}


		private static void OnOrdersRemoved(List<int> orders) {
			orders.ForEach(RemoveOrder);
		}


		private static void OnOrdersPayed(List<int> orders) {
			orders.ForEach(SetPayedOrder);
		}


		private static void UpdateOrderEvent() {
			OnUpdateOrderEvent?.Invoke();
		}
	}
}