using System;
using System.Collections.Generic;
using System.Linq;
using Network;
using Network.Events;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;

namespace Game.Data {
	public static class Storage {
		public static List<Material> Materials = new List<Material>();

		public delegate void StorageUpdate();
		public static event StorageUpdate OnStorageUpdate;


		public static void Init() {
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, Reset);
			EventsController.AddStorageUpdateListener(UpdateStorage);
		}


		public static void LoadStorage(Action callback = null) {
			PacketSender.LoadStorage(delegate (WebResponse response) {
				Debug.Log(response.Text);
				Materials = JsonConvert.DeserializeObject<List<Material>>(response.Text);
				callback?.Invoke();
			});
		}


		public static void UseMaterial(int id, int amount, bool updateOnServer = false) {
			var mat = Materials.SingleOrDefault(m => m.Id == id);
			if(mat == null) return;

			mat.Quantity -= amount;
			OnStorageUpdate?.Invoke();

			if (updateOnServer) {
				PacketSender.UseMaterial(id, amount);
			}
		}


		private static void UpdateStorage(List<StorageData> update) {
			var data     = update.Last();
			var ids      = data.Materials.Split(',');
			var quantity = data.Quantity.Split(',');

			Materials.Clear();
			for (int i = 0; i < ids.Length; i++) {
				Materials.Add(new Material {Id = int.Parse(ids[i]), Quantity = int.Parse(quantity[i])});
			}

			OnStorageUpdate?.Invoke();
		}


		private static void Reset() {
			OnStorageUpdate = null;
			Materials.Clear();
		}


		public class Material {
			[JsonProperty("material_id")] public int Id;
			[JsonProperty("amount")] public int Quantity;
		}
	}
}