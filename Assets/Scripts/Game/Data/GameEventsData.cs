using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Data {
	[CreateAssetMenu(fileName = "Events", menuName = "Events Data")]
	public class GameEventsData : ScriptableObject {
		[SerializeField] private List<Event> Events = new List<Event>();

		private static GameEventsData Data {
			get {
				if (_data != null) return _data;
				_data = Resources.Load<GameEventsData>("Events");
				return _data;
			}
		}
		private static GameEventsData _data;


		public static Event GetEvent(int eventId) {
			return Data.Events.FirstOrDefault(b => b.Id == eventId);
		}
	}

	[Serializable]
	public class Event {
		public int    Id;
		public string Name;
		public string Description;
	}
}