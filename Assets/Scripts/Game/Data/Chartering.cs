using System;
using System.Collections.Generic;
using System.Linq;
using Network;
using Network.Cache;
using Network.Cache.Data;
using Network.Events;
using Network.ServerData;
using Newtonsoft.Json;
using UI;
using UnityEngine;

namespace Game.Data {
	public static class Chartering {
		public static List<CharteringData> CharteringData = new List<CharteringData>();


		public static void Init() {
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, ResetData);
			EventsController.AddCharteringUpdateListener(Update);
		}


		public static void Load(Action callback = null) {
			PacketSender.CharteringLoad(delegate (WebResponse response) {
				Debug.Log(response.Text);
				CharteringData = JsonConvert.DeserializeObject<List<CharteringData>>(response.Text);
				callback?.Invoke();
			});
		}


		private static void Update(List<CharteringData> data) {
			// Удаление старых записей
			var tmpData = new List<CharteringData>(data);
			CharteringData.ForEach(delegate (CharteringData c) {
				var remove = tmpData.FirstOrDefault(tmp => tmp.Id == c.Id);
				if (remove != null) tmpData.Remove(remove);
			});

			// Удаление записей, где текущая команда создавала приглашение
			for (int i = 0; i < tmpData.Count; i++) {
				var cData = tmpData[i];
				if (cData.SenderId == TeamDataCache.ActiveTeamId) {
					tmpData.RemoveAt(i);
					i -= 1;
				}
			}

			// Если есть новые приглашения текущей команде
			if (tmpData.Count > 0) {
				var teamId = tmpData.First().SenderId;
				var teamName = CacheManager.TeamDataCache.Teams.Single(t => t.ID == teamId).TeamName;
				UIManager.EventPanel.SetText($"Вас приглашает команда {teamName} в совместную доставку");
				UIManager.EventPanel.ShowPanel();
			}

			Order.LoadOrders();
			CharteringData = data;
		}


		private static void ResetData() {
			CharteringData.Clear();
		}
	}
}