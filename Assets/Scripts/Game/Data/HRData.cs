using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Data {
	[CreateAssetMenu(fileName = "HRData", menuName = "HR Data")]
	public class HRData : ScriptableObject {
		[SerializeField] private List<Employees> Employees = new List<Employees>();

		private static HRData Data {
			get {
				if (_data != null) return _data;
				_data = Resources.Load<HRData>("HRData");
				return _data;
			}
		}
		private static HRData _data;


		public static Employees GetEmployees(int buildingId) {
			return Data.Employees.FirstOrDefault(e => e.BuildingId == buildingId);
		}


		public static Dictionary<int, string> Competencies = new Dictionary<int, string>() {
			{0, "АнализПринятиеРешения"},
			{1, "Быстрота реакции"},
			{2, "Внимательность к деталям"}, // Убрано
			{3, "Высшее тех образование"},
			{4, "Знание анатомии и физиол"},
			{5, "Знание норм-ов по ПБ"},
			{6, "Знание правил и законов "},
			{7, "Знание рынка недвиж-ти "},
			{8, "ЗнаниеСтанд-в обслуж-ия "},
			{9, "Инспекторские проверки"},
			{10, "Клиентоориентир-ть"},
			{11, "Координация и взаимод-е"},
			{12, "Креативность"},
			{13, "Критическое мышление"}, // Убрано
			{14, "Навык договорной работы"},
			{15, "Нацелен-ть на результат"}, // Убрано
			{16, "Нацеленность на результат"},
			{17, "Ответственность"},
			{18, "Принятие решений"},
			{19, "Свободный английский"},
			{20, "Скор.  принятия  решения"},
			{21, "Тех нормы, правила монит"},
			{22, "Умение вести переговоры "},
			{23, "Управление людьми"},
			{24, "Хорошая физ подготовка"},
			{25, "Четкая постановка задач"},
			{26, "Эмоционал интеллект"},

			{27, "Актерское маустерство"},
			{28, "Аналитическое мышление"},
			{29, "Коммуникативные навыки"},
			{30, "Правила сервировки столов"},
			{31, "ПринципыАвиаБезопасности"},
			{32, "ПринципыСистемногоАнализа"},
		};
	}

	[Serializable]
	public class Employees {
		public int BuildingId;
		public EmployeeData Employee1;
		public EmployeeData Employee2;

		[Serializable]
		public class EmployeeData {
			public string EmployeePosition;
			public string EmployeeDescription;
			public int[]  Competencies;
		}
	}
}