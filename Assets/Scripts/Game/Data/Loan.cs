using System;
using System.Collections.Generic;
using Network;
using Network.Events;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;

namespace Game.Data {
	public static class Loan {
		public static List<LoanData> Loans = new List<LoanData>();


		public static void Load(Action callback = null) {
			PacketSender.LoadLoans(delegate (WebResponse response) {
				Debug.Log(response.Text);
				Loans = JsonConvert.DeserializeObject<List<LoanData>>(response.Text);
				callback?.Invoke();
			});
		}

		public static void Init() {
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, ResetLoans);
			EventsController.AddLoansUpdateListener(UpdateLoans);
		}


		private static void UpdateLoans(List<LoanData> loans) {
			Loans = loans;
		}


		private static void ResetLoans() {
			Loans.Clear();
		}
	}
}