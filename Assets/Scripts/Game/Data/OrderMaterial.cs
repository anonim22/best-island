﻿using System.Collections;
using System.Collections.Generic;
using Game.Data;
using Game.Island;
using UI.MaterialShop;
using UnityEngine;

public class OrderMaterial
{
    public int Id;
    public string Name;
    public int Quantity;
    public float PriceForPiece;
    public float Weight;

    public OrderMaterial(int id, string name, int quantity, float priceForPiece, float weight)
    {
        Id = id;
        Name = name;
        Quantity = quantity;
        PriceForPiece = priceForPiece;
        Weight = weight;
    }

    public OrderMaterial(MaterialsData.Material material)
    {
        Id = material.Id;
        Name = material.Name;
        PriceForPiece = material.Price;
        Weight = material.Weight;
    }

    public OrderMaterial(MaterialCard materialCard)
    {
        MaterialsData.Material material = MaterialsData.Data.Materials[materialCard.MaterialId];
        Id = material.Id;
        Name = material.Name;
        PriceForPiece = material.Price;
        Weight = material.Weight;
        Quantity = materialCard.Quantity;
    }
}
