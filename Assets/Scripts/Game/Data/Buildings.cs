using System;
using System.Collections.Generic;
using System.Linq;
using Network;
using Network.Events;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;

namespace Game.Data {
	public static class Buildings {
		public static List<BuildedData> BuildingsData = new List<BuildedData>();

		public delegate void BuildingsUpdate();
		public delegate void BuildingsUpdateData(BuildedData data);
		public static event BuildingsUpdate OnBuildingsUpdate;
		public static event BuildingsUpdateData OnBuildingUpdate;


		public static void Init() {
			EventsController.AddBuildedDataUpdateListener(OnStageUpdate);
			EventsController.AddSetBuildingListener(OnSetBuilding);
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, Reset);

		}


		public static void Load(Action callback) {
			PacketSender.GetBuildings(delegate (WebResponse r) {
				Debug.Log(r.Text);
				BuildingsData = JsonConvert.DeserializeObject<List<BuildedData>>(r.Text).OrderBy(b => b.Round).ToList();
				callback?.Invoke();
				OnBuildingsUpdate?.Invoke();
			});
		}


		private static void OnStageUpdate(BuildedData data) {
			var b = BuildingsData.FirstOrDefault(bData => bData.Id == data.Id);
			if (b != null) b.Stage = data.Stage;
			else BuildingsData.Add(data);

			OnBuildingsUpdate?.Invoke();
			OnBuildingUpdate?.Invoke(data);
		}


		private static void OnSetBuilding(SetBuildingData data) {
			Load(null);
		}


		private static void Reset() {
			BuildingsData.Clear();
			OnBuildingsUpdate = null;
			OnBuildingUpdate = null;
		}
	}
}