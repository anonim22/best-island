using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Data {
	[CreateAssetMenu(fileName = "Disasters", menuName = "Disasters Data")]
	public class DisastersData : ScriptableObject {
		[SerializeField] private List<Disaster> Disasters = new List<Disaster>();

		private static DisastersData Data {
			get {
				if (_data != null) return _data;
				_data = Resources.Load<DisastersData>("Disasters");
				return _data;
			}
		}
		private static DisastersData _data;


		public static Disaster GetDisaster(int disasterId) {
			return Data.Disasters.FirstOrDefault(b => b.Id == disasterId);
		}
	}

	[Serializable]
	public class Disaster {
		public int    Id;
		public string Name;
		public string Description;
	}
}