using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data {
	[CreateAssetMenu(fileName = "Materials", menuName = "Materials Data")]
	public class MaterialsData : ScriptableObject {
		public static MaterialsData Data {
			get {
				if (_data != null) return _data;
				_data = Resources.Load<MaterialsData>("Materials");
				return _data;
			}
		}
		private static MaterialsData  _data;
		public         List<Material> Materials = new List<Material>();

		[Serializable]
		public class Material {
			public Sprite Icon;
			public int    Id;
			public string Name;
			public string Size;
			public float  Weight;
			public float  Price;
			public bool   OneQuantity;
		}
	}
}