using System;
using System.Collections.Generic;
using System.Linq;
using Network;
using Network.Events;
using UnityEngine;

namespace Game.Data {
	public static class AquaparkSlides {
		public delegate void               SlideInstalled();
		public static event SlideInstalled OnSlideInstalledEvent;

		private static int Slides;

		private static Dictionary<int, int> SlideToMaterialId = new Dictionary<int, int>() {
			{1, 9}, {2, 10}, {3, 11}, {4, 12}, {5, 13},
		};


		public static void Init() {
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, Reset);
		}


		public static void Load(Action callback) {
			PacketSender.GetSlides(delegate (WebResponse r) {
				Debug.Log(r.Text);
				Slides = int.Parse(r.Text);
				callback?.Invoke();
			});
		}


		public static void InstallSlide(int slideId) {
			var slide = 1 << (slideId - 1);
			Slides |= slide;
			Debug.Log($"Install {slide}");

			PacketSender.SetSlides(Slides, delegate (WebResponse r) {
				Debug.Log(r.Text);
				OnSlideInstalledEvent?.Invoke();
			}, r => Debug.LogError(r.Text));
		}


		public static bool SlideIsPlaced(int slideId) {
			var slideMask = 1 << (slideId - 1);
			return (Slides & slideMask) == slideMask;
		}


		public static bool SlideInStorage (int slideId) {
			var materialId     = SlideToMaterialId[slideId];
			var slideInStorage = Storage.Materials.Where(m => m.Id == materialId).ToList();
			return slideInStorage.Count > 0;
		}


		public static MaterialsData.Material GetSlideMaterial(int slideId) {
			var materialId = SlideToMaterialId[slideId];
			return MaterialsData.Data.Materials.FirstOrDefault(m => m.Id == materialId);
		}


		private static void Reset() {
			Slides                = 0;
			OnSlideInstalledEvent = null;
		}
	}
}