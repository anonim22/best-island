﻿using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.UI;

public class RoleLimiter : MonoBehaviour {
	[Header("Turn off buttons")] [SerializeField]
	private List<Button> ForDirector = new List<Button>();
	[SerializeField] private List<Button> ForFM                      = new List<Button>();
	[SerializeField] private List<Button> ForHR                      = new List<Button>();
	[SerializeField] private List<Button> ForBuilder                 = new List<Button>();
	[SerializeField] private Button NextRound;


	private void Start() {
		RoleChanged(Roles.ActiveRole);
		Roles.AddOnSwitchListener(RoleChanged);
		Timers.RoleTimerUpdateEvent += RoleTimeUpdate;
	}


	private void RoleTimeUpdate() {
		if (GameManager.DeviceNum == 1) {
			var director = Timers.GetRoleTime(Roles.PlayerRoles.Director);
			var hr       = Timers.GetRoleTime(Roles.PlayerRoles.HR);
			var builder  = Timers.GetRoleTime(Roles.PlayerRoles.Builder);
			if (director == 0 && hr == 0 && builder == 0) {
				TurnOffAllButtons();
				NextRound.interactable = true;
			}
			if (director <= 0) {
				NextRound.interactable = true;
			}
		}
		else {
			var fm = Timers.GetRoleTime(Roles.PlayerRoles.FinancialManager);
			if (fm == 0) {
				TurnOffAllButtons();
			}
		}
	}


	private void RoleChanged(Roles.PlayerRoles role) {
		switch (role) {
			case Roles.PlayerRoles.Director:
				Director();
				break;
			case Roles.PlayerRoles.FinancialManager:
				FinancialManager();
				break;
			case Roles.PlayerRoles.HR:
				HR();
				break;
			case Roles.PlayerRoles.Builder:
				Builder();
				break;
		}
	}


	private void Director() {
		TurnOnAllButtons();
		ForDirector.ForEach(b => b.interactable = false);
	}


	private void FinancialManager() {
		TurnOnAllButtons();
		ForFM.ForEach(b => b.interactable = false);
	}


	private void HR() {
		TurnOnAllButtons();
		ForHR.ForEach(b => b.interactable = false);
	}


	private void Builder() {
		TurnOnAllButtons();
		ForBuilder.ForEach(b => b.interactable = false);
	}


	private void TurnOnAllButtons() {
		ForDirector.ForEach(b => b.interactable = true);
		ForFM.ForEach(b => b.interactable       = true);
		ForHR.ForEach(b => b.interactable       = true);
		ForBuilder.ForEach(b => b.interactable  = true);
	}


	private void TurnOffAllButtons() {
		ForDirector.ForEach(b => b.interactable = false);
		ForFM.ForEach(b => b.interactable       = false);
		ForHR.ForEach(b => b.interactable       = false);
		ForBuilder.ForEach(b => b.interactable  = false);
	}
}