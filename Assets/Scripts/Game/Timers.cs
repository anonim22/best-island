using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Network;
using Network.Cache;
using Network.Cache.Data;
using Network.Events;
using UnityEngine;
using EventsController = Network.Events.EventsController;

namespace Game {
	public class Timers : MonoBehaviour {
		public delegate void TimeUpdateString(string time);
		public delegate void RoleTimerUpdate();

		public static event TimeUpdateString RoundTimerUpdate;
		public static event RoleTimerUpdate  RoleTimerUpdateEvent;
		public static event TimeUpdateString DirectorTimerStringUpdate,
											FMTimerStringUpdate,
											HRTimerStringUpdate,
											BuilderTimerStringUpdate;

		private static readonly Dictionary<Roles.PlayerRoles, int> RolesTime = new Dictionary<Roles.PlayerRoles, int> {
			{Roles.PlayerRoles.Director, 0},
			{Roles.PlayerRoles.FinancialManager, 0},
			{Roles.PlayerRoles.HR, 0},
			{Roles.PlayerRoles.Builder, 0},
		};
		private static int RoundTime;

		private readonly WaitForSeconds _second = new WaitForSeconds(1f);


		private void Start() {
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, ResetEvents);
			EventsController.AddUpdateRoundTimerListener(SetRoundTimer);
			Rounds.AddRoundStartListener(delegate { OnNewRound(); });

			if(!GameManager.RoundEnd) LoadTimers();
			else OnNewRound();

			StartCoroutine(Timer());
		}


		private IEnumerator Timer() {
			yield return new WaitForFixedUpdate();
			UpdateEvents(true);

			while (true) {
				yield return _second;
				if (GameManager.IsPause || !CacheManager.GameDataCache.GameIsStarted || GameManager.GameEnd) continue;

				var rTime = RolesTime[Roles.ActiveRole];
				RoundTime = RoundTime > 0 ? RoundTime - 1 : 0;

				if (rTime > 0) {
					var newRoleTime = rTime - 1;
					RolesTime[Roles.ActiveRole] = newRoleTime;
					PacketSender.UpdateTimer(Roles.ActiveRole, newRoleTime);
				}
				else if (GameManager.DeviceNum == 1) {
					var newRole =
						RolesTime.FirstOrDefault(r => r.Key != Roles.PlayerRoles.FinancialManager && r.Value > 0);
					if (!newRole.Equals(default(KeyValuePair<Roles.PlayerRoles, int>))) {
						Roles.SetRole(newRole.Key);
					}
				}
				UpdateEvents();
			}
		}


		public static void AddRoundTimerListener(TimeUpdateString callback) {
			RoundTimerUpdate += callback;
		}


		private static void LoadTimers() {
			RoundTime = 0;

			RolesTime[Roles.PlayerRoles.Director         ] = TeamDataCache.ActiveTeam.GeneralManagerTime;
			RolesTime[Roles.PlayerRoles.FinancialManager ] = TeamDataCache.ActiveTeam.FinancialManagerTime;
			RolesTime[Roles.PlayerRoles.HR               ] = TeamDataCache.ActiveTeam.HrTime;
			RolesTime[Roles.PlayerRoles.Builder          ] = TeamDataCache.ActiveTeam.BuilderTime;
		}


		public static void AddRoleListener(Roles.PlayerRoles role, TimeUpdateString callback) {
			switch (role) {
				case Roles.PlayerRoles.Director:
					DirectorTimerStringUpdate += callback;
					break;
				case Roles.PlayerRoles.FinancialManager:
					FMTimerStringUpdate += callback;
					break;
				case Roles.PlayerRoles.HR:
					HRTimerStringUpdate += callback;
					break;
				case Roles.PlayerRoles.Builder:
					BuilderTimerStringUpdate += callback;
					break;
			}
		}


		private static void ResetEvents() {
			RoundTimerUpdate     = null;
			RoleTimerUpdateEvent = null;

			DirectorTimerStringUpdate = null;
			FMTimerStringUpdate       = null;
			HRTimerStringUpdate       = null;
			BuilderTimerStringUpdate  = null;
		}


		private static void SetRoundTimer(int time) {
			RoundTime = time;
			RoundTimerUpdate?.Invoke(TimeFormat(time));
		}


		private static void UpdateEvents(bool updateAll = false) {
			RoundTimerUpdate?.Invoke(TimeFormat(RoundTime));
			RoleTimerUpdateEvent?.Invoke();

			if (updateAll) {
				DirectorTimerStringUpdate?.Invoke(GetRoleTimeString(Roles.PlayerRoles.Director));
				FMTimerStringUpdate?.Invoke(GetRoleTimeString(Roles.PlayerRoles.FinancialManager));
				HRTimerStringUpdate?.Invoke(GetRoleTimeString(Roles.PlayerRoles.HR));
				BuilderTimerStringUpdate?.Invoke(GetRoleTimeString(Roles.PlayerRoles.Builder));
			}
			else {
				switch (Roles.ActiveRole) {
					case Roles.PlayerRoles.Director:
						DirectorTimerStringUpdate?.Invoke(GetRoleTimeString(Roles.PlayerRoles.Director));
						break;
					case Roles.PlayerRoles.FinancialManager:
						FMTimerStringUpdate?.Invoke(GetRoleTimeString(Roles.PlayerRoles.FinancialManager));
						break;
					case Roles.PlayerRoles.HR:
						HRTimerStringUpdate?.Invoke(GetRoleTimeString(Roles.PlayerRoles.HR));
						break;
					case Roles.PlayerRoles.Builder:
						BuilderTimerStringUpdate?.Invoke(GetRoleTimeString(Roles.PlayerRoles.Builder));
						break;
				}
			}
		}


		private static string GetRoleTimeString(Roles.PlayerRoles role) {
			return TimeFormat(RolesTime[role]);
		}


		public static int GetRoleTime(Roles.PlayerRoles role) {
			return RolesTime[role];
		}


		private static void OnNewRound() {
			var template = CacheManager.GameDataCache.Templates.GameTemplate;

			RolesTime[Roles.PlayerRoles.Director         ] = template.GeneralManagerTime   * 60;
			RolesTime[Roles.PlayerRoles.FinancialManager ] = template.FinancialManagerTime * 60;
			RolesTime[Roles.PlayerRoles.HR               ] = template.HrTime               * 60;
			RolesTime[Roles.PlayerRoles.Builder          ] = template.BuilderTime          * 60;

			UpdateEvents(true);

			if (GameManager.DeviceNum == 1) {
				PacketSender.UpdateTimer(Roles.ActiveRole, RolesTime[Roles.PlayerRoles.Director]);
				PacketSender.UpdateTimer(Roles.ActiveRole, RolesTime[Roles.PlayerRoles.HR]);
				PacketSender.UpdateTimer(Roles.ActiveRole, RolesTime[Roles.PlayerRoles.Builder]);
				PacketSender.UpdateTimer(Roles.ActiveRole, RolesTime[Roles.PlayerRoles.FinancialManager]);
			}
		}


		private static string TimeFormat(int sec) {
			int m = sec / 60;
			int s = sec % 60;

			string minutes = ((m < 10) ? "0" : "") + m;
			string seconds = ((s < 10) ? "0" : "") + s;

			return $"{minutes}:{seconds}";
		}
	}
}