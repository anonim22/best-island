﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Data;
using Network.Cache.Data;
using Network.Events;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Game.Island {
	public class Area : MonoBehaviour, IPointerClickHandler {
		public static Area       SelectedArea;
		public static List<Area> AreaList = new List<Area>();

		public AreaInfo Info;

		[SerializeField] private MeshRenderer AreaMesh;
		[SerializeField] private BoxCollider  Collider;
		[SerializeField] private MeshCollider MeshCollider;


		private void Awake() {
			AreaList.Add(this);
		}


		private void OnMouseDown() {
			return;
			if (EventSystem.current.IsPointerOverGameObject() || Roles.ActiveRole != Roles.PlayerRoles.Director)
				return;
//			if (AreaSelection.IsOpened || ConstructionBuildings.IsOpened || ConstructionBuildingInfo.IsOpened) return;

			SelectedArea = this;
			UIManager.AreaSelection.ShowPanel();
			UIManager.AreaSelection.SetAreaInfo(Info);
		}

		public void OnPointerClick(PointerEventData eventData) {
			if (eventData.rawPointerPress == gameObject) {
				if (!GameInterface.InterfaceIsOpen) {
					SelectedArea = this;
					UIManager.AreaSelection.ShowPanel();
					UIManager.AreaSelection.SetAreaInfo(Info);
				}
			}
		}


		//For Demo
		public void Select() {
			SelectedArea = this;
			UIManager.AreaSelection.ShowPanel();
			UIManager.AreaSelection.SetAreaInfo(Info);
		}


		public void HideArea() {
			if (AreaMesh) AreaMesh.enabled         = false;
			if (Collider) Collider.enabled         = false;
			if (MeshCollider) MeshCollider.enabled = false;
		}


		public void ShowArea() {
			if (AreaMesh) AreaMesh.enabled         = true;
			if (Collider) Collider.enabled         = true;
			if (MeshCollider) MeshCollider.enabled = true;
		}


		public static void Init() {
			SceneManager.sceneLoaded += OnSceneLoaded;
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, Reset);
		}


		private static void Reset() {
			AreaList.Clear();
			SelectedArea = null;
		}


		/// <summary>
		/// Скрывает все участки
		/// </summary>
		public static void HideAllArea() {
			AreaList.ForEach(a => a.HideArea());
		}


		/// <summary>
		/// Скрывает переданные участки
		/// </summary>
		public static void HideAreas(IEnumerable<int> hide) {
			var areas = AreaList.Where(a => hide.Any(h => h == a.Info.AreaId)).ToList();
			areas.ForEach(a => a.HideArea());
		}


		public static void SetArea(int areaId) {
			SelectedArea = areaId > 0 ? SelectedArea = AreaList.FirstOrDefault(a => a.Info.AreaId == areaId) : null;
		}


		private static void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
			SceneManager.sceneLoaded -= OnSceneLoaded;
			if (scene.buildIndex != 1) return;

			print("Loading areas");
			var area = TeamDataCache.ActiveTeam.AreaId;

			if (area == 0) {
//				var hide = Buildings.BuildingsData.Select(b => b.AreaId);
//				HideAreas(hide);
				ShowAvailableArea();
			}
			else {
				SetArea(area);
				HideAllArea();
			}

			if (GameManager.DeviceNum == 1) {
				Rounds.AddRoundStartListener(delegate {
					SelectedArea = null;
					ShowAvailableArea();
				});
			}

			Roles.AddOnSwitchListener(delegate (Roles.PlayerRoles role) {
				if(SelectedArea) return;
				if (role == Roles.PlayerRoles.Director) {
					ShowAvailableArea();
				}
				else {
					HideAllArea();
				}
			});
		}


		public static void ShowAvailableArea() {
			AreaList.ForEach(a => a.ShowArea());
			var hide = Buildings.BuildingsData.Select(b => b.AreaId);
			HideAreas(hide);
		}


		[Serializable]
		public class AreaInfo {
			public int    AreaId;            // ID Сектора
			public string AreaName;          // Название зоны
			public string Description;       // Описание участка
			public float  Size;              // Размер участка
			public bool   SeaWater;          // Доступ к морской воде
			public bool   HotSpring;         // Горячие источники
			public bool   GroundwatersHight; // Высокий процент затопления грунтовыми водами

			public Transform BuildingAnchor;
		}
	}
}