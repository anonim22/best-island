using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Island {
	public class ConstructionStage : MonoBehaviour {
		[SerializeField] private List<GameObject> Blocks = new List<GameObject>();


		public void BuildBlock(GameObject block) {
			// Blocks.Remove(block);
			// if (Blocks.Count == 0) ConstructionController.StageCompleted();

			if(GetNotBuildedBlocks().Count == 0) ConstructionController.StageCompleted();
		}


		public int GetLeftMaterials() {
			return Blocks.Count == 0 ? 0 : Blocks.Sum(b => b.GetComponent<ConstructionBlock>().RequiredMaterials);
		}


		public int GetMaterialId() {
			return Blocks.Count == 0 ? 0 : Blocks.FirstOrDefault().GetComponent<ConstructionBlock>().BlockId;
		}


		public List<GameObject> GetNotBuildedBlocks() {
			return Blocks.Where(b => !b.GetComponent<ConstructionBlock>().Placed).ToList();
		}


		public int GetBuildedBlocks() {
			return Blocks.Count - GetNotBuildedBlocks().Count;
		}


		public void SetBuildedBlocks(int blocks) {
			for (int i = 0; i < blocks; i++) {
				Blocks[i].GetComponent<ConstructionBlock>().Build();
			}
		}
	}
}