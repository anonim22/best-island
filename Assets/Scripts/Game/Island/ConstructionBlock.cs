using Game.Island;
using UnityEngine;

namespace Game {
	public class ConstructionBlock : MonoBehaviour {
		public int  BlockId;
		public int  RequiredMaterials = 500;
		public bool Placed;

		[SerializeField] private ConstructionStage Stage;
		[SerializeField] private Material          DefaultMaterial;
		[SerializeField] private Material          HighlightedMaterial;
		[SerializeField] private Material          PlacedMaterial;
		[SerializeField] private Material          WrongMaterial;

		private MeshRenderer _mr;
		private Vector3        _startPos, _startScale;
		private Transform    _transform;


		private void Awake() {
			_mr        = GetComponent<MeshRenderer>();
			_transform = transform;
			var pos   = _transform.localPosition;
			var scale = _transform.localScale;

			_startPos = pos;
			_startScale = scale;

			_transform.localPosition = new Vector3(pos.x,   pos.y - _startScale.y / 2f, pos.z);
			_transform.localScale = new Vector3(scale.x, 0.01f, scale.z);
		}


		public void Build() {
			Placed = true;
			SetMaterial(MaterialType.Placed);
			Stage.BuildBlock(gameObject);

			_transform.localPosition = _startPos;
			_transform.localScale = _startScale;

			gameObject.GetComponent<Collider>().enabled = false;
		}


		public void SetMaterial(MaterialType type) {
			switch (type) {
				case MaterialType.Default:
					_mr.material = DefaultMaterial;
					break;
				case MaterialType.Highlighted:
					_mr.material = HighlightedMaterial;
					;
					break;
				case MaterialType.Placed:
					_mr.material = PlacedMaterial;
					break;
				case MaterialType.Wrong:
					_mr.material = WrongMaterial;
					break;
			}
		}


		public enum MaterialType {
			Default,
			Highlighted,
			Placed,
			Wrong
		}
	}
}