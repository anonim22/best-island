using System.Linq;
using Game.Data;
using Network;
using Network.Events;
using Network.ServerData;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Island {
	public class ConstructionController : MonoBehaviour {
		private static ConstructionController Instance;

		[SerializeField] private AudioSource ConstructionSound;
		[SerializeField] private AudioSource FinishSound;

		private int               _buildingId;
		private int               _stage = -1;
		private GameObject        _constructionModel;
		private ConstructionStage _constructionBlocks;


		private void Awake() {
			Instance = this;
			EventsController.AddSetBuildingListener(OnSetBuilding);
			// Buildings.OnBuildingUpdate += OnStageUpdate;
			EventsController.AddBuildedDataUpdateListener(OnStageUpdate);
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, Reset);
			Rounds.AddRoundEndListener(OnRoundEnd);
		}


		private void Reset() {
			_buildingId         = 0;
			_stage              = -1;
			_constructionModel  = null;
			_constructionBlocks = null;
		}


		private void ChangeStage(int stage) {
			Debug.Log($"Changing stage on {stage}");
			var building = BuildingsData.Data.GetBuilding(_buildingId);
			if (stage >= building.Stages.Length) {
				FinishConstruction();
				return;
			}

			DestroyConstruction();

			_stage = stage;
			if(!Area.SelectedArea) Debug.LogError("Area not selected");
			var anchor = Area.SelectedArea.Info.BuildingAnchor;
			_constructionModel = Instantiate(BuildingsData.Data.Stages[stage], anchor);
			_constructionBlocks = Instantiate(building.Stages[stage], anchor).GetComponent<ConstructionStage>();
		}


		private void DestroyConstruction() {
			if (_constructionModel) Destroy(_constructionModel);
			if (_constructionBlocks) Destroy(_constructionBlocks.gameObject);
		}


		private void FinishConstruction() {
			Instance.ConstructionSound.Pause();
			Instance.FinishSound.Play();

			var anchor = Area.SelectedArea.Info.BuildingAnchor;
			Instantiate(BuildingsData.Data.GetBuilding(_buildingId).Model, anchor);
			DestroyConstruction();

			_buildingId         = 0;
			_stage              = -1;
			_constructionModel  = null;
			_constructionBlocks = null;
		}


		private void OnRoundEnd(int round) {
			if (_buildingId == 0) return;

			FinishConstruction();
		}


		public static void StartConstruction(int buildingId) {
			Instance._buildingId = buildingId;
			Instance.ChangeStage(0);

			Instance.ConstructionSound.transform.position = Area.SelectedArea.Info.BuildingAnchor.position;
			Instance.ConstructionSound.Play();
		}


		public static void StageCompleted() {
			PacketSender.ChangeBuildingStage(Instance._stage + 1);
		}


		public static void SendBuildedBlocks() {
			PacketSender.SetBlocks(Instance._constructionBlocks.GetBuildedBlocks());
		}


		public static void Load() {
			SceneManager.sceneLoaded += OnSceneLoaded;
		}


		private static void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
			SceneManager.sceneLoaded -= OnSceneLoaded;
			if (scene.buildIndex != 1) return;

			foreach (var b in Buildings.BuildingsData) {
				var building = BuildingsData.Data.GetBuilding(b.BuildingId);
				if (Rounds.Round > b.Round || b.Stage >= building.Stages.Length ||
					(Rounds.Round == b.Round && GameManager.RoundEnd)) {
					var anchor = Area.AreaList.FirstOrDefault(a => a.Info.AreaId == b.AreaId)?.Info.BuildingAnchor;
					Instantiate(building.Model, anchor);
				}
				else {
					StartConstruction(b.BuildingId);
					if (b.Stage > 0) Instance.ChangeStage(b.Stage);

					if (GameManager.DeviceNum == 1) {
						if (b.BuildedBlocks > 0) {
							Instance._constructionBlocks.SetBuildedBlocks(b.BuildedBlocks);
						}
					}
				}
			}
		}


		private void OnStageUpdate(BuildedData data) {
			if (data.Stage <= _stage || _buildingId == 0) return;
			ChangeStage(data.Stage);
		}


		private void OnSetBuilding(SetBuildingData building) {
			Area.HideAllArea();
			Area.SetArea(building.AreaId);
			StartConstruction(building.BuildingId);
		}
	}
}