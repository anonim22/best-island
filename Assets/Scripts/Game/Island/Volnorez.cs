﻿using System.Collections;
using System.Linq;
using Game.Data;
using System.Collections.Generic;
using UnityEngine;

public class Volnorez : MonoBehaviour
{
    [SerializeField] private GameObject volnorez;
    private void Start()
    {
        UpdateQuantity();
        Storage.OnStorageUpdate += UpdateQuantity;
    }

    private void UpdateQuantity()
    {
        for (int i = 0; i < Storage.Materials.Count; i++) 
        {
            if (Storage.Materials[i].Id == 14 || Storage.Materials[i].Id == 15)
            {
                volnorez.SetActive(true);
                return;
            }
        }      
    }
}
