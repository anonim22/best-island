﻿using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class RoleSelection : MonoBehaviour
{
    [SerializeField] private CanvasGroup CanvasGroup;
    [SerializeField] private List<GameObject> Selections = new List<GameObject>();

    private int _selectedRole;


    private void Start() {
        var startRole = Roles.PlayerRoles.Director;
        if (GameManager.DeviceNum == 0) startRole = Roles.PlayerRoles.FinancialManager;
        Roles.SetRole(startRole);
        SelectRole((int) startRole);
    }


    public void OnCloseBtn()
    {
        var role = (Roles.PlayerRoles) _selectedRole;
        Roles.SetRole(role);
        HidePanel();
    }

    public void ShowPanel()
    {
        StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
        CanvasGroup.blocksRaycasts = true;
        GameInterface.InterfaceIsOpen = true;
    }

    public void HidePanel()
    {
        StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
        CanvasGroup.blocksRaycasts = false;
        GameInterface.InterfaceIsOpen = false;
    }


    public void SelectRole(int roleId)
    {
        _selectedRole = roleId;
        UpdateSelection(roleId - 1);
    }


    private void UpdateSelection(int selectionIndex)
    {
        Selections.ForEach(s => s.SetActive(false));
        Selections[selectionIndex].SetActive(true);
    }

    private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            cg.alpha = currentValue;

            if (percentageComplete >= 1) break;

            yield return new WaitForFixedUpdate();
        }

    }
}
