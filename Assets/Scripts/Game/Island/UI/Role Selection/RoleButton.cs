using TMPro;
using UnityEngine;

namespace Game.Island.UI.Role_Selection {
	public class RoleButton : MonoBehaviour {
		[SerializeField] private RoleSelection     RoleSelection;
		[SerializeField] private Roles.PlayerRoles Role;
		[SerializeField] private TextMeshProUGUI   Timer;


		private void Start() {
			Timers.AddRoleListener(Role, OnTimerUpdate);
		}


		public void OnSelectRole() {
			RoleSelection.SelectRole((int) Role);
		}


		private void OnTimerUpdate(string time) {
			Timer.text = time;
		}
	}
}