﻿using System.Collections;
using Game.Data;
using Network;
using Network.Cache;
using Network.ServerData;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Game.Island.UI {
	public class ConstructionBuildingInfo : MonoBehaviour {
		public static bool IsOpened => UIManager.ConstructionBuildingInfo.CanvasGroup.blocksRaycasts;

		[SerializeField] private CanvasGroup     CanvasGroup;
		[SerializeField] private Image           Icon;
		[SerializeField] private TextMeshProUGUI NameText;
		[SerializeField] private TextMeshProUGUI DescriptionText;

		private int _buildingId;


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
		}


		public void SetBuilding(int id) {
			_buildingId          = id;
		}


		#region Buttons Callback

		public void OnBackBtn() {
			HidePanel();
		}


		public void OnBuildBtn() {
			PacketSender.SetBuilding(_buildingId, Area.SelectedArea.Info.AreaId, null, OnSetBuildingError);
			HidePanel();
			UIManager.ConstructionBuildings.HidePanel();
		}

		#endregion


		private void OnSetBuildingError(WebResponse response) {
			Notification.Create($"Ошибка: {response.Text}");
			Area.SelectedArea = null;
			Area.ShowAvailableArea();
			UIManager.ConstructionBuildings.HidePanel();
		}


		private void SetIcon(Sprite img) {
			Icon.sprite = img;
		}


		public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}