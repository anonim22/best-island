﻿using System.Collections;
using Network.Cache;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Island.UI {
	public class ConstructionBuildings : MonoBehaviour {
		public static bool IsOpened => UIManager.ConstructionBuildings.CanvasGroup.blocksRaycasts;

		[SerializeField] private CanvasGroup CanvasGroup;
		[SerializeField] private TextMeshProUGUI buildingNameText;
        [SerializeField] private TextMeshProUGUI buildingDescriptionText;

        public void ShowPanel() {
			UIManager.GameInterface.SetTip("Выберите здание");
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts = false;
			GameInterface.InterfaceIsOpen = false;
		}


		#region Buttons Callback

		public void SelectBuilding(int id) {
			UIManager.ConstructionBuildingInfo.ShowPanel();
			UIManager.ConstructionBuildingInfo.SetBuilding(id);
		}
		public void SetNameBuilding(string nameTxt) {
			buildingNameText.text = nameTxt;
		}

        public void SetDescriptionBuilding(string descriptionTxt)
        {
            buildingDescriptionText.text = descriptionTxt;
        }


        public void OnBackBtn() {
			HidePanel();
            Area.SelectedArea = null;
        }

        #endregion


        private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}