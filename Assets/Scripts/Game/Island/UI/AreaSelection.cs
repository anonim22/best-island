﻿using System.Collections;
using Network;
using TMPro;
using UI;
using UnityEngine;

namespace Game.Island.UI {
	public class AreaSelection : MonoBehaviour {
		public static bool IsOpened => UIManager.AreaSelection.CanvasGroup.blocksRaycasts;

		[SerializeField] private CanvasGroup     CanvasGroup;
		[SerializeField] private TextMeshProUGUI AreaName;
		[SerializeField] private TextMeshProUGUI AreaSize;
		[SerializeField] private TextMeshProUGUI AreaSeaWater;
		[SerializeField] private TextMeshProUGUI AreaHotSprings;
		[SerializeField] private TextMeshProUGUI AreaGroundwaters;
		[SerializeField] private TextMeshProUGUI AreaDescription;


		public void ShowPanel() {
			if(GameInterface.InterfaceIsOpen) return;

			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
		}


		public void OnCloseBtn() {
			HidePanel();
			Area.SelectedArea = null;
		}


		public void OnSelectBtn() {
			HidePanel();
			UIManager.ConstructionBuildings.ShowPanel();
		}


		public void SetAreaInfo(Area.AreaInfo info) {
			AreaName.text         = $"Зона {info.AreaId}";
			AreaSize.text         = $"{info.Size} га";
			AreaSeaWater.text     = info.SeaWater ? "Да" : "Нет";
			AreaHotSprings.text   = info.HotSpring ? "Да" : "Нет";
			AreaGroundwaters.text = info.GroundwatersHight ? "Высокие" : "Низкие";
			AreaDescription.text  = info.Description;
		}


		public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}