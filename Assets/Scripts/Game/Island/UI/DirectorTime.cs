﻿using TMPro;
using UnityEngine;

namespace Game.Island.UI.Role_Selection {
	public class DirectorTime : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI Timer;


		private void Start() {
			Timers.AddRoleListener(Roles.PlayerRoles.Director,         OnTimerUpdate);
			Timers.AddRoleListener(Roles.PlayerRoles.FinancialManager, OnTimerUpdate);
			Timers.AddRoleListener(Roles.PlayerRoles.HR,               OnTimerUpdate);
			Timers.AddRoleListener(Roles.PlayerRoles.Builder,          OnTimerUpdate);
		}


		private void OnTimerUpdate(string time) {
			Timer.text = time;
		}
	}
}