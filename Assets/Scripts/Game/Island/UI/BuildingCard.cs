using System.Linq;
using Game.Data;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Island.UI {
	public class BuildingCard : MonoBehaviour {
		[SerializeField] private int             BuildingId;
		[SerializeField] private Button          SelectButton;


		private void Start() {
			Buildings.OnBuildingsUpdate += OnBuildingsUpdate;
			OnBuildingsUpdate();
		}


		private void OnBuildingsUpdate() {
			var build = CheckBuilding(BuildingId);
			if (build) {
				SelectButton.interactable = false;
				return;
			}

			// Проверка для зданий отеля со спа и без
			var secondHotel = BuildingId == 1 ? 2 : BuildingId == 2 ? 1 : 0;
			if (secondHotel == 0) return;

			build = CheckBuilding(secondHotel);
			if (build) {
				SelectButton.interactable = false;
			}
		}


		private bool CheckBuilding(int id) {
			return Buildings.BuildingsData.Any(b => b.BuildingId == id);
		}
	}
}