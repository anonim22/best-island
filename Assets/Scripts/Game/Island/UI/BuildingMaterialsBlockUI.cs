using System.Linq;
using Game.Data;
using Network;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.Island.UI {
	public class BuildingMaterialsBlockUI : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {
		[SerializeField] private int        BlockId;
		[SerializeField] private Text       QuantityText;
		[SerializeField] private Sprite     BlockSprite;
		[SerializeField] private Vector2    InstanceSize;
		[SerializeField] private Color      InstanceColor;
		[SerializeField] private Canvas     Canvas;
		[SerializeField] private GameObject Block;
		[SerializeField] private string[] txtNotification;

		private RectTransform     _movingBlock;
		private ConstructionBlock _lastBlock;
		private int               _quantity;


		private void Start() {
			UpdateQuantity();
			Storage.OnStorageUpdate += UpdateQuantity;
		}


		public void OnDrag(PointerEventData eventData) {
			UpdateBlockSpritePosition();
			var block = Raycast();

			if (_lastBlock && _lastBlock != block) _lastBlock.SetMaterial(ConstructionBlock.MaterialType.Default);
			if (!block || block.Placed) return;
			if (block.BlockId != BlockId) {
				_lastBlock = block;
				block.SetMaterial(ConstructionBlock.MaterialType.Wrong);
				return;
			}

			_lastBlock = block;
			block.SetMaterial(ConstructionBlock.MaterialType.Highlighted);
		}


		public void OnBeginDrag(PointerEventData eventData) {
			CreateBlockSprite();
		}


		public void OnEndDrag(PointerEventData eventData) {
			DestroyBlockSprite();

			// ��������� �����
			var block = Raycast();

			// ���� ��������� ��� ������� �� ������ ����, �� ���������� ����������� ����� ����������� ���������
			if (_lastBlock && _lastBlock != block) _lastBlock.SetMaterial(ConstructionBlock.MaterialType.Default);
			_lastBlock = null;

			// ���� ����� ��� ��� �� ��������, �� �����
			if (!block || block.Placed) return;

			// ���� �� �������� ��������, �� ������� � ��������� ������� ���������
			if (block.BlockId != BlockId) {
				//Notification.Create("������������ ����");
				Notification.Create(txtNotification[0]);
				block.SetMaterial(ConstructionBlock.MaterialType.Default);
				return;
			}

			// �������� ���-��
			if (_quantity < block.RequiredMaterials) {
				//Notification.Create("������������ ������ ��� �������������");
				Notification.Create(txtNotification[1]);
				block.SetMaterial(ConstructionBlock.MaterialType.Default);
				return;
			}

			block.Build();
			ConstructionController.SendBuildedBlocks();
			Storage.UseMaterial(BlockId, block.RequiredMaterials, true);
		}


		private void UpdateBlockSpritePosition () {
			RectTransformUtility.ScreenPointToLocalPointInRectangle(_movingBlock.transform as RectTransform,
																	Input.mousePosition, Canvas.worldCamera,
																	out var pos);
			_movingBlock.position = _movingBlock.transform.TransformPoint(pos);
		}


		private ConstructionBlock Raycast() {
			if (Camera.main != null) {
				Ray       ray  = Camera.main.ScreenPointToRay(Input.mousePosition);
				LayerMask mask = LayerMask.GetMask("Building Block");

				if (Physics.Raycast(ray, out var hit, Mathf.Infinity, mask)) {
					if (hit.collider.GetComponent<ConstructionBlock>())
						return hit.collider.GetComponent<ConstructionBlock>();
				}
			}

			return null;
		}


		private void CreateBlockSprite() {
			DestroyBlockSprite();

			var go = new GameObject($"Block {BlockId}");
			go.transform.parent    = Canvas.transform;
			_movingBlock           = go.AddComponent<RectTransform>();
			_movingBlock.sizeDelta = InstanceSize;
			var img = go.AddComponent<Image>();
			img.sprite = BlockSprite;
			img.color  = InstanceColor;
		}


		private void DestroyBlockSprite() {
			if (!_movingBlock) return;
			Destroy(_movingBlock.gameObject);
			_movingBlock = null;
		}


		private void UpdateQuantity() {
			var material = Storage.Materials.FirstOrDefault(m => m.Id == BlockId);

			_quantity = material?.Quantity ?? 0;
			Block.SetActive(_quantity == 0);
			QuantityText.text = $"{_quantity} "+txtNotification[2];
		}
	}
}