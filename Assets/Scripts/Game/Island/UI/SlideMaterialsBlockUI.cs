using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.Island.UI {
	public class SlideMaterialsBlockUI : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {
		[SerializeField] private int        BlockId;
		[SerializeField] private Text       QuantityText;
		[SerializeField] private Sprite     BlockSprite;
		[SerializeField] private Vector2    InstanceSize;
		[SerializeField] private Color      InstanceColor;
		[SerializeField] private Canvas     Canvas;
		[SerializeField] private GameObject Block;

		private RectTransform     _movingBlock;
		private ConstructionSlide _lastBlock;
		private int               _quantity;


		public void OnDrag(PointerEventData eventData) {
			UpdateBlockSpritePosition();
			var block = Raycast();

			if (_lastBlock && _lastBlock != block) _lastBlock.SetMaterial(ConstructionSlide.MaterialType.Default);
			if (!block || block.Placed) return;
			if (block.BlockId != BlockId) {
				_lastBlock = block;
				block.SetMaterial(ConstructionSlide.MaterialType.Wrong);
				return;
			}

			_lastBlock = block;
			block.SetMaterial(ConstructionSlide.MaterialType.Highlighted);
		}


		public void OnBeginDrag(PointerEventData eventData) {
			CreateBlockSprite();
		}


		public void OnEndDrag(PointerEventData eventData) {
			DestroyBlockSprite();

			// Получение горки
			var block = Raycast();

			// Если указатель был наведен на другой блок, то возвращаем предыдущему блоку стандартную подсветку
			if (_lastBlock && _lastBlock != block) _lastBlock.SetMaterial(ConstructionSlide.MaterialType.Default);
			_lastBlock = null;

			// Если блока нет или он построен, то выход
			if (!block || block.Placed) return;

			// Если не подходит материал, то выходим и оставляем обычную подсветку
			if (block.BlockId != BlockId) {
				block.SetMaterial(ConstructionSlide.MaterialType.Default);
				return;
			}
			block.Build();
		}


		private void UpdateBlockSpritePosition () {
			RectTransformUtility.ScreenPointToLocalPointInRectangle(_movingBlock.transform as RectTransform,
																	Input.mousePosition, Canvas.worldCamera,
																	out var pos);
			_movingBlock.position = _movingBlock.transform.TransformPoint(pos);
		}


		private ConstructionSlide Raycast() {
			if (Camera.main != null) {
				Ray       ray  = Camera.main.ScreenPointToRay(Input.mousePosition);
				LayerMask mask = LayerMask.GetMask("Building Block");

				if (Physics.Raycast(ray, out var hit, Mathf.Infinity, mask)) {
					if (hit.collider.GetComponent<ConstructionSlide>())
						return hit.collider.GetComponent<ConstructionSlide>();
				}
			}

			return null;
		}


		private void CreateBlockSprite() {
			DestroyBlockSprite();

			var go = new GameObject($"Block {BlockId}");
			go.transform.parent    = Canvas.transform;
			_movingBlock           = go.AddComponent<RectTransform>();
			_movingBlock.sizeDelta = InstanceSize;
			var img = go.AddComponent<Image>();
			img.sprite = BlockSprite;
			img.color  = InstanceColor;
		}


		private void DestroyBlockSprite() {
			if (!_movingBlock) return;
			Destroy(_movingBlock.gameObject);
			_movingBlock = null;
		}
	}
}