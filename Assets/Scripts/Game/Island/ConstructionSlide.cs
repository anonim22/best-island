using Game.Island;
using UnityEngine;

namespace Game {
	public class ConstructionSlide : MonoBehaviour {
		public int BlockId;
		public bool Placed;
		public GameObject buildSlide;

		[SerializeField] private ConstructionStage Stage;
		[SerializeField] private UnityEngine.Material DefaultMaterial;
		[SerializeField] private UnityEngine.Material HighlightedMaterial;
		[SerializeField] private UnityEngine.Material PlacedMaterial;
		[SerializeField] private UnityEngine.Material WrongMaterial;

		private MeshRenderer _mr;
		private float _startYPos, _startYScale;
		private Transform _transform;


		private void Start() {
			_mr = GetComponent<MeshRenderer>();
			_transform = transform;
			var pos = _transform.position;
			var scale = _transform.localScale;

			_startYPos = pos.y;

			_startYScale = scale.y;
			//_transform.localScale = new Vector3(scale.x, 0.01f, scale.z);

			_transform.position = new Vector3(pos.x, pos.y - _startYScale / 2f, pos.z);
		}


		public void Build() {
			/* Placed = true;
			SetMaterial(MaterialType.Placed);
			Stage.BuildBlock(gameObject);

			var scale = _transform.localScale;
			var pos = _transform.position;

			_transform.position = new Vector3(pos.x, _startYPos, pos.z);
			_transform.localScale = new Vector3(scale.x, _startYScale, scale.z); */
			buildSlide.SetActive(true);
			gameObject.SetActive(false);
		}


		public void SetMaterial(MaterialType type) {
			switch (type) {
				case MaterialType.Default: _mr.material = DefaultMaterial; break;
				case MaterialType.Highlighted: _mr.material = HighlightedMaterial; ;break;
				case MaterialType.Placed: _mr.material = PlacedMaterial; break;
				case MaterialType.Wrong: _mr.material = WrongMaterial; break;
			}
		}

		public enum MaterialType {
			Default,
			Highlighted,
			Placed,
            Wrong
		}
	}
}