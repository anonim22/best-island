using Game.Data;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Island {
	public class Aquapark : MonoBehaviour, IPointerClickHandler {
		[SerializeField] private GameObject Slide1, Slide2, Slide3, Slide4, Slide5;


		private void Start() {
			UpdateSlides();
			AquaparkSlides.OnSlideInstalledEvent += UpdateSlides;
		}


		public void OnMouseDown() {
			return;
			if (!GameInterface.InterfaceIsOpen &&
				GameManager.DeviceNum == 1     &&
				Roles.ActiveRole      == Roles.PlayerRoles.Director ||
				Roles.ActiveRole == Roles.PlayerRoles.Builder) {
				UIManager.AquaparkSlidesPanel.ShowPanel();
			}
		}


		private void UpdateSlides() {
			Slide1.SetActive(AquaparkSlides.SlideIsPlaced(1));
			Slide2.SetActive(AquaparkSlides.SlideIsPlaced(2));
			Slide3.SetActive(AquaparkSlides.SlideIsPlaced(3));
			Slide4.SetActive(AquaparkSlides.SlideIsPlaced(4));
			Slide5.SetActive(AquaparkSlides.SlideIsPlaced(5));
		}


		public void OnPointerClick(PointerEventData eventData) {
			if (!GameInterface.InterfaceIsOpen &&
				GameManager.DeviceNum == 1     &&
				Roles.ActiveRole      == Roles.PlayerRoles.Director ||
				Roles.ActiveRole == Roles.PlayerRoles.Builder) {
				UIManager.AquaparkSlidesPanel.ShowPanel();
			}
		}
	}
}