using System;
using System.Collections.Generic;
using Network;
using Network.Cache;
using Network.Cache.Data;
using Network.Events;
using Network.ServerData;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public static class GameManager
    {
        public delegate void Pause();
        public delegate void GamePaused(bool pause);
        public static event Pause OnRoundEnd, OnRoundStart;
        public static event Pause OnPlayerLeft, OnPlayerJoined;
        public static event Pause OnAdminPause, OnAdminResume;
        public static event Pause OnAuctionStart, OnAuctionEnd;
        public static event Pause OnGameEnd;

        public static bool IsPause => RoundEnd || PlayerLeft || AdminPause || Auction || WaitingRoundResult;

        public static bool RoundEnd;
        public static bool PlayerLeft;
        public static bool AdminPause;
        public static bool Auction;
        public static bool GameEnd {
            get => CacheManager.GameDataCache.GameIsEnded;
            set => CacheManager.GameDataCache.GameIsEnded = value;
        }

        private static bool _waitingRoundResult;
        private static bool _waitingGameResult;

        public static bool WaitingRoundResult
        {
            get => _waitingRoundResult;
            set
            {
                _waitingRoundResult = value;
                CheckPause();
            }
        }

        public static bool WaitingGameResult
        {
            get => _waitingGameResult;
            set
            {
                _waitingGameResult = value;
                CheckPause();
            }
        }

        public static int DeviceNum;


        public static void Init()
        {
            EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, Reset);

            EventsController.AddGamePausedListener(OnPause);
            EventsController.AddGameResumedListener(OnGameResume);
            EventsController.AddUpdateRoundTimerListener(delegate
            {
                RoundEnd = false;
                PlayerLeft = false;
                AdminPause = false;

                CacheManager.GameDataCache.GameIsStarted = true;
                CacheManager.GameDataCache.RoundIsStarted = true;

                CheckPause();
            });

            RoundEnd = Convert.ToBoolean(Convert.ToInt16(TeamDataCache.ActiveTeam.RoundEnd)) ||
                        (!CacheManager.GameDataCache.RoundIsStarted);
            PlayerLeft = CacheManager.GameDataCache.GameIsPaused;
            AdminPause = CacheManager.GameDataCache.AdminPause;
            Auction = CacheManager.GameDataCache.Data.PausedByAuction;

            Debug.Log($"-----Game state-----{Environment.NewLine}" +
                    $"RoundEnd: {RoundEnd}{Environment.NewLine}" +
                    $"PlayerLeft: {PlayerLeft}{Environment.NewLine}" +
                    $"AdminPause: {AdminPause}{Environment.NewLine}" +
                    $"Auction: {Auction}{Environment.NewLine}" +
                    "--------------------");

            SceneManager.sceneLoaded += OnSceneLoaded;
        }


        private static void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            CheckPause();
            PacketSender.GameLoaded();
        }


        private static void OnPause(List<string> reasons)
        {
            foreach (var reason in reasons)
            {
                switch (reason)
                {
                    case "player_left":
                        OnPlayerLeft?.Invoke();
                        PlayerLeft = true;
                        break;
                    case "admin_pause":
                        OnAdminPause?.Invoke();
                        AdminPause = true;
                        CacheManager.GameDataCache.RoundIsStarted = false;
                        break;
                    case "round_end":
                        OnRoundEnd?.Invoke();
                        RoundEnd = true;
                        break;
                    case "auction":
                        OnAuctionStart?.Invoke();
                        Auction = true;
                        break;
                    case "game_end":
                        if (!RoundEnd) {
                            OnRoundEnd?.Invoke();
                            RoundEnd = true;
                        }

                        GameEnd = true;
                        OnGameEnd?.Invoke();
                        break;
                    default:
                        Debug.LogError($"Incorrect pause: {reason}");
                        break;
                }
            }

            CheckPause();
        }


        private static void OnGameResume(List<string> reasons)
        {
            foreach (var reason in reasons)
            {
                Debug.Log($"game resumed: {reason}");
                switch (reason)
                {
                    case "player_joined":
                        OnPlayerJoined?.Invoke();
                        PlayerLeft = false;
                        break;
                    case "admin_resume":
                        OnAdminResume?.Invoke();
                        AdminPause = false;
                        break;
                    case "new_round":
                        OnRoundStart?.Invoke();
                        RoundEnd = false;
                        break;
                    case "auction":
                        OnAuctionEnd?.Invoke();
                        Auction = false;
                        break;
                    default:
                        Debug.LogError($"Incorrect game resume reason: {reason}");
                        break;
                }
            }

            CheckPause();
        }


        private static void CheckPause()
        {
            if(SceneManager.GetActiveScene().buildIndex != 1) return;

            if (GameEnd) {
                UIManager.PauseScreen.SetText(2);
                UIManager.PauseScreen.ShowPanel();

                if (WaitingGameResult) {
                    UIManager.PauseScreen.SetText(6);
                }
                return;
            }

            if (IsPause) UIManager.PauseScreen.ShowPanel();
            else
            {
                UIManager.PauseScreen.HidePanel();
                return;
            }

            if (AdminPause) UIManager.PauseScreen.SetText(0);
            else if (WaitingRoundResult) UIManager.PauseScreen.SetText(4);
            else if (RoundEnd) UIManager.PauseScreen.SetText(1);
            else if (PlayerLeft) UIManager.PauseScreen.SetText(3);
            else if (Auction) UIManager.PauseScreen.SetText(5);
        }


        private static void Reset()
        {
            AdminPause = false;
            RoundEnd = false;
            PlayerLeft = false;
            Auction = false;
            DeviceNum = 0;

            OnRoundEnd = null;
            OnRoundStart = null;
            OnPlayerLeft = null;
            OnPlayerJoined = null;
            OnAdminPause = null;
            OnAdminResume = null;
            OnAuctionEnd = null;
            OnAuctionStart = null;
        }
    }
}