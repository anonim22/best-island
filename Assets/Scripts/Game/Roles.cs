﻿using UI;

namespace Game {
	public static class Roles {
		public static   PlayerRoles      ActiveRole;
		public delegate void             OnRoleSwitch(PlayerRoles role);
		public static event OnRoleSwitch OnRoleSwitchEvent;


		public static void Init() {
			ResetOnSwitchListener();
		}


		private static void ActivateRole(PlayerRoles role) {
			switch (role) {
				case PlayerRoles.Director:
					break;
				case PlayerRoles.FinancialManager:
					break;
				case PlayerRoles.HR:
					break;
				case PlayerRoles.Builder:
					ActivateBuilder();
					break;
			}
		}


		private static void DeactivateRole(PlayerRoles role) {
			switch (role) {
				case PlayerRoles.Director:         break;
				case PlayerRoles.FinancialManager: break;
				case PlayerRoles.HR:               break;
				case PlayerRoles.Builder:
					DeactivateBuilder();
					break;
			}
		}


		private static void ActivateBuilder() {
			UI.UIManager.MaterialsBlockPanel.ShowPanel();
		}


		private static void DeactivateBuilder() {
			UI.UIManager.MaterialsBlockPanel.HidePanel();
		}


		public static void SetRole(PlayerRoles role) {
			DeactivateRole(ActiveRole);
			ActiveRole = role;
			ActivateRole(role);
			Notification.Create($"Выбрана роль \"{GetRoleName(role)}\"");
			OnRoleSwitchEvent?.Invoke(role);
		}


		public static void AddOnSwitchListener(OnRoleSwitch callback) {
			OnRoleSwitchEvent += callback;
		}


		public static void ResetOnSwitchListener() {
			OnRoleSwitchEvent = null;
		}


		public static string GetRoleName(PlayerRoles role) {
			switch (role) {
				case PlayerRoles.Director: return "Генеральный директор";
				case PlayerRoles.FinancialManager: return "Финансовый директор";
				case PlayerRoles.HR: return "HR";
				case PlayerRoles.Builder: return "Исполнитель";
			}

			return "";
		}


		public enum PlayerRoles {
			Director         = 1,
			FinancialManager = 2,
			HR               = 3,
			Builder          = 4
		}
	}
}