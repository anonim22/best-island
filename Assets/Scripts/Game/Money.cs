using System.Collections.Generic;
using System.Linq;
using Network.Cache.Data;
using Network.Events;
using Network.ServerData;
using UI;

namespace Game {
	public static class Money {
		public delegate void              BalanceUpdate(int balance);
		public static event BalanceUpdate OnBalanceUpdateEvent;

		public static int Balance = 0;


		public static void Init() {
			Balance = TeamDataCache.ActiveTeam.Money;

			EventsController.AddMoneyChangedListener(OnMoneyChanged);
			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, OnConnectionLost);
		}


		public static void AddBalanceUpdateListener(BalanceUpdate callback) {
			OnBalanceUpdateEvent += callback;
		}


		public static string Format(int price) {
			return $"{price:n0}".Replace(' ', '.');
		}


		public static string Format(float price) {
			return $"{price:n0}".Replace(' ', '.');
		}


		public static string ToMoneyFormat(this float price) {
			return $"{price:n0}".Replace(' ', '.');
		}


		public static string ToMoneyFormat(this int price) {
			return $"{price:n0}".Replace(' ', '.');
		}


		public static string ToMoneyFormat(this string price) {
			return $"{price:n0}".Replace(' ', '.');
		}


		private static void OnConnectionLost() {
			Balance = 0;
			ResetEvent();
		}


		private static void OnMoneyChanged(List<MoneyChanged> money) {
			money = money.Where(m => m.Balance != Balance).ToList();
			if(money.Count == 0) return;

			var changed = money.GroupBy(m => m.Balance).Select(m => m.First()).ToList();
			changed.ForEach(delegate (MoneyChanged m) {
				var text = (m.Changed < 0 ? "Расходы: " : "Пополнение: ") + Money.Format(m.Changed);
				Notification.Create(text);
			});

			Balance = money.Last().Balance;
			OnBalanceUpdate(Balance);
		}


		private static void OnBalanceUpdate(int balance) {
			OnBalanceUpdateEvent?.Invoke(balance);
		}


		private static void ResetEvent() {
			OnBalanceUpdateEvent = null;
		}
	}
}