﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;

[RequireComponent(typeof(CanvasGroup))]
public class GameSettings : MonoBehaviour {
	CanvasGroup       uiElement;
	public Slider     effectsSound;
	public Slider     bgSound;
	public GameObject arSessionSize;
	public Toggle     ConsoleToggle;
	public Canvas     ConsoleCanvas;
	public AudioMixer AudioMixer;


	void Start() {
		uiElement             = GetComponent<CanvasGroup>();
		ConsoleCanvas.enabled = ConsoleToggle.isOn;
	}


	public void ShowPanel() {
		StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 1, .5f));
		uiElement.blocksRaycasts      = true;
		GameInterface.InterfaceIsOpen = true;
	}


	public void HidePanel() {
		StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 0, .5f));
		uiElement.blocksRaycasts      = false;
		GameInterface.InterfaceIsOpen = false;
	}


	public void OnValueChangedBG(float value) {
		AudioMixer.SetFloat("BackgroundVolume", value);
	}


	public void OnValueChangedEffect(float value) {
		AudioMixer.SetFloat("EffectsVolume", value);
	}


	public void ChangeSize(int size) {
		arSessionSize.transform.localScale = new Vector3(size, size, size);
	}


	public void OnConsoleValueChanged(bool value) {
		ConsoleCanvas.enabled = value;
	}


	public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
		float _timeStartedLerping = Time.time;
		float timeSinceStarted    = Time.time - _timeStartedLerping;
		float percentageComplete  = timeSinceStarted / lerpTime;

		while (true) {
			timeSinceStarted   = Time.time - _timeStartedLerping;
			percentageComplete = timeSinceStarted / lerpTime;

			float currentValue = Mathf.Lerp(start, end, percentageComplete);

			cg.alpha = currentValue;

			if (percentageComplete >= 1) break;

			yield return new WaitForFixedUpdate();
		}
	}
}