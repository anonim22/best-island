using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Data;
using Network.Events;
using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI {
	public class TopPanel : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI MoneyText;
		[SerializeField] private TextMeshProUGUI LoansText;
		[SerializeField] private TextMeshProUGUI RoundTimer;


		private void Start() {
			Timers.AddRoundTimerListener(OnRoundTimerUpdate);
			Money.AddBalanceUpdateListener(OnBalanceUpdate);
			EventsController.AddLoansUpdateListener(OnLoanUpdate);

			OnBalanceUpdate(Money.Balance);
			OnLoanUpdate(Loan.Loans);
		}


		private void OnRoundTimerUpdate(string time) {
			RoundTimer.text = time;
		}


		private void OnBalanceUpdate(int balance) {
			MoneyText.text = balance.ToString("n0");
		}


		private void OnLoanUpdate(List<LoanData> loans) {
			LoansText.text = loans.Where(l => l.Status == "approved").Sum(l => l.Balance).ToString("n0");
		}
	}
}