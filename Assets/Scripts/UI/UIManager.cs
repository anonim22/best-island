using System;
using Game.Island.UI;
using UI.Chartering;
using UI.Chartering.Invitations;
using UI.Chartering.Ships;
using UI.LK;
using UI.RoundResult;
using UnityEngine;

namespace UI {
	public class UIManager : MonoBehaviour {
		private static          UIManager Instance;
		[SerializeField] public UIPanel   UI;

		public static AreaSelection            AreaSelection            => Instance.UI.AreaSelection;
		public static ConstructionBuildings    ConstructionBuildings    => Instance.UI.ConstructionBuildings;
		public static ConstructionBuildingInfo ConstructionBuildingInfo => Instance.UI.ConstructionBuildingInfo;
		public static RoleSelection            RoleSelection            => Instance.UI.RoleSelection;
		public static LKBuildings              LKBuildings              => Instance.UI.LKBuildings;
		public static LoanPanel                LoanPanel                => Instance.UI.LoanPanel;
		public static MaterialsBlockPanel      MaterialsBlockPanel      => Instance.UI.MaterialsBlockPanel;
		public static GameInterface            GameInterface            => Instance.UI.GameInterface;
		public static MaterialShopPanel        MaterialShopPanel        => Instance.UI.MaterialShopPanel;
		public static MaterialShopOrders       MaterialShopOrders       => Instance.UI.MaterialShopOrders;
		public static ShipsPanel               ShipsPanel               => Instance.UI.ShipsPanel;
		public static CargoPanel               CargoPanel               => Instance.UI.CargoPanel;
		public static SendInvitePanel          SendInvitePanel          => Instance.UI.SendInvitePanel;
		public static InvitiationsPanel        InvitiationsPanel        => Instance.UI.InvitiationsPanel;
		public static PauseScreen              PauseScreen              => Instance.UI.PauseScreen;
		public static RolePick                 RolePick                 => Instance.UI.RolePick;
		public static NextRound                NextRound                => Instance.UI.NextRound;
		public static GameRoundResults         GameRoundResults         => Instance.UI.GameRoundResults;
		public static Insurance                Insurance                => Instance.UI.Insurance;
		public static HR_Panel                 HR_Panel                 => Instance.UI.HR_Panel;
		public static AquaparkSlidesPanel      AquaparkSlidesPanel      => Instance.UI.AquaparkSlidesPanel;
		public static EventPanel               EventPanel               => Instance.UI.EventPanel;


		private void Awake() {
			Instance = this;
		}


		[Serializable]
		public class UIPanel {
			public AreaSelection            AreaSelection;
			public ConstructionBuildings    ConstructionBuildings;
			public ConstructionBuildingInfo ConstructionBuildingInfo;
			public LKBuildings              LKBuildings;
			public RoleSelection            RoleSelection;
			public MaterialsBlockPanel      MaterialsBlockPanel;
			public GameInterface            GameInterface;
			public MaterialShopPanel        MaterialShopPanel;
			public MaterialShopOrders       MaterialShopOrders;
			public ShipsPanel               ShipsPanel;
			public CargoPanel               CargoPanel;
			public SendInvitePanel          SendInvitePanel;
			public InvitiationsPanel        InvitiationsPanel;
			public LoanPanel                LoanPanel;
			public PauseScreen              PauseScreen;
			public RolePick                 RolePick;
			public NextRound                NextRound;
			public GameRoundResults         GameRoundResults;
			public Insurance                Insurance;
			public HR_Panel                 HR_Panel;
			public AquaparkSlidesPanel      AquaparkSlidesPanel;
			public EventPanel               EventPanel;
		}
	}
}