﻿using System.Collections;
using Game;
using Network;
using Network.Events;
using UnityEngine;
using TMPro;
using UI;

[RequireComponent(typeof(CanvasGroup))]
public class Auction : MonoBehaviour {
	[SerializeField] private CanvasGroup     CanvasGroup;
	[SerializeField] private AudioSource     Sound1, Sound2;
	[SerializeField] private TextMeshProUGUI Lot;
	[SerializeField] private TMP_InputField  BetField;


	private void Start() {
		EventsController.AddAuctionListener(OnNextLot);
		EventsController.AddAuctionConfirmListener(delegate {
			Notification.Create("Лот достается Вам!");
		});
		EventsController.AddAuctionRejectListener(delegate {
			Notification.Create("Вашу ставку отклонили");
		});
		GameManager.OnAuctionEnd += HidePanel;

		if (GameManager.Auction) {
			PacketSender.GetAuctionLot(delegate (WebResponse response) {
				print(response.Text);
				OnNextLot(response.Text);
			});
		}
	}


	public void ShowPanel() {
		StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
		CanvasGroup.blocksRaycasts    = true;
		GameInterface.InterfaceIsOpen = true;
	}


	public void HidePanel() {
		StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
		CanvasGroup.blocksRaycasts    = false;
		GameInterface.InterfaceIsOpen = false;

		Sound1.Pause();
		Sound2.Pause();
	}


	public void OnBetBtn() {
		print(BetField.text);
		var bet = int.Parse(BetField.text);
		if (bet < 0) {
			Notification.Create("Введите положительное значение");
			return;
		}

		if (bet == 0) {
			Notification.Create("Введите значение");
			return;
		}

		PacketSender.AuctionBet(bet, delegate { Notification.Create("Ставка сделана"); });
	}


	private void OnNextLot(string lot) {
		Lot.text = lot;
		ShowPanel();
		Sound1.Play();
		Sound2.Play();
	}


	private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
		float _timeStartedLerping = Time.time;
		float timeSinceStarted    = Time.time - _timeStartedLerping;
		float percentageComplete  = timeSinceStarted / lerpTime;

		while (true) {
			timeSinceStarted   = Time.time - _timeStartedLerping;
			percentageComplete = timeSinceStarted / lerpTime;

			float currentValue = Mathf.Lerp(start, end, percentageComplete);

			cg.alpha = currentValue;

			if (percentageComplete >= 1) break;

			yield return new WaitForFixedUpdate();
		}
	}
}