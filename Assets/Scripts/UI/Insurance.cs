﻿using System.Collections;
using System.Linq;
using Game;
using Network;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
	[RequireComponent(typeof(CanvasGroup))]
	public class Insurance : MonoBehaviour {
		[SerializeField] private CanvasGroup CanvasGroup;
		[SerializeField] private Toggle[] Toggles;

		private int[] _prices = {
			500000,
			500000,
			500000,
			1300000,
		};


		private void Start() {
			if(GameManager.DeviceNum != 1) return;
			Rounds.AddRoundStartListener(OnStartRound);

			if(Rounds.Round < 2 || GameManager.RoundEnd) return;
			PacketSender.GetInsurance(delegate (WebResponse r) {
				print($"Insurance: {r.Text}");
				if (r.Text == "not set") {
					ShowPanel();
				}
			});
		}


		private void OnStartRound(int round) {
			if(round < 2) return;

			Toggles.ToList().ForEach(t => t.isOn = false);
			ShowPanel();
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
		}


		public void Buy() {
			var tIndex = Toggles.ToList().FindIndex(t => t.isOn);
			if (tIndex == -1) {
				Notification.Create("Выберите вид страхования");
				return;
			}
			if (Money.Balance < _prices[tIndex]) {
				Notification.Create("Недостаточно средств");
				return;
			}

			PacketSender.BuyInsurance(tIndex + 1, delegate {
				Notification.Create("Страховка приобретена");
				HidePanel();
			},delegate (WebResponse r) {
				Notification.Create($"Ошибка: {r.Text}");
			});
		}


		public void Cancel() {
			PacketSender.BuyInsurance(0);
			HidePanel();
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}