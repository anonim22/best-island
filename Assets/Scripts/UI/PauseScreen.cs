using System.Collections;
using TMPro;
using UnityEngine;
using System.Text;

namespace UI
{
    public class PauseScreen : MonoBehaviour
    {
        [SerializeField] private CanvasGroup CanvasGroup;
        [SerializeField] private TextMeshProUGUI ReasonText;
        [SerializeField] private string[] txt;


        public void ShowPanel()
        {
            StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
            CanvasGroup.blocksRaycasts = true;
            GameInterface.InterfaceIsOpen = true;
        }


        public void HidePanel()
        {
            StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
            CanvasGroup.blocksRaycasts = false;
            GameInterface.InterfaceIsOpen = false;

            SetText(5);
        }


        public void SetText(int num)
        {
            ReasonText.text = txt[num];
        }


        private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1)
        {
            float _timeStartedLerping = Time.time;
            float timeSinceStarted = Time.time - _timeStartedLerping;
            float percentageComplete = timeSinceStarted / lerpTime;

            while (true)
            {
                timeSinceStarted = Time.time - _timeStartedLerping;
                percentageComplete = timeSinceStarted / lerpTime;

                float currentValue = Mathf.Lerp(start, end, percentageComplete);

                cg.alpha = currentValue;

                if (percentageComplete >= 1) break;

                yield return new WaitForFixedUpdate();
            }
        }
    }
}