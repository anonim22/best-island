using System;
using System.Collections.Generic;
using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.GameResult {
	public class GameBlock3 : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI Round1;
		[SerializeField] private TextMeshProUGUI Round2;
		[SerializeField] private TextMeshProUGUI Round3;
		[SerializeField] private TextMeshProUGUI Round4;

		private LayoutElement Label1, Label2, Label3, Label4;
		private bool _heightUpdated;


		private void LateUpdate() {
			// Обновление размеров 1 раз. Если вызывать в SetData, то к тому
			// моменту размер текста еще не будет рассчитан компонентом
			// VerticalLayoutGroup и будет записано неверное значение
			if (!_heightUpdated) {
				UpdateHeight();
			}
		}


		public void SetData(GameResultData.TeamData data,   LayoutElement label1, LayoutElement label2,
							LayoutElement           label3, LayoutElement label4) {
			Label1 = label1;
			Label2 = label2;
			Label3 = label3;
			Label4 = label4;

			var gameEvents = data.Events;
			var disasters  = data.Disasters;

			var eventsList = new List<string>();

			for (int i = 0; i < gameEvents.Count; i++) {
				var events = "";
				if (gameEvents[i] != "0") {
					events += gameEvents[i];
					if (disasters[i] != "0") events += ", ";
				}

				if (disasters[i] != "0") events += disasters[i];
				events = events.Replace(",", Environment.NewLine);
				eventsList.Add(events);
			}

			Round1.text = eventsList.Count > 0 ? eventsList[0] : "";
			Round2.text = eventsList.Count > 1 ? eventsList[1] : "";
			Round3.text = eventsList.Count > 2 ? eventsList[2] : "";
			Round4.text = eventsList.Count > 3 ? eventsList[3] : "";
		}


		private void UpdateHeight() {
			var height1 = Round1.gameObject.GetComponent<RectTransform>().sizeDelta.y;
			var height2 = Round2.gameObject.GetComponent<RectTransform>().sizeDelta.y;
			var height3 = Round3.gameObject.GetComponent<RectTransform>().sizeDelta.y;
			var height4 = Round4.gameObject.GetComponent<RectTransform>().sizeDelta.y;

			if (Label1.preferredHeight < height1) Label1.preferredHeight = height1;
			if (Label2.preferredHeight < height2) Label2.preferredHeight = height2;
			if (Label3.preferredHeight < height3) Label3.preferredHeight = height3;
			if (Label4.preferredHeight < height4) Label4.preferredHeight = height4;

			_heightUpdated = true;
		}
	}
}