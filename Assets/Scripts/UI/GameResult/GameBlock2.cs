using Game.Data;
using Network.ServerData;
using TMPro;
using UnityEngine;

namespace UI.GameResult {
	public class GameBlock2 : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI Round1;
		[SerializeField] private TextMeshProUGUI Round2;
		[SerializeField] private TextMeshProUGUI Round3;
		[SerializeField] private TextMeshProUGUI Round4;


		public void SetData(GameResultData.TeamData data) {
			var b = data.Buildings;
			Round1.text = b.Length > 0 ? BuildingsData.Data.GetBuilding(b[0]).Name : "";
			Round2.text = b.Length > 1 ? BuildingsData.Data.GetBuilding(b[1]).Name : "";
			Round3.text = b.Length > 2 ? BuildingsData.Data.GetBuilding(b[2]).Name : "";
			Round4.text = b.Length > 3 ? BuildingsData.Data.GetBuilding(b[3]).Name : "";
		}
	}
}