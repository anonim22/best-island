﻿using System.Collections;
using System.Collections.Generic;
using Game;
using Network;
using Network.Events;
using Network.ServerData;
using Newtonsoft.Json;
using TMPro;
using UI.RoundResult;
using UnityEngine;
using UnityEngine.UI;

namespace UI.GameResult {
	[RequireComponent(typeof(CanvasGroup))]
	public class GameResults : MonoBehaviour {
		public string TestData;

		[SerializeField] private CanvasGroup CanvasGroup;
		[SerializeField] private TextMeshProUGUI TopDirector;

		[Space]
		[SerializeField] private GameObject RoundsSelected;
		[SerializeField] private GameObject RoundsNotSelected;
		[SerializeField] private GameObject TenYearsSelected;
		[SerializeField] private GameObject TenYearsNotSelected;

		[Space]
		[SerializeField] private GameObject TeamBlock1;
		[SerializeField] private GameObject TeamBlock2, TeamBlock3, TeamBlock4, TeamBlock5;
		[Space]
		[SerializeField] private Transform Container1;
		[SerializeField] private Transform Container2, Container3, Container4, Container5;
		[Space]
		[SerializeField] private LayoutElement EventRow1;
		[SerializeField] private LayoutElement EventRow2, EventRow3, EventRow4;

		private readonly List<GameObject> _teamBlockObjects = new List<GameObject>();
		private readonly List<IGameBlock> _teamBlocks = new List<IGameBlock>();


		private void Start() {
			if (GameManager.GameEnd) {
				GetResult();
				return;
			}

			GameManager.OnGameEnd += GetResult;

			/*var data = JsonConvert.DeserializeObject<GameResultData>(TestData);
			SetData(data);
			ShowPanel();*/
		}


		private void GetResult() {
			PacketSender.GetResults(delegate (WebResponse r) {
				print(r.Text);
				if (r.Text == "waiting for admin") {
					GameManager.WaitingGameResult = true;
					EventsController.AddGameResultConfirmedListener(delegate (GameResultData data) {
						SetData(data);
						ShowPanel();
						GameManager.WaitingGameResult = false;
					});
				}
				else {
					var data = JsonConvert.DeserializeObject<GameResultData>(r.Text);
					SetData(data);
					ShowPanel();
				}
			});
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;

			ClearResults();
		}


		public void OnShowRounds() {
			_teamBlocks.ForEach(t => t.SetRoundsValues());

			RoundsSelected.SetActive(true);
			RoundsNotSelected.SetActive(false);
			TenYearsSelected.SetActive(false);
			TenYearsNotSelected.SetActive(true);
		}


		public void OnShow10Years() {
			_teamBlocks.ForEach(t => t.Set10YearsValues());

			RoundsSelected.SetActive(false);
			RoundsNotSelected.SetActive(true);
			TenYearsSelected.SetActive(true);
			TenYearsNotSelected.SetActive(false);
		}


		private void SetData(GameResultData data) {
			ClearResults();

			for (int i = 0; i < data.Teams.Count; i++) {
				CreateTeam(data.Teams[i], i);
			}

			TopDirector.text = data.TopDirector;
		}


		private void CreateTeam(GameResultData.TeamData data, int teamIndex) {
			var teamBlock1 = Instantiate(TeamBlock1, Container1).GetComponent<GameBlock1>();
			var teamBlock2 = Instantiate(TeamBlock2, Container2).GetComponent<GameBlock2>();
			var teamBlock3 = Instantiate(TeamBlock3, Container3).GetComponent<GameBlock3>();
			var teamBlock4 = Instantiate(TeamBlock4, Container4).GetComponent<GameBlock4>();
			var teamBlock5 = Instantiate(TeamBlock5, Container5).GetComponent<GameBlock5>();

			teamBlock1.SetData(data, teamIndex);
			teamBlock2.SetData(data);
			teamBlock3.SetData(data, EventRow1, EventRow2, EventRow3, EventRow4);
			teamBlock4.SetData(data);
			teamBlock5.SetData(data);

			_teamBlockObjects.Add(teamBlock1.gameObject);
			_teamBlockObjects.Add(teamBlock2.gameObject);
			_teamBlockObjects.Add(teamBlock3.gameObject);
			_teamBlockObjects.Add(teamBlock4.gameObject);
			_teamBlockObjects.Add(teamBlock5.gameObject);

			_teamBlocks.Add(teamBlock1);
			_teamBlocks.Add(teamBlock4);
		}


		private void ClearResults() {
			_teamBlockObjects.ForEach(Destroy);
			_teamBlockObjects.Clear();

			_teamBlocks.Clear();
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}