namespace UI.GameResult {
	public interface IGameBlock {
		void SetRoundsValues();
		void Set10YearsValues();
	}
}