using Game;
using Network.ServerData;
using TMPro;
using UnityEngine;

namespace UI.GameResult {
	public class GameBlock4 : MonoBehaviour, IGameBlock {
		[SerializeField] private TextMeshProUGUI MoneyFromOtherTeams;
		[SerializeField] private TextMeshProUGUI OpportunityIncome;
		[SerializeField] private TextMeshProUGUI AuctionIncome;
		[SerializeField] private TextMeshProUGUI AuctionCosts;
		[SerializeField] private TextMeshProUGUI MoneySent;
		[SerializeField] private TextMeshProUGUI EventCosts;
		[SerializeField] private TextMeshProUGUI LoanInterestPaid;

		private string _moneyFromOtherTeams, _moneyFromOtherTeams10;
		private string _opportunityIncome,   _opportunityIncome10;
		private string _auctionIncome,       _auctionIncome10;
		private string _auctionCosts,        _auctionCosts10;
		private string _moneySent,           _moneySent10;
		private string _eventCosts,          _eventCosts10;
		private string _loanInterestPaid,    _loanInterestPaid10;


		public void SetData(GameResultData.TeamData data) {
			_moneyFromOtherTeams = data.MoneyReceived.ToMoneyFormat();
			_opportunityIncome   = data.EventsRevenue.ToMoneyFormat();
			_auctionIncome       = data.AuctionRevenue.ToMoneyFormat();
			_auctionCosts        = data.AuctionCosts.ToMoneyFormat();
			_moneySent           = data.MoneySent.ToMoneyFormat();
			_eventCosts          = data.EventsCosts.ToMoneyFormat();
			_loanInterestPaid    = data.AnnualPayment.ToMoneyFormat();

			_moneyFromOtherTeams10 = data.MoneyReceived.ToMoneyFormat();
			_opportunityIncome10   = data.EventsRevenue10.ToMoneyFormat();
			_auctionIncome10       = data.AuctionRevenue10.ToMoneyFormat();
			_auctionCosts10        = data.AuctionCosts.ToMoneyFormat();
			_moneySent10           = data.MoneySent.ToMoneyFormat();
			_eventCosts10          = data.EventsCosts.ToMoneyFormat();
			_loanInterestPaid10    = data.AnnualPayment10.ToMoneyFormat();

			SetRoundsValues();
		}


		public void SetRoundsValues() {
			MoneyFromOtherTeams.text = _moneyFromOtherTeams;
			OpportunityIncome.text   = _opportunityIncome;
			AuctionIncome.text       = _auctionIncome;
			AuctionCosts.text        = _auctionCosts;
			MoneySent.text           = _moneySent;
			EventCosts.text          = _eventCosts;
			LoanInterestPaid.text    = _loanInterestPaid;
		}


		public void Set10YearsValues() {
			MoneyFromOtherTeams.text = _moneyFromOtherTeams10;
			OpportunityIncome.text   = _opportunityIncome10;
			AuctionIncome.text       = _auctionIncome10;
			AuctionCosts.text        = _auctionCosts10;
			MoneySent.text           = _moneySent10;
			EventCosts.text          = _eventCosts10;
			LoanInterestPaid.text    = _loanInterestPaid10;
		}
	}
}