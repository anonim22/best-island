using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace UI.GameResult {
	public class GameBlock5 : MonoBehaviour {
		[SerializeField] private Slider          CFO_Efficiency;
		[SerializeField] private TextMeshProUGUI CFO_EfficiencyText;
		[SerializeField] private Slider          HR_Efficiency;
		[SerializeField] private TextMeshProUGUI HR_EfficiencyText;
		[SerializeField] private Slider          Building_Efficiency;
		[SerializeField] private TextMeshProUGUI Building_EfficiencyText;
		[SerializeField] private Slider          AdminScore;
		[SerializeField] private TextMeshProUGUI AdminScoreText;
		[SerializeField] private Slider          GameScore;
		[SerializeField] private TextMeshProUGUI GameScoreText;


		public void SetData(GameResultData.TeamData data) {
			CFO_Efficiency.value         = data.FMEfficiency;
			CFO_EfficiencyText.text      = data.FMEfficiency.ToString();
			HR_Efficiency.value          = data.HREfficiency;
			HR_EfficiencyText.text       = data.HREfficiency.ToString();
			Building_Efficiency.value    = data.BuilderEfficiency;
			Building_EfficiencyText.text = data.BuilderEfficiency.ToString();
			AdminScore.value             = data.DirectorEfficiency;
			AdminScoreText.text          = data.DirectorEfficiency.ToString();
			GameScore.value              = data.GameEfficiency;
			GameScoreText.text           = data.GameEfficiency.ToString();
		}
	}
}