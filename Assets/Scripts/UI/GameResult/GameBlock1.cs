using Game;
using Game.Data;
using Network.ServerData;
using TMPro;
using UnityEngine;

namespace UI.GameResult {
	public class GameBlock1 : MonoBehaviour, IGameBlock {
		[SerializeField] private TextMeshProUGUI TeamNum;
		[SerializeField] private TextMeshProUGUI TeamName;
		[SerializeField] private TextMeshProUGUI GameResult;

		private string _gameResult, _gameResult10;


		public void SetData(GameResultData.TeamData data, int teamNum) {
			TeamNum.text    = "Команда " + (teamNum + 1);
			TeamName.text   = data.TeamName;

			_gameResult = data.MoneyResult.ToMoneyFormat();
			_gameResult10 = data.MoneyResult10.ToMoneyFormat();

			SetRoundsValues();
		}


		public void SetRoundsValues() {
			GameResult.text = _gameResult;
		}


		public void Set10YearsValues() {
			GameResult.text = _gameResult10;
		}
	}
}