﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Data;
using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.RoundResult {
	[RequireComponent(typeof(CanvasGroup))]
	public class GameRoundResults : MonoBehaviour {
		[SerializeField] private CanvasGroup     CanvasGroup;
		[SerializeField] private TextMeshProUGUI RoundNumber;
		[SerializeField] private GameObject      TeamBlock1,          TeamBlock2,          TeamBlock3;
		[SerializeField] private Transform       TeamBlockContainer1, TeamBlockContainer2, TeamBlockContainer3;
		[SerializeField] private LayoutElement   EventRow;

		private readonly List<GameObject> _teamBlocks = new List<GameObject>();


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
		}


		public void ShowResult(RoundResultData data) {
			ClearResults();
			SetData(data);
			ShowPanel();
		}


		private void SetData(RoundResultData data) {
			RoundNumber.text = data.Teams.FirstOrDefault()?.Round.ToString();
			for (int i = 0; i < data.Teams.Count; i++) {
				CreateTeam(data.Teams[i], i);
			}
		}


		private void CreateTeam(RoundResultData.TeamData data, int teamIndex) {
			var teamBlock1 = Instantiate(TeamBlock1, TeamBlockContainer1).GetComponent<RoundBlock1>();
			var teamBlock2 = Instantiate(TeamBlock2, TeamBlockContainer2).GetComponent<RoundBlock2>();
			var teamBlock3 = Instantiate(TeamBlock3, TeamBlockContainer3).GetComponent<RoundBlock3>();

			teamBlock1.SetData(data, teamIndex);
			teamBlock2.SetData(data, EventRow);
			teamBlock3.SetData(data);

			_teamBlocks.Add(teamBlock1.gameObject);
			_teamBlocks.Add(teamBlock2.gameObject);
			_teamBlocks.Add(teamBlock3.gameObject);
		}


		private void ClearResults() {
			_teamBlocks.ForEach(Destroy);
			_teamBlocks.Clear();
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}