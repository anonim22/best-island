using System;
using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.RoundResult {
	public class RoundBlock2 : MonoBehaviour {
		private LayoutElement EventRow;

		[SerializeField] private TextMeshProUGUI Area;
		[SerializeField] private TextMeshProUGUI InTime;
		[SerializeField] private TextMeshProUGUI LoanPercentPayed;
		[SerializeField] private TextMeshProUGUI Events;
		[SerializeField] private TextMeshProUGUI Insurance;

		private bool _heightUpdated;


		private void LateUpdate() {
			// Обновление размеров 1 раз. Если вызывать в SetData, то к тому
			// моменту размер текста еще не будет рассчитан компонентом
			// VerticalLayoutGroup и будет записано неверное значение
			if (!_heightUpdated) {
				UpdateHeight();
			}
		}


		public void SetData(RoundResultData.TeamData data, LayoutElement eventRow) {
			EventRow = eventRow;

			var gameEvents       = !string.IsNullOrEmpty(data.Events) ? data.Events.Split(',') : null;
			var disasters        = !string.IsNullOrEmpty(data.Disasters) ? data.Disasters.Split(',') : null;

			var gameEventsString = gameEvents != null ? string.Join(Environment.NewLine, gameEvents) : "";
			var disastersString  = disasters  != null ? string.Join(Environment.NewLine, disasters) : "";

			var events           = gameEventsString;

			if (!string.IsNullOrEmpty(disastersString)) {
				if (!string.IsNullOrEmpty(gameEventsString)) events += Environment.NewLine;
				events += disastersString;
			}

			Area.text             = data.AreaEfficiency   == 1 ? "Выбрана верно" : "Выбрана неверно";
			InTime.text           = data.BuiltOnTime      == 1 ? "Да" : "Нет";
			LoanPercentPayed.text = data.LoanPercentPayed == 1 ? "Да" : "Нет";
			Events.text           = events;
			Insurance.text        = data.Insurance;
		}


		private void UpdateHeight() {
			var height = Events.gameObject.GetComponent<RectTransform>().sizeDelta.y;
			print($"Height {height}");
			if (EventRow.preferredHeight > height) return;
			EventRow.preferredHeight = height;

			_heightUpdated = true;
		}
	}
}