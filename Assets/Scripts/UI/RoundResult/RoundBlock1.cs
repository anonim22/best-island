using Game;
using Game.Data;
using Network.ServerData;
using TMPro;
using UnityEngine;

namespace UI.RoundResult {
	public class RoundBlock1 : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI TeamNum;
		[SerializeField] private TextMeshProUGUI TeamName;
		[SerializeField] private TextMeshProUGUI Building;
		[SerializeField] private TextMeshProUGUI Income;
		[SerializeField] private TextMeshProUGUI Expenses;
		[SerializeField] private TextMeshProUGUI RoundResult;
		[SerializeField] private TextMeshProUGUI GameResult;


		public void SetData(RoundResultData.TeamData data, int teamNum) {
			TeamNum.text     = "Команда " + (teamNum + 1);
			TeamName.text    = data.TeamName;
			Building.text    = BuildingsData.Data.GetBuilding(data.BuildingId).Name;
			Income.text      = data.Revenue.ToMoneyFormat();
			Expenses.text    = data.MoneySpent.ToMoneyFormat();
			RoundResult.text = data.RoundResult.ToMoneyFormat();
			GameResult.text  = data.GameResult.ToMoneyFormat();
		}
	}
}