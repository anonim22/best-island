using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.RoundResult {
	public class RoundBlock3 : MonoBehaviour {
		[SerializeField] private Slider          CFO_Efficiency;
		[SerializeField] private TextMeshProUGUI CFO_EfficiencyText;
		[SerializeField] private Slider          HR_Efficiency;
		[SerializeField] private TextMeshProUGUI HR_EfficiencyText;
		[SerializeField] private Slider          Building_Efficiency;
		[SerializeField] private TextMeshProUGUI Building_EfficiencyText;
		[SerializeField] private Slider          AdminScore;
		[SerializeField] private TextMeshProUGUI AdminScoreText;
		[SerializeField] private Slider          RoundScore;
		[SerializeField] private TextMeshProUGUI RoundScoreText;


		public void SetData(RoundResultData.TeamData data) {
			CFO_Efficiency.value         = data.FMEfficiency;
			CFO_EfficiencyText.text      = data.FMEfficiency.ToString();
			HR_Efficiency.value          = data.HREfficiency;
			HR_EfficiencyText.text       = data.HREfficiency.ToString();
			Building_Efficiency.value    = data.BuilderEfficiency;
			Building_EfficiencyText.text = data.BuilderEfficiency.ToString();
			AdminScore.value             = data.DirectorEfficiency;
			AdminScoreText.text          = data.DirectorEfficiency.ToString();
			RoundScore.value             = data.RoundEfficiency;
			RoundScoreText.text          = data.RoundEfficiency.ToString();
		}
	}
}