﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


[RequireComponent(typeof(CanvasGroup))]
public class ParticipantSelection : MonoBehaviour
{
    CanvasGroup uiElement;
    [SerializeField] private TextMeshProUGUI[] name;

    void Start()
    {
        uiElement = GetComponent<CanvasGroup>();
    }

    public void SetName(string[] Name) 
    {
        for (int i = 0; i < name.Length; i++) 
        {
            name[i].text = Name[i];
        }
    }

    public void ShowPanel()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 1, .5f));
        uiElement.blocksRaycasts = true;
        GameInterface.InterfaceIsOpen = true;
    }

    public void HidePanel()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 0, .5f));
        uiElement.blocksRaycasts = false;
        GameInterface.InterfaceIsOpen = false;
    }

    public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            cg.alpha = currentValue;

            if (percentageComplete >= 1) break;

            yield return new WaitForFixedUpdate();
        }

    }
}
