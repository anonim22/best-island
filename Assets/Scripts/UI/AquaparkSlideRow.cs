﻿using System.Linq;
using Game;
using Game.Data;
using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
	public class AquaparkSlideRow : MonoBehaviour {
		[Tooltip("From 1 to 5")] [SerializeField]
		private int SlideId;
		[SerializeField] private TextMeshProUGUI Price;
		[SerializeField] private TextMeshProUGUI Status;
		[SerializeField] private GameObject      InstallButton;


		private void Start () {
			AquaparkSlides.OnSlideInstalledEvent += UpdateData;
			Storage.OnStorageUpdate              += UpdateData;
			UpdateData();
		}


		private void UpdateData() {
			var material = AquaparkSlides.GetSlideMaterial(SlideId);
			Price.text = Money.Format((int) material.Price);

			if (AquaparkSlides.SlideIsPlaced(SlideId)) {
				Status.text = "Установлено";
				InstallButton.SetActive(false);
				AquaparkSlides.OnSlideInstalledEvent -= UpdateData;
				Storage.OnStorageUpdate              -= UpdateData;
			}
			else if (AquaparkSlides.SlideInStorage(SlideId)) {
				InstallButton.SetActive(true);
				Storage.OnStorageUpdate -= UpdateData;
			}
			else {
				Status.text = "Нет на складе";
				InstallButton.SetActive(false);
			}
		}


		public void OnInstallSlide() {
			AquaparkSlides.InstallSlide(SlideId);
		}
	}
}