﻿using System.Collections;
using System.Globalization;
using System.Linq;
using Game;
using Game.Data;
using UnityEngine;
using TMPro;
using UI;

[RequireComponent(typeof(CanvasGroup))]
public class GameInterface : MonoBehaviour {
    public static bool InterfaceIsOpen;

    CanvasGroup uiElement;
    [SerializeField] private GameObject Tip;
    [SerializeField] private TextMeshProUGUI TipText;

    void Start()
    {
        uiElement = GetComponent<CanvasGroup>();
    }

    public void ShowPanel()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 1, .5f));
        uiElement.blocksRaycasts = true;
    }

    public void HidePanel()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 0, .5f));
        uiElement.blocksRaycasts = false;
    }

    public void SetTip(string text)
    {
        Tip.SetActive(true);
        TipText.text = text;
    }


    public void HideTipe()
    {
        Tip.SetActive(false);
    }


    #region Button Callbacks

    public void OnLKBtn()
    {
        if(!InterfaceIsOpen) UI.UIManager.LKBuildings.ShowPanel();
    }

    public void OnConstructBuildingBtn()
    {
        if(!InterfaceIsOpen) UI.UIManager.ConstructionBuildings.ShowPanel();
    }

    public void OnRoleSelectionBtn()
    {
        if(!InterfaceIsOpen) UI.UIManager.RoleSelection.ShowPanel();
    }

    public void OnHRBtn()
    {
        if(!InterfaceIsOpen) UI.UIManager.HR_Panel.ShowPanel();
    }

    public void OnShopBtn()
    {
        if(!InterfaceIsOpen) UI.UIManager.MaterialShopOrders.ShowPanel();
    }

    public void OnBuildingsBtn()
    {
        if(!InterfaceIsOpen) UI.UIManager.LKBuildings.ShowPanel();
    }

    public void OnCharteringBtn()
    {
        if(!InterfaceIsOpen) UI.UIManager.ShipsPanel.ShowPanel();
    }

    public void OnNextRoundBtn() {
        if (InterfaceIsOpen) return;

        if (Buildings.BuildingsData.All(b => b.Round != Rounds.Round)) {
            Notification.Create("Постройте здание перед завершением раунда");
            return;
        }

        UI.UIManager.NextRound.ShowPanel();
    }

    public void OnMaterialBlockPanelBtn()
    {
        if (!InterfaceIsOpen) UI.UIManager.MaterialsBlockPanel.ShowPanel();
        InterfaceIsOpen = false;
    }

    #endregion


    public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            cg.alpha = currentValue;

            if (percentageComplete >= 1) break;

            yield return new WaitForFixedUpdate();
        }

    }
}
