﻿using System.Linq;
using Game;
using Game.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.MaterialShop {
	public class MaterialCard : MonoBehaviour {
		public                    int             MaterialId;
		[SerializeField]  private Image           Icon;
		[SerializeField]  private TextMeshProUGUI NameText;
		[SerializeField]  private TextMeshProUGUI WeightText;
		[SerializeField]  private TextMeshProUGUI PriceText;
		[SerializeField]  private TextMeshProUGUI SizeText;
		[SerializeField]  private TMP_InputField  QuantityText;
		[HideInInspector] public  int             Quantity;
		[HideInInspector] public  bool            OneQuantity;


		void Start() {
			LoadMaterial();
			ResetQuantity();
		}


		public void ResetQuantity() {
			SetQuantity(0);
		}


		public void PlusBtn() {
			SetQuantity(Quantity + 1000);
		}


		public void MinusBtn() {
			SetQuantity(Quantity - 1000);
		}


		private void SetQuantity(int quantity) {
			quantity          = quantity < 0 ? 0 : quantity;
			Quantity          = quantity;
			QuantityText.text = quantity.ToString();
		}


		public void SetQuantityInput() {
			var quantity = 0;
			if (!string.IsNullOrEmpty(QuantityText.text)) quantity = int.Parse(QuantityText.text);
			if (OneQuantity && quantity > 0) {
				if (Storage.Materials.FirstOrDefault(m => m.Id == MaterialId) != null) {
					Quantity          = 0;
					QuantityText.text = "0";
					Notification.Create("Уже куплено");
					return;
				}
				quantity = 1;
				QuantityText.text = "1";
			}
			Quantity = quantity;
		}


		private void LoadMaterial() {
			var material = MaterialsData.Data.Materials.Single(m => m.Id == MaterialId);

			Icon.sprite     = material.Icon;
			NameText.text   = material.Name;
			WeightText.text = material.Weight.ToString();
			PriceText.text  = Money.Format(material.Price);
			SizeText.text   = material.Size;
			OneQuantity     = material.OneQuantity;
		}
	}
}