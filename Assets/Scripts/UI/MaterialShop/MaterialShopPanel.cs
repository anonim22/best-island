﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Data;
using Game.Island;
using Network;
using Network.ServerData;
using Newtonsoft.Json;
using UI.MaterialShop;
using UnityEngine;
using UnityEngine.UI;

public class MaterialShopPanel : MonoBehaviour {
	[SerializeField] private CanvasGroup        CanvasGroup;
	[SerializeField] private List<MaterialCard> Materials;


	public void ShowPanel() {
		StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
		CanvasGroup.blocksRaycasts    = true;
		GameInterface.InterfaceIsOpen = true;

		Materials.ForEach(m => m.ResetQuantity());
	}


	public void HidePanel() {
		StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
		CanvasGroup.blocksRaycasts    = false;
		GameInterface.InterfaceIsOpen = false;
	}


	public void Buy() {
		var order = new Game.Data.Order();

		// Добавляем в заказ материалы
		Materials.Where(m => m.Quantity > 0).ToList().ForEach(m => order.Materials.Add(new OrderMaterial(m)));
		if (order.Materials.Count == 0) return;

		int[] materialsId = new int[order.Materials.Count];
		int[] quantity    = new int[order.Materials.Count];

		for (var index = 0; index < order.Materials.Count; index++) {
			var material = order.Materials[index];
			materialsId[index] = material.Id;
			quantity[index]    = material.Quantity;
		}

		PacketSender.CreateOrder(materialsId, quantity, OnOrderCreated);
	}


	private void OnOrderCreated(WebResponse response) {
		Debug.Log(response.Text);
		var order = OrderData.Parse(JsonConvert.DeserializeObject<OrderData>(response.Text));
		Game.Data.Order.AddOrder(order);
		HidePanel();
		UI.UIManager.MaterialShopOrders.ShowPanel();
	}


	private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
		float _timeStartedLerping = Time.time;
		float timeSinceStarted    = Time.time - _timeStartedLerping;
		float percentageComplete  = timeSinceStarted / lerpTime;

		while (true) {
			timeSinceStarted   = Time.time - _timeStartedLerping;
			percentageComplete = timeSinceStarted / lerpTime;

			float currentValue = Mathf.Lerp(start, end, percentageComplete);

			cg.alpha = currentValue;

			if (percentageComplete >= 1) break;

			yield return new WaitForFixedUpdate();
		}
	}
}