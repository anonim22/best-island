﻿using System.Globalization;
using System.Linq;
using Game;
using Network;
using TMPro;
using UnityEngine;

namespace UI.MaterialShop {
	public class Order : MonoBehaviour {
		[SerializeField] private Transform       MaterialsContainer;
		[SerializeField] private GameObject      MaterialRowPrefab;
		[SerializeField] private TextMeshProUGUI OrderIdText;
		[SerializeField] private TextMeshProUGUI OrderStatusText;
		[SerializeField] private TextMeshProUGUI OrderSumText;
		[SerializeField] private TextMeshProUGUI OrderWeightText;
		[SerializeField] private GameObject      PayBtn;
		[SerializeField] private GameObject      CancelBtn;

		public Game.Data.Order    OrderData;
		public MaterialShopOrders MaterialShopOrders;


		public void SetData(Game.Data.Order order) {
			OrderData            = order;
			OrderIdText.text     = order.OrderId.ToString();
			OrderSumText.text    = ((int) (order.GetSum() / 1000)).ToString(CultureInfo.InvariantCulture);
			OrderWeightText.text = order.GetWeight().ToString(CultureInfo.InvariantCulture);

			if (order.Payed) SetPayed();

			CreateMaterialRows();
		}


		public void OnPayBtn() {
			if (OrderData.GetSum() > Money.Balance) {
				Notification.Create("Недостаточно средств");
				return;
			}

			if (OrderData.Materials.FirstOrDefault(m => m.Id == 14 || m.Id == 15) != null) {
				Notification.Create("Волнорез установлен");
			}

			PacketSender.PayOrder(OrderData.Id, OnPayResponse, OnPayResponseError);
		}


		public void OnCancelBtn() {
			PacketSender.RemoveOrder(OrderData.Id, OnCancelResponse);
		}


		private void OnPayResponse(WebResponse response) {
			if (response.Text != "ok") return;
			SetPayed();
		}


		private void OnPayResponseError(WebResponse response) {
			Notification.Create("Недостаточно средств");
		}


		private void OnCancelResponse(WebResponse response) {
			if (response.Text != "ok") return;
			Game.Data.Order.RemoveOrder(OrderData);
		}


		private void SetPayed() {
			OrderStatusText.text = "Оплачено, ожидает доставки";
			OrderData.Payed      = true;
			PayBtn.SetActive(false);
			CancelBtn.SetActive(false);
		}


		private void CreateMaterialRows() {
			foreach (var material in OrderData.Materials) {
				var row = Instantiate(MaterialRowPrefab, MaterialsContainer);
				row.GetComponent<Material>().SetData(material);
			}
		}
	}
}