﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Data;
using Network;
using Network.Cache.Data;
using Network.ServerData;
using UnityEngine;
using UnityEngine.UI;
using Order = UI.MaterialShop.Order;

[RequireComponent(typeof(CanvasGroup))]
public class MaterialShopOrders : MonoBehaviour
{
    [SerializeField] private CanvasGroup CanvasGroup;
    [SerializeField] private Transform OrdersParent;
    [SerializeField] private GameObject OrderPrefab;

    private List<Order> OrderRows = new List<Order>();


    public void Start() {
        Game.Data.Order.OnUpdateOrderEvent += UpdateOrders;
        UpdateOrders();
    }

    public void ShowPanel()
    {
        StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
        CanvasGroup.blocksRaycasts = true;
        GameInterface.InterfaceIsOpen = true;
    }

    public void HidePanel()
    {
        StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
        CanvasGroup.blocksRaycasts = false;
        GameInterface.InterfaceIsOpen = false;
    }
    
    public void OnNewOrderBtn()
    {
        HidePanel();
        UI.UIManager.MaterialShopPanel.ShowPanel();
    }


    private void DestroyOrders()
    {
        OrderRows.ForEach(o => Destroy(o.gameObject));
        OrderRows.Clear();
    }


    private void UpdateOrders() {
        DestroyOrders();
        LoadOrders();
    }


    private void LoadOrders()
    {
        foreach (var order in Game.Data.Order.Orders)
        {
            if (order.TeamId != TeamDataCache.ActiveTeamId)  continue;

            GameObject orderObject = Instantiate(OrderPrefab, OrdersParent);
            var orderRow = orderObject.GetComponent<Order>();
            orderRow.SetData(order);
            orderRow.MaterialShopOrders = this;
            OrderRows.Add(orderRow);
        }
    }


    private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            cg.alpha = currentValue;

            if (percentageComplete >= 1) break;

            yield return new WaitForFixedUpdate();
        }

    }
}
