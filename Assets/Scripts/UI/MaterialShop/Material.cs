﻿using System.Linq;
using Game;
using Game.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.MaterialShop {
	public class Material : MonoBehaviour {
		[SerializeField] private Image           Icon;
		[SerializeField] private TextMeshProUGUI MaterialName;
		[SerializeField] private TextMeshProUGUI Quantity;
		[SerializeField] private TextMeshProUGUI Price;


		public void SetData(OrderMaterial order) {
			Icon.sprite       = MaterialsData.Data.Materials.Single(m => m.Id == order.Id).Icon;
			MaterialName.text = order.Name;
			Quantity.text     = order.Quantity.ToString();
			Price.text        = Money.Format(order.PriceForPiece * order.Quantity);
		}
	}
}