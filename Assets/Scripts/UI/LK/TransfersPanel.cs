﻿using System.Collections;
using System.Collections.Generic;
using Game.Data;
using Network.Events;
using Network.ServerData;
using UnityEngine;

namespace UI.LK {
	[RequireComponent(typeof(CanvasGroup))]
	public class TransfersPanel : MonoBehaviour {
		[SerializeField] private CanvasGroup    CanvasGroup;
		[SerializeField] private CreateTransfer CreateTransferPanel;
		[SerializeField] private GameObject     TransferRowPrefab;
		[SerializeField] private Transform      TransfersContainer;

		private List<TransferRow> _transfers = new List<TransferRow>();

		private void Start() {
			UpdateRows();
			EventsController.AddTransferUpdateListener(transfer => UpdateRows());
		}


		private void UpdateRows() {
			DestroyRows();
			CreateRows();
		}


		private void CreateRows() {
			Transfers.TransferData.ForEach(t => _transfers.Add(CreateRow(t)));
		}


		private TransferRow CreateRow(TransferData transfer) {
			var row = Instantiate(TransferRowPrefab, TransfersContainer).GetComponent<TransferRow>();
			row.SetData(transfer);
			return row;
		}


		private void DestroyRows() {
			_transfers.ForEach(t => Destroy(t.gameObject));
			_transfers.Clear();
		}


		public void OnCreateTransferBtn() {
			CreateTransferPanel.ShowPanel();
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			var   timeStartedLerping = Time.time;
			float timeSinceStarted   = Time.time - timeStartedLerping;

			while (true) {
				timeSinceStarted = Time.time - timeStartedLerping;
				var percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}