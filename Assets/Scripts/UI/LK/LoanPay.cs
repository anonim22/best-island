﻿using System.Collections;
using Network;
using Network.ServerData;
using TMPro;
using UnityEngine;

namespace UI.LK {
	[RequireComponent(typeof(CanvasGroup))]
	public class LoanPay : MonoBehaviour {
		[SerializeField] private CanvasGroup    CanvasGroup;
		[SerializeField] private TMP_InputField Input;

		public LoanData Loan;


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
		}


		public void OnPay() {
			if(string.IsNullOrEmpty(Input.text)) return;

			var sum = int.Parse(Input.text);
			if (sum <= 0) return;

			PacketSender.PayLoan(Loan.Id, sum);
			HidePanel();
		}


		public void SetLoan(LoanData data) {
			Loan = data;
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}