﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Data;
using Network.ServerData;
using UnityEngine;
using TMPro;
using UI;

public class BuildingRow : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI Round;
    [SerializeField] private TextMeshProUGUI Building;
    [SerializeField] private TextMeshProUGUI Area;
    [SerializeField] private TextMeshProUGUI Income;
    [SerializeField] private TextMeshProUGUI Status;

    private BuildedData _data;


    public void SetData(BuildedData data) {
        var building = BuildingsData.Data.GetBuilding(data.BuildingId);

        Round.text = data.Round.ToString();
        Building.text = building.Name;
        Area.text = data.AreaId.ToString();
        Status.text = data.Stage >= building.Stages.Length ? "Построено" : "Строится";
        Income.text = "0";

        if (Rounds.Round > data.Round || GameManager.RoundEnd) {
            var roundResult = Rounds.RoundResults.FirstOrDefault(r => r.Round == data.Round);
            if (roundResult != null) {
                Income.text = Money.Format(roundResult.CurrentTeam.RevenueBuild);
            }
        }

        _data = data;
    }


    public void OnClick() {
        if (Rounds.Round > _data.Round || GameManager.RoundEnd) {
            var roundResult = Rounds.RoundResults.FirstOrDefault(r => r.Round == _data.Round);
            UIManager.GameRoundResults.ShowResult(roundResult);
        }
    }
}
