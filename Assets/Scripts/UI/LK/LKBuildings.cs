﻿using System.Collections;
using System.Collections.Generic;
using Game.Data;
using Network.Events;
using Network.ServerData;
using UnityEngine;

namespace UI.LK {
	[RequireComponent(typeof(CanvasGroup))]
	public class LKBuildings : MonoBehaviour {
		[SerializeField] private CanvasGroup CanvasGroup;
		[SerializeField] private GameObject  BuildingRowPrefab;
		[SerializeField] private Transform   BuildingsContainer;

		private List<BuildingRow> _rows = new List<BuildingRow>();


		private void Start() {
			UpdateBuildings();
			Buildings.OnBuildingsUpdate += UpdateBuildings;
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
		}


		private void UpdateBuildings() {
			DestroyRows();
			Buildings.BuildingsData.ForEach(b => _rows.Add(CreateBuildingRow(b)));
		}


		private BuildingRow CreateBuildingRow(BuildedData data) {
			var row = Instantiate(BuildingRowPrefab, BuildingsContainer).GetComponent<BuildingRow>();
			row.SetData(data);
			return row;
		}


		private void DestroyRows() {
			_rows.ForEach(r => Destroy(r.gameObject));
			_rows.Clear();
		}


		public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}