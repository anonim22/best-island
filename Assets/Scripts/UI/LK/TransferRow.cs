﻿using System.Linq;
using Network;
using Network.Cache;
using Network.Cache.Data;
using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.LK {
	public class TransferRow : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI Sender;
		[SerializeField] private TextMeshProUGUI Receiver;
		[SerializeField] private TextMeshProUGUI Sum;
		[SerializeField] private Button          CancelBtn;

		private TransferData _transfer;


		public void SetData(TransferData data) {
			Sender.text = (data.Sender == TeamDataCache.ActiveTeamId)
				? "Вы"
				: CacheManager.TeamDataCache.Teams.FirstOrDefault(t => t.ID == data.Sender)?.TeamName;

			Receiver.text = (data.ReceiverTeam == TeamDataCache.ActiveTeamId)
				? "Вы"
				: CacheManager.TeamDataCache.Teams.FirstOrDefault(t => t.ID == data.ReceiverTeam)?.TeamName;

			Sum.text      = data.Sum.ToString();
			CancelBtn.interactable = data.Sender == TeamDataCache.ActiveTeamId;

			_transfer = data;
		}


		public void OnCancelBtn() {
			PacketSender.CancelTransfer(_transfer.Id, response => gameObject.SetActive(false));
		}
	}
}