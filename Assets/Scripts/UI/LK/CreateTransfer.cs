﻿using System.Collections;
using System.Linq;
using Network;
using Network.Cache;
using Network.Cache.Data;
using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.LK {
	[RequireComponent(typeof(CanvasGroup))]
	public class CreateTransfer : MonoBehaviour {
		[SerializeField] private CanvasGroup    CanvasGroup;
		[SerializeField] private TMP_Dropdown   Dropdown;
		[SerializeField] private TMP_InputField InputSum;
		[SerializeField] private Toggle         OneTimePayToggle;
		[SerializeField] private Toggle         EveryRoundPayToggle;


		private void Start() {
			LoadTeams();
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts = true;

			OneTimePayToggle.isOn    = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts = false;
		}


		private void LoadTeams() {
			Dropdown.options.Clear();
			var teams = CacheManager.TeamDataCache.Teams.Where(t => t.ID != TeamDataCache.ActiveTeamId).ToList();
			teams.ForEach(t => Dropdown.options.Add(new TMP_Dropdown.OptionData(t.TeamName)));
			Dropdown.RefreshShownValue();
		}


		public void SendMoney() {
			if (string.IsNullOrEmpty(InputSum.text)) return;

			int sum = int.Parse(InputSum.text);
			if (sum <= 0) return;

			var teams = CacheManager.TeamDataCache.Teams.Where(t => t.ID != TeamDataCache.ActiveTeamId).ToList();
			int targetTeam = teams[Dropdown.value].ID;
			int oneTimePay = OneTimePayToggle.isOn ? 0 : 1;

			PacketSender.SendMoney(targetTeam, sum, oneTimePay, response => HidePanel());
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}