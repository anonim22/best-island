﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Data;
using Network.Events;
using Network.ServerData;
using UnityEngine;

namespace UI.LK {
	[RequireComponent(typeof(CanvasGroup))]
	public class LoanPanel : MonoBehaviour {
		[SerializeField] private CanvasGroup CanvasGroup;
		[SerializeField] private LoanRequest LoanRequestPanel;
		[SerializeField] private LoanPay     LoanPayPanel;
		[SerializeField] private GameObject  LoanRowPrefab;
		[SerializeField] private Transform   LoanContainer;

		private List<LoanRow> _rows = new List<LoanRow>();


		private void Start() {
			EventsController.AddLoansUpdateListener(UpdateLoans);
			Rounds.AddRoundStartListener(delegate { UpdateLoans(Loan.Loans); });
			UpdateLoans(Loan.Loans);
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
		}


		private void UpdateLoans(List<LoanData> loans) {
			DestroyLoanRows();
			loans.Where(l => l.Status == "approved" || l.Status == "request").ToList()
			.ForEach(l => _rows.Add(CreateLoanRow(l)));
		}


		private LoanRow CreateLoanRow(LoanData loan) {
			var row = Instantiate(LoanRowPrefab, LoanContainer).GetComponent<LoanRow>();
			row.LoanPayPanel = LoanPayPanel;
			row.SetData(loan);
			return row;
		}


		private void DestroyLoanRows() {
			_rows.ForEach(r => Destroy(r.gameObject));
			_rows.Clear();
		}


		public void OnCreateRequestBtn() {
			LoanRequestPanel.ShowPanel();
		}


		public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}