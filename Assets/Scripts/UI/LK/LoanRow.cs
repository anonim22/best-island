﻿using Game;
using Network.ServerData;
using TMPro;
using UnityEngine;

namespace UI.LK {
	public class LoanRow : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI Sum;
		[SerializeField] private TextMeshProUGUI Balance;
		[SerializeField] private TextMeshProUGUI ToPay;
		[SerializeField] private TextMeshProUGUI Duration;

		[SerializeField] private GameObject Status;
		[SerializeField] private GameObject PayBtn;

		[HideInInspector] public LoanPay  LoanPayPanel;
		private                  LoanData _loan;


		public void SetData(LoanData data) {
			Sum.text     = Money.Format(data.Loan);
			Balance.text = Money.Format(data.Balance);
			ToPay.text   = Money.Format(data.ToPay);

			var roundsLeft = 5 - Rounds.Round + data.RequestRound;

			if (data.Status == "approved") {
				Status.SetActive(false);
				PayBtn.SetActive(true);
				Duration.text = $"{roundsLeft}/5 лет";
			}
			else {
				Duration.text = "5/5 лет";
			}

			_loan = data;
		}


		public void OnPayBtn() {
			LoanPayPanel.SetLoan(_loan);
			LoanPayPanel.ShowPanel();
		}
	}
}