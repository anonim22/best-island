﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace UI {
	public class Notification : MonoBehaviour {
		private static Notification Instance;

		[SerializeField] private AudioSource AudioSource;
		[SerializeField] private float       CreatingSpeedTime = 5f;
		[SerializeField] private float       WaitingTime       = 5f;
		[SerializeField] private GameObject  NotificationPrefab;

		private WaitForSeconds _wait;
		private Transform      _t;


		private void Start() {
			Instance = this;
			_t       = transform;
			DontDestroyOnLoad(_t.parent);
			_wait = new WaitForSeconds(WaitingTime);
		}


		public static void Create(string textNotification, int delay = 0) {
			var n = Instance.CreateObject(textNotification);
			Instance.AudioSource.Play();
			Instance.StartCoroutine(Instance.Process(n, delay));
		}


		private GameObject CreateObject(string textNotification) {
			var spawnedNotification = Instantiate(NotificationPrefab, _t.position, _t.rotation, _t.transform);
			spawnedNotification.GetComponentInChildren<TextMeshProUGUI>().text = textNotification;

			return spawnedNotification;
		}


		private IEnumerator Process(GameObject instance, int delay = 0) {
			var rect  = instance.transform.GetChild(0).GetComponent<RectTransform>();
			var size  = rect.sizeDelta;
			var width = size.x;
			rect.anchoredPosition = new Vector2(-width + 1, 0);

			while (rect.anchoredPosition.x < 0) {
				var progress = (rect.anchoredPosition.x + width) / width; // 0..1f
				var delta    = Mathf.Sin(progress * 3f);

				rect.anchoredPosition += new Vector2(delta * width * Time.deltaTime * CreatingSpeedTime, 0);
				yield return null;
			}

			rect.anchoredPosition = Vector2.zero;
			if (delay > 0) yield return new WaitForSeconds(delay);
			else yield return _wait;

			while (-width - rect.anchoredPosition.x < -1 ) {
				var progress = (rect.anchoredPosition.x + width) / width; // 0..1f
				var delta    = Mathf.Sin(progress * 3f);

				rect.anchoredPosition -= new Vector2(delta * width * Time.deltaTime * CreatingSpeedTime, 0);
				yield return null;
			}

			Destroy(instance);
		}
	}
}