using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Network;
using Network.Cache;
using Network.Cache.Data;
using Network.Events;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI {
	public class RolePick : MonoBehaviour {
		private static List<string> LoadedRoles = new List<string>();

		[SerializeField] private TMP_Dropdown[] Dropdown = new TMP_Dropdown[4];
		[SerializeField] private CanvasGroup    CanvasGroup;

		private static List<string> PlayersName = new List<string>();
		private static List<string> DirectorsName = new List<string>();


		public static void Init() {
			if (GameManager.DeviceNum != 1) return;

			EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, RoleReset);

			PlayersName.Add(TeamDataCache.ActiveTeam.Player1_Name);
			PlayersName.Add(TeamDataCache.ActiveTeam.Player2_Name);
			PlayersName.Add(TeamDataCache.ActiveTeam.Player3_Name);
			PlayersName.Add(TeamDataCache.ActiveTeam.Player4_Name);
		}


		public static void Load(Action callback) {
			if (Rounds.Round >= 1) {
				PacketSender.GetDirectors(delegate (WebResponse directorsResponse) {
					print($"Directors: {directorsResponse.Text}");
					DirectorsName = directorsResponse.Text.Split(',').ToList();
				});
			}

			PacketSender.GetRoles(delegate (WebResponse r) {
				print($"Get roles: {r.Text}");
				if (r.Text == "success") {
					callback.Invoke();
					return;
				}

				LoadedRoles = r.Text.Split(',').ToList();
				callback.Invoke();
			});
		}


		private static void RoleReset() {
			LoadedRoles.Clear();
			PlayersName.Clear();
			DirectorsName.Clear();
		}


		private static void RefreshValues() {
			if (UIManager.RolePick.Dropdown[0].options.Count == 0) {
				UIManager.RolePick.CreatePlayers();
			}

			for (var i = 0; i < UIManager.RolePick.Dropdown.Length; i++) {
				var dropdown = UIManager.RolePick.Dropdown[i];
				dropdown.value = i;
				dropdown.RefreshShownValue();
			}
		}


		public void CheckRoles() {
			if (LoadedRoles.Count == 0) {
				ShowPanel();
			}
			else {
				PacketSender.RoundReady();
			}
		}


		public void ShowPanel() {
			RefreshValues();
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
		}


		public void OnConfirmButton() {
			var values = Dropdown.ToList().Select(d => d.value).ToList();
			var director = PlayersName[values[0]];
			if (DirectorsName.Contains(director)) {
				Notification.Create("Директор уже был выбран в одном из предыдущих раундов");
				return;
			}

			if (values.GroupBy(v => v).Count() < values.Count) {
				Notification.Create("Назначьте роли разным участникам");
				return;
			}

			var players = values.Select(v => PlayersName[v]);
			PacketSender.SetRoles(players, OnRoleSet);
			DirectorsName.Add(director);
		}


		private void OnRoleSet(WebResponse response) {
			PacketSender.RoundReady();
			HidePanel();
		}


		private void CreatePlayers() {
			foreach (var dropdown in Dropdown) {
				dropdown.ClearOptions();
				PlayersName.ForEach(playerName => dropdown.options.Add(new TMP_Dropdown.OptionData(playerName)));
			}
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}