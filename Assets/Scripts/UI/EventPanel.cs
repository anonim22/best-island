﻿using System.Collections;
using System.Linq;
using Game;
using Game.Data;
using Network;
using Network.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
	[RequireComponent(typeof(CanvasGroup))]
	public class EventPanel : MonoBehaviour {
		[SerializeField] private CanvasGroup     CanvasGroup;
		[SerializeField] private AudioSource     Sound;
		[SerializeField] private TextMeshProUGUI Header;
		[SerializeField] private TextMeshProUGUI Description;


		private void Start() {
			EventsController.AddGameEventListener(OnGameEvent);
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
			Sound.Pause();
		}


		private void OnGameEvent(int eventId) {
			SetData(eventId);
			ShowPanel();
			Sound.Play();
		}


		private void SetData(int eventId) {
			var e = GameEventsData.GetEvent(eventId);

			Header.text      = e.Name;
			Description.text = e.Description;
		}


		public void SetText(string text) {
			Description.text = text;
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}