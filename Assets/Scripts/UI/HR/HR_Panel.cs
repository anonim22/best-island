﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Data;
using Network;
using Network.Events;
using TMPro;
using UI;
using UI.HR;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Random = UnityEngine.Random;

[RequireComponent(typeof(CanvasGroup))]
public class HR_Panel : MonoBehaviour {
	[SerializeField] private CanvasGroup     CanvasGroup;
	[SerializeField] private AudioSource     Sound, ConstructionSound, BackgroundSound;
	[SerializeField] private TextMeshProUGUI Employer1,     Employer2;
	[SerializeField] private string          Employer1Desc, Employer2Desc;
	[SerializeField] private Transform       CompetenceContainer;
	[SerializeField] private GameObject      CompetencePrefab;
	[SerializeField] private ReorderableList ReorderableList1, ReorderableList2;
	[SerializeField] private Button          FindBtn1,         FindBtn2;

	private List<GameObject> _list1           = new List<GameObject>();
	private List<GameObject> _list2           = new List<GameObject>();
	private List<GameObject> _competencesList = new List<GameObject>();

	private List<int> _lastList1 = new List<int>();
	private List<int> _lastList2 = new List<int>();

	private bool _secondChance1, _secondChance2;
	private bool _scoreSend;

	private int       _maxScore1, _maxScore2;
	private Employees _employees;


	private void Start() {
		EventsController.AddListener(EventsController.DefaultEvents.ConnectionLost, Reset);
		Rounds.AddRoundEndListener(delegate { HidePanel(); });
		Rounds.AddRoundStartListener(delegate {
			HidePanel();
			Reset();
		});

		PacketSender.CheckHRScore(delegate (WebResponse r) {
			print(r.Text);
			if (r.Text == "1") {
				print($"HR competences selected");
				Reset();
				HidePanel();
				_scoreSend = true;
			}
			else {
				print($"HR competences not selected");
			}
		});
	}


	public void ShowPanel() {
		if (_scoreSend) {
			Notification.Create("Компетенции уже были выбраны");
			return;
		}

		var building = Buildings.BuildingsData.FirstOrDefault(b => b.Round == Rounds.Round);
		if (building == null) {
			Notification.Create("Сначала начните стройку");
			return;
		}
		_employees = HRData.GetEmployees(building.BuildingId);

		LoadCompetences();

		StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
		CanvasGroup.blocksRaycasts    = true;
		GameInterface.InterfaceIsOpen = true;

		Sound.Play();
		ConstructionSound.volume = 0f;
		BackgroundSound.volume = 0f;
	}


	public void HidePanel() {
		StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
		CanvasGroup.blocksRaycasts    = false;
		GameInterface.InterfaceIsOpen = false;

		Sound.Pause();
		ConstructionSound.volume = 1f;
		BackgroundSound.volume   = 1f;
	}


	#region UI Callback

	public void OnAddToList(ReorderableList.ReorderableListEventStruct element) {
		/*print("Add to list " + (element.ToList == ReorderableList1 ? "1" :
				element.ToList                  == ReorderableList2 ? "2" : element.ToList.ToString()));
		print("From list " + (element.FromList == ReorderableList1 ? "1" :
				element.FromList                == ReorderableList2 ? "2" : element.FromList.ToString()));*/

		if (element.ToList == ReorderableList1) {
			AddToList(element.DroppedObject, _list1, ReorderableList1);
		}
		else if (element.ToList == ReorderableList2) {
			AddToList(element.DroppedObject, _list2, ReorderableList2);
		}
		else {
			Destroy(element.DroppedObject);
		}
	}


	public void OnRemoveFromList(ReorderableList.ReorderableListEventStruct element) {
		RemoveFromList(element, _list1, ReorderableList1);
		RemoveFromList(element, _list2, ReorderableList2);
	}


	public void OnCheckCompetence1Btn() {
		CheckCompetences(_list1, 1);
	}


	public void OnCheckCompetence2Btn() {
		CheckCompetences(_list2 , 2);
	}


	public void OnCloseBtn() {
		HidePanel();

		// Если есть список компетенций и были нажаты кнопки подбора, то отправляем результаты
		if (_lastList1 != null && _lastList2 != null && _lastList1.Count == 5 && _lastList2.Count == 5) {
			SendScore();
		}
		else {
			Notification.Create("Компетенции не выбраны");
		}
	}


	public void OnManager1Click() {
		if (!string.IsNullOrEmpty(Employer1Desc)) {
			UIManager.EventPanel.SetText(Employer1Desc);
			UIManager.EventPanel.ShowPanel();
		}
	}


	public void OnManager2Click() {
		if (!string.IsNullOrEmpty(Employer2Desc)) {
			UIManager.EventPanel.SetText(Employer2Desc);
			UIManager.EventPanel.ShowPanel();
		}
	}

	#endregion


	private void AddToList(GameObject element, List<GameObject> list, ReorderableList reorderableList) {
		var competences = list.Select(c => c.GetComponent<CompetenceCard>().Competence).ToList();
		var competence  = element.GetComponent<CompetenceCard>().Competence;

		if (competences.Contains(competence) && !list.Contains(element)) {
//			print("Destroy");
			Destroy(element);
			return;
		}

		element.GetComponent<Image>().color = Color.white;
		list.Add(element);

		if (list.Count >= 5) {
			reorderableList.IsDropable = false;
		}
	}


	private void RemoveFromList(ReorderableList.ReorderableListEventStruct element, List<GameObject> list,
								ReorderableList                            reorderableList) {
		list.Remove(element.DroppedObject);
		if (list.Count < 5) reorderableList.IsDropable = true;
	}


	private void CheckCompetences(List<GameObject> competencesList, int employeeId) {
		if (competencesList.Count < 5) {
			Notification.Create("Выберите 5 компетенций");
			return;
		}

		var competenceCardList = competencesList.Select(c => c.GetComponent<CompetenceCard>()).ToList();
		var lastCheckList      = employeeId == 1 ? _lastList1 : _lastList2;

		// Проверка на совпадение выбранных компетенций с предыдущим подбором
		if (lastCheckList != null) {
			var competenceIndexes = competenceCardList.Select(c => c.Competence);
			var compare           = competenceIndexes.OrderBy(t => t).SequenceEqual(lastCheckList.OrderBy(t => t));

			if (compare) {
				Notification.Create("Измените список компетенций для второй попытки подбора");
				return;
			}
		}

		GetHRScore(); // Чтобы записать макс. очков

		if (employeeId == 1) {
			if (_secondChance1) {
				FindBtn1.interactable        = false;
				ReorderableList1.IsDraggable = false;
			}
			else _secondChance1 = true;

			_lastList1 = new List<int>(_list1.Select(c => c.GetComponent<CompetenceCard>().Competence));
		}
		else {
			if (_secondChance2) {
				FindBtn2.interactable        = false;
				ReorderableList2.IsDraggable = false;
			}
			else _secondChance2 = true;

			_lastList2 = new List<int>(_list2.Select(c => c.GetComponent<CompetenceCard>().Competence));
		}

		var employee         = employeeId == 1 ? _employees.Employee1 : _employees.Employee2;
		var trueCompetences  = GetTrueCompetences(competenceCardList, employee);
		var falseCompetences = GetFalseCompetences(competenceCardList, employee);

		trueCompetences.ForEach(c => c.GetComponent<Image>().color  = Color.green);
		falseCompetences.ForEach(c => c.GetComponent<Image>().color = Color.red);
	}


	private List<CompetenceCard> GetTrueCompetences(List<CompetenceCard>   competenceCards,
													Employees.EmployeeData employee) {
		return (from c in competenceCards
				where employee.Competencies.Contains(c.Competence)
				select c).ToList();
	}


	private List<CompetenceCard> GetFalseCompetences(List<CompetenceCard>  competenceCards,
													Employees.EmployeeData employee) {
		return (from c in competenceCards
				where !employee.Competencies.Contains(c.Competence)
				select c).ToList();
	}


	private void LoadCompetences() {
		Employer1.text = _employees.Employee1.EmployeePosition;
		Employer2.text = _employees.Employee2.EmployeePosition;
		Employer1Desc = _employees.Employee1.EmployeeDescription;
		Employer2Desc = _employees.Employee2.EmployeeDescription;

		DestroyCompetences();
		CreateCompetences();
	}


	private void CreateCompetences() {
		var competences = GetCompetenceList();
		if (competences == null) return;

		foreach (var competenceIndex in competences) {
			var competence = Instantiate(CompetencePrefab, CompetenceContainer);
			competence.GetComponent<CompetenceCard>().Competence = competenceIndex;
			_competencesList.Add(competence);
		}
	}


	private List<int> GetCompetenceList() {
		var requiredCompetences  = _employees.Employee1.Competencies.ToList();
		var requiredCompetences2 = _employees.Employee2.Competencies;

		// ---Получение требуемых компетенций---
		// Объединение
		requiredCompetences.AddRange(requiredCompetences2);
		// Получение уникальных компетенций
		requiredCompetences = requiredCompetences.GroupBy(c => c).Select(c => c.First()).ToList();

		// Получение списка остальных компетенций
		var otherCompetences = (from c in HRData.Competencies.Keys
								where !requiredCompetences.Contains(c)
								select c).ToList();

		// Рандомно выбираем среди остальных компетенций, пока нужных и остальных не будет 15 штук
		while (requiredCompetences.Count < 15) {
			var rand = Random.Range(0, otherCompetences.Count);
			var comp = otherCompetences[rand];

			requiredCompetences.Add(comp);
			otherCompetences.RemoveAt(rand);
		}

		// Перемешивание
		requiredCompetences = ShuffleCompetences(requiredCompetences);
		return requiredCompetences;
	}


	private List<int> ShuffleCompetences(List<int> list) {
		for (var i = list.Count - 1; i >= 1; i--) {
			var j = Random.Range(0, i + 1);

			var tmp = list[j];
			list[j] = list[i];
			list[i] = tmp;
		}

		return list;
	}


	private void DestroyCompetences() {
		_competencesList.ForEach(Destroy);
		_competencesList.Clear();
	}


	private void SendScore() {
		var score = GetHRScore();
		PacketSender.SetHRScore(score, delegate (WebResponse r) {
//			print(r.Text);
			_scoreSend = true;
		});
	}


	private int GetHRScore() {
		var score1 = GetListScore(_list1, _secondChance1, _employees.Employee1);
		var score2 = GetListScore(_list2, _secondChance2, _employees.Employee2);

		score1 = Mathf.Max(score1, _maxScore1);
		score2 = Mathf.Max(score2, _maxScore2);

		_maxScore1 = score1;
		_maxScore2 = score2;

		print($"score1: {score1}, score2: {score2}");

		return score1 + score2;
	}


	private int GetListScore(List<GameObject> list, bool secondChance, Employees.EmployeeData employee) {
		var competenceCardList = list.Select(c => c.GetComponent<CompetenceCard>()).ToList();
		var trueCompetences    = GetTrueCompetences(competenceCardList, employee).Count;
		// кол-во верных компетенций минус 2 (магическое число)
		var score = trueCompetences - 2;

		// Снятие очков за вторую попытку
		if (secondChance) score -= 1;

		// Ограничение очков в рамках от 0 до 3
		return Mathf.Clamp(score, 0, 3);
	}


	private void Reset() {
		_list1.ForEach(Destroy);
		_list2.ForEach(Destroy);

		_list1.Clear();
		_list2.Clear();

		_scoreSend = false;
		_maxScore1 = _maxScore2 = 0;

		_secondChance1               = _secondChance2               = false;
		_lastList1                   = _lastList2                   = null;
		FindBtn1.interactable        = FindBtn2.interactable        = true;
		ReorderableList1.IsDraggable = ReorderableList2.IsDraggable = true;
		ReorderableList1.IsDropable  = ReorderableList2.IsDropable  = true;
	}


	private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
		float _timeStartedLerping = Time.time;
		float timeSinceStarted    = Time.time - _timeStartedLerping;
		float percentageComplete  = timeSinceStarted / lerpTime;

		while (true) {
			timeSinceStarted   = Time.time - _timeStartedLerping;
			percentageComplete = timeSinceStarted / lerpTime;

			float currentValue = Mathf.Lerp(start, end, percentageComplete);

			cg.alpha = currentValue;

			if (percentageComplete >= 1) break;

			yield return new WaitForFixedUpdate();
		}
	}
}