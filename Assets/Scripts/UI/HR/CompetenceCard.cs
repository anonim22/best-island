using Game.Data;
using TMPro;
using UnityEngine;

namespace UI.HR {
	public class CompetenceCard : MonoBehaviour {
		[HideInInspector] public int Competence;
		[SerializeField] private TextMeshProUGUI Text;


		private void Start() {
			var competenceName = HRData.Competencies[Competence];
			Text.text = competenceName;
		}
	}
}