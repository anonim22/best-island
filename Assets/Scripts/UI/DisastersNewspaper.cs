﻿using System.Collections;
using Game.Data;
using Network.Events;
using TMPro;
using UnityEngine;

namespace UI {
	[RequireComponent(typeof(CanvasGroup))]
	public class DisastersNewspaper : MonoBehaviour {
		[SerializeField] private CanvasGroup     CanvasGroup;
		[SerializeField] private AudioSource     Sound;
		[SerializeField] private TextMeshProUGUI Title;
		[SerializeField] private TextMeshProUGUI Description;


		private void Start() {
			EventsController.AddDisasterListener(OnDisaster);
		}


		private void OnDisaster(int id) {
			LoadData(id);
			ShowPanel();
			Sound.Play();
		}


		private void LoadData(int id) {
			var disaster = DisastersData.GetDisaster(id);

			Title.text       = disaster.Name;
			Description.text = disaster.Description;
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 1, .5f));
			CanvasGroup.blocksRaycasts    = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(CanvasGroup, CanvasGroup.alpha, 0, .5f));
			CanvasGroup.blocksRaycasts    = false;
			GameInterface.InterfaceIsOpen = false;
			Sound.Pause();
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}