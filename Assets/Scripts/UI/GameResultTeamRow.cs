﻿using System.Linq;
using Game;
using Game.Data;
using Network.ServerData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
	public class GameResultTeamRow : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI Place;
		[SerializeField] private TextMeshProUGUI TeamName;
		[SerializeField] private TextMeshProUGUI Revenue;
		[SerializeField] private TextMeshProUGUI GameEfficiency;
		[SerializeField] private TextMeshProUGUI EffectiveRound;
		[SerializeField] private GameObject      LightBackground;


		/*public void SetData(int place, GameResultData.Team team) {
			Place.text          = place.ToString();
			TeamName.text       = team.Name;
			Revenue.text        = Money.Format(team.Revenue);
			GameEfficiency.text = team.GameEfficiency.ToString();
			EffectiveRound.text = team.EffectiveRound.ToString();
		}*/


		public void SetLight() {
			LightBackground.SetActive(true);
		}
	}
}