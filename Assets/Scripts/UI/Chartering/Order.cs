﻿using System.Globalization;
using System.Linq;
using Game;
using Game.Data;
using Network.Cache;
using Network.Cache.Data;
using TMPro;
using UnityEngine;
using Material = UI.MaterialShop.Material;

namespace UI.Chartering {
	public class Order : MonoBehaviour {
		[SerializeField] private Transform       MaterialsContainer;
		[SerializeField] private GameObject      MaterialRowPrefab;
		[SerializeField] private TextMeshProUGUI OrderIdText;
		[SerializeField] private TextMeshProUGUI TeamText;
		[SerializeField] private TextMeshProUGUI OrderSumText;
		[SerializeField] private TextMeshProUGUI OrderWeightText;
		[SerializeField] private GameObject      CancelBtn;

		public Game.Data.Order OrderData;
		public bool            Selected;

		[HideInInspector] public CargoPanel CargoSelection;


		public void SetData(Game.Data.Order order) {
			OrderData            = order;
			OrderIdText.text     = order.OrderId.ToString();
			TeamText.text        = CacheManager.TeamDataCache.Teams.Single(t => t.ID == order.TeamId).TeamName;
			OrderSumText.text    = ((int) (order.GetSum() / 1000)).ToString(CultureInfo.InvariantCulture);
			OrderWeightText.text = order.GetWeight().ToString(CultureInfo.InvariantCulture);
			CreateMaterialRows();
		}


		public void OnPickBtn() {
			Selected = true;
			CancelBtn.SetActive(true);
			CargoSelection.UpdateWeight();
		}


		public void OnCancelBtn() {
			Selected = false;
			CancelBtn.SetActive(false);
			CargoSelection.UpdateWeight();
		}


		private void CreateMaterialRows() {
			foreach (var material in OrderData.Materials) {
				var row = Instantiate(MaterialRowPrefab, MaterialsContainer);
				row.GetComponent<Material>().SetData(material);
			}
		}
	}
}