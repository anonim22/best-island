﻿using System.Collections;
using System.Collections.Generic;
using UI.Chartering;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class TeamChoice : MonoBehaviour
{
    CanvasGroup uiElement;
    public CargoPanel charteringCargoSelection;

    void Start()
    {
        uiElement = GetComponent<CanvasGroup>();
    }

    
    public void ShowPanel()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 1, .5f));
        uiElement.blocksRaycasts = true;
        GameInterface.InterfaceIsOpen = true;
    }

    public void HidePanel()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 0, .5f));
        uiElement.blocksRaycasts = false;
        GameInterface.InterfaceIsOpen = false;
    }

    public void ReturnShipSelectionPanel()
    {
        HidePanel();
        charteringCargoSelection.ShowPanel();
    }

     public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            cg.alpha = currentValue;

            if (percentageComplete >= 1) break;

            yield return new WaitForFixedUpdate();
        }

    }
}
