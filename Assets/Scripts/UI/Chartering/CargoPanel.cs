using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Network;
using Network.Events;
using Network.ServerData;
using TMPro;
using UI.Chartering.Ships;
using UnityEngine;

namespace UI.Chartering {
	[RequireComponent(typeof(CanvasGroup))]
	public class CargoPanel : MonoBehaviour {
		[SerializeField] private CanvasGroup     UIElement;
		[SerializeField] private GameObject      OrderPrefab;
		[SerializeField] private Transform       OrdersParent;
		[SerializeField] private TextMeshProUGUI CargoWeight;
		[SerializeField] private TextMeshProUGUI ShipCapacity;
		[SerializeField] private GameObject      NoPayedOrders;

		private List<Order> _orders = new List<Order>();


		private void Start() {
			Game.Data.Order.OnUpdateOrderEvent += CharteringUpdate;
			EventsController.AddCharteringUpdateListener(delegate { CharteringUpdate(); });
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(UIElement, UIElement.alpha, 1, .5f));
			UIElement.blocksRaycasts      = true;
			GameInterface.InterfaceIsOpen = true;
			DestroyOrdersInstance();
			LoadOrders();
			UpdateWeight();
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(UIElement, UIElement.alpha, 0, .5f));
			UIElement.blocksRaycasts      = false;
			GameInterface.InterfaceIsOpen = false;
		}


		public void OnSendButton() {
			var selectedOrders = _orders.Where(o => o.Selected).Select(o => o.OrderData).ToList();
			if (selectedOrders.Count == 0) {
				Notification.Create("Выберите заказы");
				return;
			}

			var orderWeight = selectedOrders.Sum(o => o.GetWeight());
			if (orderWeight > ShipsPanel.SelectedShip.Capacity) {
				Notification.Create("Превышен вес груза");
				return;
			}

			var selectedOrderIds = selectedOrders.Select(o => o.Id.ToString());
			PacketSender.SendShip(ShipsPanel.SelectedShip.Id, selectedOrderIds);

			HidePanel();
		}


		public void OnBackBtn() {
			HidePanel();
			UIManager.ShipsPanel.ShowPanel();
		}


		public void OnSendInviteBtn() {
			HidePanel();
			UIManager.SendInvitePanel.ShowPanel();
		}


		public void UpdateWeight() {
			if (ShipsPanel.SelectedShip == null) return;

			CargoWeight.text = _orders.Where(o => o.Selected).Sum(o => o.OrderData.GetWeight())
									.ToString(CultureInfo.InvariantCulture);
			ShipCapacity.text = ShipsPanel.SelectedShip.Capacity.ToString();
		}


		private void CharteringUpdate() {
			DestroyOrdersInstance();
			LoadOrders();
			UpdateWeight();
		}


		private void LoadOrders() {
			NoPayedOrders.SetActive(false);
			var orders = Game.Data.Order.Orders.Where(o => o.Payed).ToList();
			if (!orders.Any()) {
				NoPayedOrders.SetActive(true);
				return;
			}

			foreach (var order in orders) {
				var instance =  Instantiate(OrderPrefab, OrdersParent).GetComponent<Order>();
				instance.SetData(order);
				instance.CargoSelection = this;
				_orders.Add(instance);
			}
		}


		private void DestroyOrdersInstance() {
			_orders.ForEach(x => Destroy(x.gameObject));
			_orders.Clear();
		}


		public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float _timeStartedLerping = Time.time;
			float timeSinceStarted    = Time.time - _timeStartedLerping;
			float percentageComplete  = timeSinceStarted / lerpTime;

			while (true) {
				timeSinceStarted   = Time.time - _timeStartedLerping;
				percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}