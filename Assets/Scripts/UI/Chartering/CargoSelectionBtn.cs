﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Chartering {
    public class CargoSelectionBtn : MonoBehaviour
    {

        private Toggle toggle;
        private Image  image;
        public  Color  color;
        [HideInInspector]
        public bool selected;
        private Transform       selectBtn;
        private GameObject      chartering;
        public  bool            rent;
        public  TextMeshProUGUI Number;
        public  TextMeshProUGUI Weight;

        void Start()
        {
            toggle     = gameObject.GetComponent<Toggle>();
            image      = gameObject.GetComponent<Image>();
            selectBtn  = gameObject.transform.Find("Select");
            chartering = GameObject.Find("Chartering (Joint delivery (Cargo selection))");
            toggle.onValueChanged.AddListener(OnToggleValueChanged);
        }

        void Update()
        {
            if(rent)
            {
                selectBtn.gameObject.SetActive(false);
            }else
            {
                selectBtn.gameObject.SetActive(true);
            }
        }

        public void OnSelectOrderBtn()
        {
            /*if (!chartering.GetComponent<CharteringCargoSelection>()._selectedOrders.Contains(gameObject))
            {
                chartering.GetComponent<CharteringCargoSelection>()._selectedOrders.Add(this.gameObject);
            }
            else
            {
                chartering.GetComponent<CharteringCargoSelection>()._selectedOrders.Remove(this.gameObject);
            }*/

            if(rent)
            {
                rent = false;
            } else
            {
                rent = true;
            }
        }

        public void SelectUnSlect()
        {
            if(rent)
            {
                rent = false;
            } else
            {
                rent = true;
            }
        }

        public void SetText(int n, int w)
        {
            Number.text = n.ToString();
            Weight.text = w.ToString();
        }

        private void OnToggleValueChanged(bool isOn)
        {
            if (isOn)
            {
                image.color = color;
                selected    = true;
            }
            else
            {
                image.color = Color.white;
                selected    = false;
            }
        }
    }
}
