using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Network.Cache;
using Network.Cache.Data;
using Network.Events;
using Network.ServerData;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

namespace UI.Chartering.Invitations {
	public class InvitiationsPanel : MonoBehaviour {
		[SerializeField] private CanvasGroup UIElement;
		[SerializeField] private Transform   TeamContainer;
		[SerializeField] private GameObject  TeamPrefab;

		private List<InvitingTeam> _teams = new List<InvitingTeam>();


		private void Start() {
			EventsController.AddCharteringUpdateListener(chartering => UpdateCards());
		}


		public void ShowPanel() {
			UpdateCards();

			StartCoroutine(FadeCanvasGroup(UIElement, UIElement.alpha, 1, .5f));
			UIElement.blocksRaycasts      = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(UIElement, UIElement.alpha, 0, .5f));
			UIElement.blocksRaycasts      = false;
			GameInterface.InterfaceIsOpen = false;
		}


		public void OnBackBtn() {
			HidePanel();
			UIManager.ShipsPanel.ShowPanel();
		}


		public void OnCloseBtn() {
			HidePanel();
		}


		public void UpdateCards() {
			print("Update cards");
			DestroyCards();
			LoadInvites();
		}


		private void LoadInvites() {
			var invites = Game.Data.Chartering.CharteringData.Where(c => c.Recipient == TeamDataCache.ActiveTeamId);
			invites.ToList().ForEach(CreateTeamCard);
		}


		private void CreateTeamCard(CharteringData data) {
			var team    = CacheManager.TeamDataCache.Teams.Single(t => t.ID == data.SenderId);
			var tObject = Instantiate(TeamPrefab, TeamContainer).GetComponent<InvitingTeam>();
			tObject.SetTeam(team);
			tObject.SetData(data);
			_teams.Add(tObject);
		}


		private void DestroyCards() {
			_teams.ForEach(t => Destroy(t.gameObject));
			_teams.Clear();
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float startTime = Time.time;

			while (true) {
				var timeSinceStarted   = Time.time - startTime;
				var percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}