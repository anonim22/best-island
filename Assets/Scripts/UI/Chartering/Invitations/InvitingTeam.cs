﻿using System.Collections.Generic;
using Network;
using Network.ServerData;
using TMPro;
using UI.Chartering.Ships;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Chartering.Invitations {
	[RequireComponent(typeof(CanvasGroup))]
	public class InvitingTeam : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI TeamNameText;
		[SerializeField] private TextMeshProUGUI StatusText;
		[SerializeField] private Button          ConfirmButton, RejectButton;

		private TeamData       _team;
		private CharteringData _data;
		private string         _status;


		public void OnConfirmBtn() {
			PacketSender.CharteringConfirm(_team.ID);
		}


		public void OnRejectBtn() {
			PacketSender.CharteringReject(_team.ID);
		}


		public void SetTeam(TeamData teamData) {
			_team             = teamData;
			TeamNameText.text = teamData.TeamName;
		}


		public void SetData(CharteringData data) {
			_data   = data;
			_status = data.Status;

			UpdateStatus();
			UpdateButtons();
		}


		private void UpdateStatus() {
			switch (_status) {
				case "":
					StatusText.text = "";
					break;
				case "send":
					StatusText.text = "Приглашение";
					break;
				case "confirmed":
					StatusText.text = "Приглашение принято";
					break;
				case "rejected":
					StatusText.text = "Приглашение отклонено";
					break;
			}
		}


		private void UpdateButtons() {
			ConfirmButton.interactable = _status == "send" ||  _status == "rejected";
			RejectButton.interactable  = _status != "rejected";
		}
	}
}