using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Network.Cache;
using Network.Cache.Data;
using Network.Events;
using Network.ServerData;
using UnityEngine;

namespace UI.Chartering.Invitations {
	public class SendInvitePanel : MonoBehaviour {
		[SerializeField] private CanvasGroup UIElement;
		[SerializeField] private Transform   TeamContainer;
		[SerializeField] private GameObject  TeamPrefab;

		private List<InviteTeam> _teams = new List<InviteTeam>();


		private void Start() {
			EventsController.AddCharteringUpdateListener(chartering => UpdateStatuses());
			CreateTeams();
			UpdateStatuses();
		}


		public void ShowPanel() {
			/*if (Game.Chartering.RentedShip != null) {
				Game.Island.UI.UIManager.CargoPanel.ShowPanel();
				return;
			}*/

			StartCoroutine(FadeCanvasGroup(UIElement, UIElement.alpha, 1, .5f));
			UIElement.blocksRaycasts      = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(UIElement, UIElement.alpha, 0, .5f));
			UIElement.blocksRaycasts      = false;
			GameInterface.InterfaceIsOpen = false;
		}


		public void OnBackBtn() {
			HidePanel();
			UIManager.MaterialShopOrders.ShowPanel();
		}


		public void OnCloseBtn() {
			HidePanel();
		}


		private void UpdateStatuses() {
			_teams.ForEach(t => t.SetStatus(""));
			foreach (var data in Game.Data.Chartering.CharteringData) {
				if (data.Recipient == TeamDataCache.ActiveTeamId) continue;

				_teams.Single(t => t.Team.ID == data.Recipient).SetStatus(data.Status);
			}
		}


		private void CreateTeams() {
			var otherTeams = CacheManager.TeamDataCache.Teams.Where(t => t.ID != TeamDataCache.ActiveTeamId);
			otherTeams.ToList().ForEach(CreateTeamCard);
		}


		private void CreateTeamCard(TeamData team) {
			var t = Instantiate(TeamPrefab, TeamContainer).GetComponent<InviteTeam>();
			t.SetTeam(team);
			t.InviteTeams = _teams;
			_teams.Add(t);
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float startTime = Time.time;

			while (true) {
				var timeSinceStarted   = Time.time - startTime;
				var percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}