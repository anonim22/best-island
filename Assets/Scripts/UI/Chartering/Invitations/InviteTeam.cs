﻿using System.Collections.Generic;
using Network;
using Network.ServerData;
using TMPro;
using UI.Chartering.Ships;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Chartering.Invitations {
	[RequireComponent(typeof(CanvasGroup))]
	public class InviteTeam : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI TeamNameText;
		[SerializeField] private TextMeshProUGUI StatusText;
		[SerializeField] private GameObject      SendButton, CancelButton;

		[HideInInspector] public List<InviteTeam> InviteTeams;

		public TeamData Team;
		private string _status;


		public void OnSendBtn() {
			PacketSender.CharteringInvite(Team.ID, ShipsPanel.SelectedShip.Id);
		}


		public void OnCancelBtn() {
			PacketSender.CharteringCancel(Team.ID);
		}


		public void SetTeam(TeamData teamData) {
			Team = teamData;
			TeamNameText.text = teamData.TeamName;
		}


		public void SetStatus(string status) {
			_status = status;
			UpdateStatus();
			UpdateButtons();
		}


		private void UpdateStatus() {
			switch (_status) {
				case "":
					StatusText.text = "";
					break;
				case "send":
					StatusText.text = "Приглашение отправлено";
					break;
				case "confirmed":
					StatusText.text = "Приглашение принято";
					break;
				case "rejected":
					StatusText.text = "Приглашение отклонено";
					break;
			}
		}


		private void UpdateButtons() {
			switch (_status) {
				case "send":
				case "confirmed":
					SendButton.SetActive(false);
					CancelButton.SetActive(true);
					break;
				default:
					SendButton.SetActive(true);
					CancelButton.SetActive(false);
					break;
			}
		}
	}
}