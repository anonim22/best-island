﻿using Game;
using Game.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Chartering.Ships {
	public class Ship : MonoBehaviour {
		[SerializeField] private ShipsPanel Chartering;
		[SerializeField] private Image      SelectImage;
		public                   int        ShipId;
		[SerializeField] private Image      Icon;

		[SerializeField] private TextMeshProUGUI Name;
		[SerializeField] private TextMeshProUGUI Capacity;
		[SerializeField] private TextMeshProUGUI RentCost;

		[SerializeField] private Toggle Toggle;
		[SerializeField] private Color  SelectedColor;

		[HideInInspector] public bool Selected;


		void Start() {
			LoadShipInfo();
			Toggle.onValueChanged.AddListener(OnToggleValueChanged);
		}


		private void OnToggleValueChanged(bool value) {
			ColorBlock cb = Toggle.colors;
			if (value) {
				SelectImage.color = SelectedColor;
				Selected          = true;
				Chartering.SelectShipButton(this);
			}
			else {
				SelectImage.color = Color.white;
				Selected          = false;
				Chartering.SelectShipButton(null);
			}

			Toggle.colors = cb;
		}


		public void TurnOff() {
			Toggle.isOn = false;
		}


		private void LoadShipInfo() {
			var ship = ShipsData.GetShip(ShipId);
			Icon.sprite   = ship.Icon;
			Name.text     = ship.Name;
			Capacity.text = ship.Capacity.ToString();
			RentCost.text = Money.Format(ship.RentCost);
		}
	}
}