﻿using System.Collections;
using System.Collections.Generic;
using Game.Data;
using UnityEngine;

namespace UI.Chartering.Ships {
	public class ShipsPanel : MonoBehaviour {
		public static Game.Data.Ship SelectedShip;

		[SerializeField] private CanvasGroup UIElement;
		[SerializeField] private GameObject  SendShipBtn;

		private Ship _activeShip;


		public void SelectShipButton(Ship button) {
			if(_activeShip && _activeShip != button) _activeShip.TurnOff();

			SendShipBtn.SetActive(button != null && button.Selected);
			_activeShip = button;
		}


		public void ShowPanel() {
			StartCoroutine(FadeCanvasGroup(UIElement, UIElement.alpha, 1, .5f));
			UIElement.blocksRaycasts = true;
			GameInterface.InterfaceIsOpen = true;
		}


		public void HidePanel() {
			StartCoroutine(FadeCanvasGroup(UIElement, UIElement.alpha, 0, .5f));
			UIElement.blocksRaycasts = false;
			GameInterface.InterfaceIsOpen = false;
			SelectShipButton(null);
		}


		public void SendShip() {
			if(!_activeShip) return;
			SelectedShip = ShipsData.GetShip(_activeShip.ShipId);

			HidePanel();
			UIManager.CargoPanel.ShowPanel();
		}


		public void OnInvitationsBtn() {
			HidePanel();
			UIManager.InvitiationsPanel.ShowPanel();
		}


		private IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1) {
			float startTime = Time.time;

			while (true) {
				var timeSinceStarted   = Time.time - startTime;
				var percentageComplete = timeSinceStarted / lerpTime;

				float currentValue = Mathf.Lerp(start, end, percentageComplete);

				cg.alpha = currentValue;

				if (percentageComplete >= 1) break;

				yield return new WaitForFixedUpdate();
			}
		}
	}
}