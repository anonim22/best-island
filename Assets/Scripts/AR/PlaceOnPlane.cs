using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UX;

/// <summary>
/// Listens for touch events and performs an AR raycast from the screen touch point.
/// AR raycasts will only hit detected trackables like feature points and planes.
///
/// If a raycast hits a trackable, the <see cref="placedPrefab"/> is instantiated
/// and moved to the hit position.
/// </summary>
[RequireComponent(typeof(ARRaycastManager))]
public class PlaceOnPlane : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
    GameObject m_PlacedPrefab;

    public UIManager uiManager;
    ARTrackedImageManager trackedImageManager;
    public Vector3 imageTransform;
    MakeAppearOnPlane makeAppearOnPlane;
    public bool once = true;

    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject placedPrefab
    {
        get { return m_PlacedPrefab; }
        set { m_PlacedPrefab = value; }
    }

    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public GameObject spawnedObject { get; private set; }

    void Awake()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
        trackedImageManager = FindObjectOfType<ARTrackedImageManager>();
        makeAppearOnPlane = GetComponent<MakeAppearOnPlane>();
    }

    void OnEnable()
    {
        trackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }
 
    void OnDisable()
    {
        trackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            var mousePosition = Input.mousePosition;
            touchPosition = new Vector2(mousePosition.x, mousePosition.y);
            return true;
        }
#else
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }
#endif

        touchPosition = default;
        return false;
    }

    void UpdateTrackedImage(ARTrackedImage trackedImage)
    {
        
        //Make sure your desired prefab is the m_ReferencePointPrefab on the ARReferencePointManager in the Editor
        //referencePointManager.AddReferencePoint(new Pose(trackedImage.transform.position, trackedImage.transform.rotation));
        imageTransform = new Vector3(trackedImage.transform.position.x,trackedImage.transform.position.y,trackedImage.transform.position.z);
        Debug.Log(imageTransform);
        trackedImageManager.enabled = false;
        uiManager.FadeImageFound();
        //Destroy(this);
        
    }
 
    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var trackedImage in eventArgs.added)
            UpdateTrackedImage(trackedImage);
 
        foreach (var trackedImage in eventArgs.updated)
            UpdateTrackedImage(trackedImage);
    }


    void Update()
    {
        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;

        if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            var hitPose = s_Hits[0].pose;

            if (once)
            {
                //spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation);
                //spawnedObject = Instantiate(m_PlacedPrefab, new Vector3(imageTransform.x,hitPose.position.y,imageTransform.z), hitPose.rotation);
                m_PlacedPrefab.SetActive(true);
                m_PlacedPrefab.transform.position = new Vector3(imageTransform.x, hitPose.position.y, imageTransform.z);
                uiManager.FadeAnim();
                //makeAppearOnPlane.content = spawnedObject.transform;
                once = false;
            }
            else
            {
                //spawnedObject.transform.position = hitPose.position;
            }
        }
    }

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARRaycastManager m_RaycastManager;
}
