using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UX;

public class AutoPlaceItem : MonoBehaviour
{
     public GameObject GameObjectToPlace;
    public UIManager uiManager;
    ARTrackedImageManager trackedImageManager;
    public Vector3 imageTransform;
    bool imageDetected = false;
    bool once = true;

    ARSessionOrigin m_SessionOrigin;

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARRaycastManager m_RaycastManager;
    ARPlaneManager m_PlaneManager;

    void Awake()
    {
        m_SessionOrigin = GetComponent<ARSessionOrigin>();
        m_RaycastManager = GetComponent<ARRaycastManager>();
        m_PlaneManager = GetComponent<ARPlaneManager>();
        trackedImageManager = FindObjectOfType<ARTrackedImageManager>();
    }

    void OnEnable()
    {
        trackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }

    void OnDisable()
    {
        trackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }

    void UpdateTrackedImage(ARTrackedImage trackedImage)
    {
        imageTransform = new Vector3(trackedImage.transform.position.x, trackedImage.transform.position.y, trackedImage.transform.position.z);
        Debug.Log(imageTransform);
        trackedImageManager.enabled = false;
        uiManager.FadeImageFound();
        imageDetected = true;
    }

    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var trackedImage in eventArgs.added)
            UpdateTrackedImage(trackedImage);

        foreach (var trackedImage in eventArgs.updated)
            UpdateTrackedImage(trackedImage);
    }

    void ActivateFindImage() 
    {
        trackedImageManager.enabled = true;
        imageDetected = false;
        once = true;
        m_PlaneManager.enabled = true;
    }

    void Update()
    {
        if (m_RaycastManager.Raycast(Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)), s_Hits, TrackableType.PlaneWithinPolygon) && imageDetected)
        {
            Pose hitPose = s_Hits[0].pose;
            if (once)
            {
                GameObjectToPlace.transform.position = new Vector3(imageTransform.x, hitPose.position.y, imageTransform.z);
                uiManager.FadeImageFound();
                GameObjectToPlace.SetActive(true);
                m_PlaneManager.enabled = false;
                once = false;
                Invoke("ActivateFindImage", 120f);
            }
        }
    }
}
