using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
 
public class StickToImage : MonoBehaviour
{
    public int delayCount = 200;
 
    ARTrackedImageManager trackedImageManager;
    ARReferencePointManager referencePointManager;
 
    int count = 0;
 
    void Awake()
    {
        trackedImageManager = FindObjectOfType<ARTrackedImageManager>();
        referencePointManager = FindObjectOfType<ARReferencePointManager>();
    }
 
    void OnEnable()
    {
        trackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }
 
    void OnDisable()
    {
        trackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }
 
    void UpdateTrackedImage(ARTrackedImage trackedImage)
    {
        count++;
 
        if (count >= delayCount)
        {
            //Make sure your desired prefab is the m_ReferencePointPrefab on the ARReferencePointManager in the Editor
            referencePointManager.AddReferencePoint(new Pose(trackedImage.transform.position, trackedImage.transform.rotation));
            Debug.Log("Placed Object at " + Time.time);
            trackedImageManager.enabled = false;
            Destroy(this);
        }
    }
 
    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var trackedImage in eventArgs.added)
            UpdateTrackedImage(trackedImage);
 
        foreach (var trackedImage in eventArgs.updated)
            UpdateTrackedImage(trackedImage);
    }
}