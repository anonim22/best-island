using System;
using Network;
using Network.Cache;
using Network.ServerData;
using Newtonsoft.Json;

namespace MainMenu {
	public partial class MainMenuController {
		/// <summary>
		/// Вызывается при успешной авторизации
		/// </summary>
		private void OnAuth(WebResponse response) {
			ConnectStatus.text = "Соединение установлено";

			var authData = JsonConvert.DeserializeObject<Auth>(response.Text);
			if (authData == null || string.IsNullOrEmpty(authData.Token)) {
				OnAuthError(new WebResponse(response.Text, response.Data, "Token not received", response.Code));
				return;
			}

			Session.StartSession(authData.Token);
			LoadGames();
			print($"Connected. Token: {authData.Token}");
		}


		/// <summary>
		/// Вызывается при ошибке авторизации
		/// </summary>
		private void OnAuthError(WebResponse response) {
			print($"Auth error: {response.Error}, {response.Code}");
			PlayBtn.interactable = true;
			ServerIP_Field.interactable = true;
			ConnectStatus.text   = $"Ошибка подключения{Environment.NewLine} {response.Error}:{response.Code}";
		}
	}
}