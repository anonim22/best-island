﻿using Network;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu {
	public partial class MainMenuController : MonoBehaviour {
		public InputField ServerIP_Field;

		public Text   ConnectStatus;
		public Button PlayBtn;

		public GameList GameList;


		/// <summary>
		/// Нажатие на кнопку Play
		/// </summary>
		public void OnPlayBtn() {
			Connect();
		}


		/// <summary>
		/// Подключение к серверу
		/// </summary>
		private void Connect() {
			PlayBtn.interactable = false;
			ServerIP_Field.interactable = false;

			ConnectStatus.text = "Подключение...";
			print($"Connecting to {NetworkManager.URI/*ServerIP.text*/}...");

			PacketSender.Auth(OnAuth, OnAuthError);
		}


		private void LoadGames() {
//			ConnectStatus.text = "Загрузка списка игр...";
			ConnectStatus.text = "";

			PlayBtn.interactable = true;
			GameList.OpenGamesPanel();
		}
	}
}