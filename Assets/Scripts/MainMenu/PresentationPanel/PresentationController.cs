using System.Collections;
using System.Collections.Generic;
using Network;
using Network.Cache;
using Network.ServerData;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using NetworkManager = Network.NetworkManager;

namespace MainMenu.PresentationPanel {
	public partial class PresentationController : MonoBehaviour {
		[SerializeField] private GameObject       PresentationPanel;
		[SerializeField] private Transform        SlidesParent;
		[SerializeField] private List<Sprite>     SlideImages;
		private                  List<GameObject> _slides = new List<GameObject>();
		private                  int              _slideIndex;

		private List<string> _slidesURL = new List<string> {
			"public/img/prezrnt_img_0.jpg",
			"public/img/prezrnt_img_1.jpg",
			"public/img/prezrnt_img_2.jpg",
			"public/img/prezrnt_img_3.jpg",
		};

		public delegate void OnPresentationLoaded();
		public delegate void OnPresentationWatched();

		private event OnPresentationLoaded  OnPresentationLoadedEvent;
		private event OnPresentationWatched OnPresentationWatchedEvent;

		public bool IsLoaded { get; private set; }


		public void LoadPresentation() {
			Debug.Log("Loading presentation");
			StartCoroutine(LoadSlides());
		}


		private IEnumerator LoadSlides() {
			foreach (var slideURL in _slidesURL) {
				var request = UnityWebRequest.Get(NetworkManager.URI + slideURL);
				yield return request.SendWebRequest();

				if(request.downloadedBytes < 1024) continue;

				Texture2D txt = new Texture2D(2, 2);
				txt.LoadImage(request.downloadHandler.data, false);

				var sprite = Sprite.Create(txt, new Rect(0, 0 , txt.width, txt.height),
											new Vector2(0.5f, 0.5f));
				SlideImages.Add(sprite);
			}

			OnSlidesLoaded();
		}


		private void OnSlidesLoaded() {
			IsLoaded = true;

			InitSlides();
			PresentationLoaded();
		}


		#region Show/Hide panels

		public void ShowPresentation() {
			PresentationPanel.SetActive(true);
			_slideIndex = 0;
			ChangeSlideTo(0);
		}


		public void HidePresentation() {
			PresentationPanel.SetActive(false);
		}

		#endregion


		#region Add listeners

		public void AddOnLoadListener(OnPresentationLoaded callback) {
			if (IsLoaded) {
				callback.Invoke();
				return;
			}

			OnPresentationLoadedEvent += callback;
		}


		public void AddOnWatchedListener(OnPresentationWatched callback) {
			OnPresentationWatchedEvent += callback;
		}

		#endregion


		#region Button listeners

		public void OnNextSlideBtn() {
			if (_slideIndex < _slides.Count - 1) {
				ChangeSlideTo(_slideIndex + 1);
				return;
			}

			PresentationWatched();
		}


		public void OnPreviousSlideBtn() {
			if (_slideIndex <= 0) return;
			ChangeSlideTo(_slideIndex - 1);
		}

		#endregion


		#region Events

		private void PresentationLoaded() {
			Debug.Log("Presentation loaded");
			OnPresentationLoadedEvent?.Invoke();
			OnPresentationLoadedEvent = null;
		}


		private void PresentationWatched() {
			Debug.Log("Presentation watched");
			OnPresentationWatchedEvent?.Invoke();
			OnPresentationWatchedEvent = null;
		}

		#endregion


		#region Slides

		private void InitSlides() {
			for (int index = 0; index < SlideImages.Count; index++) {
				var img   = SlideImages[index];
				var slide = CreateSlide(img, index);
				_slides.Add(slide);
			}
		}


		private GameObject CreateSlide(Sprite image, int index) {
			GameObject slide = new GameObject("Slide " + index);
			slide.transform.parent = SlidesParent;
			slide.transform.localScale = Vector3.one;
			slide.SetActive(false);

			RectTransform rt = slide.AddComponent<RectTransform>();
			rt.anchorMin = Vector2.zero;
			rt.anchorMax = Vector2.one;
			rt.offsetMin = Vector2.zero;
			rt.offsetMax = Vector2.zero;

			Image img = slide.AddComponent<Image>();
			img.sprite = image;
			img.preserveAspect = true;

			return slide;
		}


		private void ChangeSlideTo(int newSlideIndex) {
			_slides[_slideIndex].SetActive(false);
			_slides[newSlideIndex].SetActive(true);
			_slideIndex = newSlideIndex;
		}

		#endregion
	}
}