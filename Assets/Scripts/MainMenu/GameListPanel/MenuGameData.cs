using System;
using System.Collections.Generic;
using Network;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;

namespace MainMenu {
	public struct MenuGameData {
		public const int PlayersInTeam = 4;
		public const int PlayersPerDevice = 2;

		public int    ID;
		public string Name;
		public bool   IsStarted;
		public int    ActivePlayers;
		public int    MaxPlayers;
		public int    TeamsCount;


		public MenuGameData(int id, string name, bool status, int activePlayers, int maxPlayers, int teamCount) {
			ID            = id;
			Name          = name;
			IsStarted     = status;
			ActivePlayers = activePlayers;
			MaxPlayers    = maxPlayers;
			TeamsCount    = teamCount;
		}


		public static MenuGameData[] ParseGamesData(WebResponse response) {
			Debug.Log(response.Text);
			var gameList = JsonConvert.DeserializeObject<List<MenuGamesData>>(response.Text);
			if (gameList == null || gameList.Count == 0) return null;

			MenuGameData[] gamesData = new MenuGameData[gameList.Count];

			for (int i = 0; i < gameList.Count; i++) {
				var game = gameList[i];

				int    gameId            = game.ID;
				string gameName          = game.Name;
				bool   gameIsStarted     = Convert.ToBoolean(Convert.ToInt16(game.GameIsStarted));
				int    gameActivePlayers = game.PlayersCount * PlayersPerDevice;
				int    gameTeamsCount    = game.TeamsCount;
				int    gameMaxPlayers    = gameTeamsCount * PlayersInTeam;

				var gameData = new MenuGameData(gameId, gameName, gameIsStarted, gameActivePlayers, gameMaxPlayers, gameTeamsCount);

				gamesData[i] = gameData;
			}

			return gamesData;
		}
	}
}