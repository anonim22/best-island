using System.Collections.Generic;
using Network;
using Network.Cache;
using Network.Cache.Data;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu {
	public partial class GameList : MonoBehaviour {
		[SerializeField] private GameObject      GamesPanel;
		[SerializeField] private Text            GamesListTextHint;
		[SerializeField] private GameObject      GameRowPrefab;
		[SerializeField] private Transform       GameRowsParent;
		[SerializeField] private GameLoader 	 GameLoader;

		private readonly List<GameObject> _gameRows = new List<GameObject>();

		public delegate void OnConnectBtnEvent(int gameId);


		public void OpenGamesPanel() {
			GamesPanel.SetActive(true);
			GamesListTextHint.gameObject.SetActive(true);
			GamesListTextHint.text = "Загрузка активных игр";

			LoadGames();
		}


		public void CloseGamesPanel() {
			DestroyRows();
			GamesListTextHint.gameObject.SetActive(false);
			GamesPanel.SetActive(false);
		}


		public void OnBtnRefreshGames() {
			DestroyRows();

			GamesListTextHint.gameObject.SetActive(true);
			GamesListTextHint.text = "Обновление списка игр";

			LoadGames();
		}


		public void OnBtnCloseGamesList() {
			CloseGamesPanel();
		}


		private void LoadGames() {
			print($"Loading games");
			PacketSender.GetGames(OnLoadGames, OnLoadGamesError);
		}


		private void OnConnectToGameBtn(int gameId) {
			print($"Connect to game {gameId}");
			CacheManager.GameDataCache.GameId = gameId;
			PacketSender.SetGame(gameId, OnConnectToGame, OnConnectToGameError);
		}


		/// <summary>
		/// Создание списка игр
		/// </summary>
		/// <param name="gamesData">Данные о загруженных играх</param>
		private void CreateGameRows(MenuGameData[] gamesData) {
			if (gamesData == null || gamesData.Length == 0) {
				GamesListTextHint.gameObject.SetActive(true);
				GamesListTextHint.text = "В данный момент нет запущенных игр";
				return;
			}

			foreach (var game in gamesData) {
				CreateGameRow(game);
			}
		}


		/// <summary>
		/// Создание префаба игры в списке игр
		/// </summary>
		/// <param name="data">Данные об игре</param>
		/// <param name="onConnectBtn">Событие при нажатии на кнопку подключения</param>
		private void CreateGameRow(MenuGameData data) {
			var rowGo = Instantiate(GameRowPrefab, GameRowsParent);

			GameListRow row = rowGo.GetComponent<GameListRow>();
			row.SetGameName(data.Name);
			row.SetGameStatus(data.IsStarted);
			row.SetPlayersCount(data.ActivePlayers, data.MaxPlayers);
			row.AddGameConnectAction(OnConnectToGameBtn, data.ID);
			// TODO: Раскомментировать после тестов
			if(data.ActivePlayers >= data.MaxPlayers) row.BlockConnectButton();

			_gameRows.Add(rowGo);
		}


		/// <summary>
		/// Уничтожение всех префабов для списка игр
		/// </summary>
		private void DestroyRows() {
			foreach (var row in _gameRows) {
				Destroy(row);
			}
			_gameRows.Clear();
		}
	}
}