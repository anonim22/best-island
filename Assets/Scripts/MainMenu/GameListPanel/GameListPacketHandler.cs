using Network;
using Network.Cache;
using Network.Cache.Data;
using Network.ServerData;
using UnityEngine;

namespace MainMenu {
	public partial class GameList {
		/// <summary>
		/// Успешное получение списка игр
		/// </summary>
		private void OnLoadGames(WebResponse response) {
			var gamesData = MenuGameData.ParseGamesData(response);
			GamesListTextHint.gameObject.SetActive(false);
			CreateGameRows(gamesData);
		}


		/// <summary>
		/// Ошибка при загрузке игр
		/// </summary>
		private void OnLoadGamesError(WebResponse response) {
			GamesListTextHint.gameObject.SetActive(true);
			string error = response.Error;
			GamesListTextHint.text = $"Ошибка получения списка игр: {response.Error}:{response.Code}";
		}


		/// <summary>
		/// Успешное подключение к игре
		/// </summary>
		private void OnConnectToGame(WebResponse response) {
			Debug.Log("Connected to game: " + response.Text);
			CacheManager.GameDataCache.LoadTemplates(response);
			GameLoader.OnConnectedToGame();
			CloseGamesPanel();
		}


		/// <summary>
		/// Ошибка при подключении к игре
		/// </summary>
		private void OnConnectToGameError(WebResponse response) {
			GamesListTextHint.gameObject.SetActive(true);

			string error     = response.Error;
			long   errorCode = response.Code;

			CacheManager.GameDataCache.GameId = 0;
			GamesListTextHint.text = $"Ошибка при подключение к игре: {error}:{errorCode}";

			DestroyRows();
		}
	}
}