﻿using System;
using MainMenu;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameListRow : MonoBehaviour {
	[SerializeField] private Text   GameNameText;
	[SerializeField] private Text   GameStatusText;
	[SerializeField] private Text   PlayersCountText;
	[SerializeField] private Button ConnectToGameButton;


	public void SetGameName(string name) {
		GameNameText.text = name;
	}


	public void SetGameStatus(bool gameIsStarted) {
		string stat = gameIsStarted ? "Игра запущена" : "Игра не запущена";
		GameStatusText.text = stat;
	}


	public void SetPlayersCount(int activePlayers, int maxPlayers) {
		PlayersCountText.text = $"{activePlayers} из {maxPlayers} игроков";
	}


	public void AddGameConnectAction(GameList.OnConnectBtnEvent onConnect, int gameID) {
		ConnectToGameButton.onClick.AddListener(() => onConnect(gameID));
	}


	public void BlockConnectButton() {
		ConnectToGameButton.interactable = false;
	}
}