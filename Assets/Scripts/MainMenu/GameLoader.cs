using Game;
using Game.Data;
using Game.Island;
using MainMenu.TeamPickPanel;
using Network;
using Network.Cache;
using Network.Events;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using EventsController = Network.Events.EventsController;
using PresentationController = MainMenu.PresentationPanel.PresentationController;

namespace MainMenu {
	public class GameLoader : MonoBehaviour {
		// Если True, то есть возможность играть без ожидания других участников
		public const bool SinglePlayerMode = true;

		[SerializeField] private PresentationController Presentation;
		[SerializeField] private TeamPick               TeamPick;
		[SerializeField] private GameObject             LoaderScreen;
		[SerializeField] private Text                   LoaderText;

		[Header("Loader text")] [SerializeField]
		private string LoadText = "Загрузка";
		[SerializeField] private string WaitingText             = "Ожидание игроков";
		[SerializeField] private string PresentationLoadingText = "Загрузка вводной презентации";


		/// <summary>
		/// Отображение загрузочного экрана
		/// </summary>
		public void ShowLoader() {
			LoaderScreen.SetActive(true);
		}


		/// <summary>
		/// Скрытие загрузочного экрана
		/// </summary>
		public void HideLoader() {
			LoaderScreen.SetActive(false);
		}


		/// <summary>
		/// Обработчик подключения к игре
		/// </summary>
		public void OnConnectedToGame() {
			CacheManager.AddCacheLoadedListener(OnCacheLoaded);
			CacheManager.LoadCache();
		}


		/// <summary>
		/// Вызывается, когда кэш был загружен
		/// </summary>
		private void OnCacheLoaded() {
			Debug.Log("Game loader: cache loaded");
			Presentation.LoadPresentation();

			TeamPick.AddTeamPickListener(OnTeamPicked);
			TeamPick.LoadTeams();
			TeamPick.ShowPanel();
		}


		/// <summary>
		/// Вызывается, когда на запускаемом устройстве была выбрана команда
		/// </summary>
		private void OnTeamPicked() {
			Debug.Log("Team picked");
			TeamPick.HidePanel();

			if (CacheManager.GameDataCache.GameIsStarted) {
				ContinueGame();
				return;
			}

			ShowLoader();
			SetLoaderText(PresentationLoadingText);
			Presentation.AddOnLoadListener(OnPresentationLoaded);
		}


		/// <summary>
		/// Установка текста для загрузочного экрана
		/// </summary>
		private void SetLoaderText(string text) {
			LoaderText.text = text;
		}


		/// <summary>
		/// Вызывается когда загрузка презентации прошла успешно
		/// </summary>
		private void OnPresentationLoaded() {
			Debug.Log("Game loader: presentation loaded");

			HideLoader();

			Presentation.AddOnWatchedListener(OnPresentationWatched);
			Presentation.ShowPresentation();
		}


		/// <summary>
		/// Вызывается когда презентация была просмотрена
		/// </summary>
		private void OnPresentationWatched() {
			Debug.Log("Game loader: presentation watched");
			SetLoaderText(LoadText);
			ShowLoader();
			Presentation.HidePresentation();

			InitGame();
			LoadGame();
		}


		private void LoadGame() {
			print("Load Game");

			SceneManager.LoadSceneAsync(1);
		}


		private void InitGame() {
			GameManager.Init();
			Rounds.Init();
			Transfers.Init();
			Money.Init();
			Order.Init();
			Storage.Init();
			Loan.Init();
			Chartering.Init();
			Buildings.Init();
			Roles.Init();
			RolePick.Init();
			Area.Init();
			AquaparkSlides.Init();
		}


		private void ContinueGame() {
			Debug.Log("Continue game");
			SetLoaderText(LoadText);
			ShowLoader();
			InitGame();

			Rounds.Load(delegate {
				Buildings.Load(delegate {
					Transfers.Load(delegate {
						Storage.LoadStorage(delegate {
							Chartering.Load(delegate {
								Loan.Load(delegate {
									Order.LoadOrders(delegate {
										RolePick.Load(delegate {
											AquaparkSlides.Load(LoadGame);
										});
									});
								});
							});
						});
					});
				});
			});

			ConstructionController.Load();
		}
	}
}