using System.Collections.Generic;
using Network;
using Network.Cache;
using Network.Cache.Data;
using Network.ServerData;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.TeamPickPanel {
	public partial class TeamPick : MonoBehaviour {
		[SerializeField] private GameObject Panel;
		[SerializeField] private Dropdown   Dropdown;

	    private List<TeamData> _loadedTeams;

		public delegate void TeamPickDel();

		private event TeamPickDel OnTeamPickEvent;


		public void ShowPanel() {
			Panel.SetActive(true);
		}


		public void HidePanel() {
			Panel.SetActive(false);
		}


		public void LoadTeams() {
			Dropdown.ClearOptions();
            PacketSender.GetAvailableTeams(CacheManager.GameDataCache.GameId, OnGetAvailableTeam, OnGetAvailableTeamFailed);
		}


		public void AddTeamPickListener(TeamPickDel callback) {
			OnTeamPickEvent += callback;
		}


		private void OnTeamPick() {
			OnTeamPickEvent?.Invoke();
			OnTeamPickEvent = null;
		}


		private void CreateTeams(List<TeamData> teams) {
			Dropdown.onValueChanged.AddListener(OnSelectTeam);
			foreach (var team in teams) {
				Dropdown.options.Add(new Dropdown.OptionData(team.TeamName));
			}
			Dropdown.RefreshShownValue();
			OnSelectTeam(0);
		}


		public void OnSelectTeam(int teamIndex)
		{
		    var teamId = _loadedTeams[teamIndex].ID;
			CacheManager.TeamDataCache.SetTeam(teamId);
		}


	    public void OnConfirmSelectedTeamBtn()
	    {
			PacketSender.SetTeam(TeamDataCache.ActiveTeamId, OnSetTeam, OnSetTeamFailed);
        }
	}
}