﻿using System.Collections.Generic;
using Game;
using Network;
using Network.Cache;
using Network.ServerData;
using Newtonsoft.Json;
using UnityEngine;

namespace MainMenu.TeamPickPanel
{
    public partial class TeamPick
    {
        /// <summary>
        /// Получение списка доступных команд
        /// </summary>
        private List<TeamData> GetDeserializedTeams(string response)
        {
            return JsonConvert.DeserializeObject<List<TeamData>>(response);
        }


        /// <summary>
        /// Успешное получение списка команд
        /// </summary>
        private void OnGetAvailableTeam(WebResponse response)
        {
            _loadedTeams = GetDeserializedTeams(response.Text);
            if (_loadedTeams.Count == 0) {
                NetworkManager.CloseConnection();
                return;
            }
            CreateTeams(_loadedTeams);
        }


        /// <summary>
        /// Неудачное получение списка команд
        /// </summary>
        private void OnGetAvailableTeamFailed(WebResponse response)
        {
            // Повторная попытка получения списка команд
            // и отключение от сервера при ошибке
            PacketSender.GetAvailableTeams(CacheManager.GameDataCache.GameId, OnGetAvailableTeam, r => NetworkManager.CloseConnection());
        }


        /// <summary>
        /// Успешная привязка к команде
        /// </summary>
        private void OnSetTeam(WebResponse response)
        {
            int device = int.Parse(response.Text);
            print($"Device: {device}");
            GameManager.DeviceNum = device;

            OnTeamPick();
        }


        /// <summary>
        /// При ошибке выбора команды - разрыв соединения
        /// </summary>
        private void OnSetTeamFailed(WebResponse response)
        {
            if(response.Code != 503) NetworkManager.CloseConnection();
        }
    }
}
